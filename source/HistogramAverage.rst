Histogram Average
-----------------

Calculates the average value of a buffer, given a histogram of the buffer.

.. figure:: images/HistogramAverageNode.png
   :alt: 

Inputs
~~~~~~

Histogram (Type: ``Histogram``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The input histogram.

Outputs
~~~~~~~

Average (Type: Object)
^^^^^^^^^^^^^^^^^^^^^^

The average value.

Comments
~~~~~~~~

The **Histogram Average** node calculates the average value of a buffer, given a histogram to it.

If a color buffer is used, the average is calculated channel-wise.

Sample
~~~~~~

Here is an example that calculates the average of histogram.

.. figure:: images/BufferAverageSample.png
   :alt: 


