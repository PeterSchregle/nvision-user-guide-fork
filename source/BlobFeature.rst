Blob Feature
------------

Measures features of a region.

.. figure:: images/BlobFeatureNode.png
   :alt: 

Inputs
~~~~~~

Blob (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^

The region that defines the blob.

Outputs
~~~~~~~

Feature
^^^^^^^

The blob analysis result data. The type of the result depends on the selected feature.

Comments
~~~~~~~~

The **Blob Feature** node calculates the feature for a single region.

The features can be selected inside the node. The full set of features is described in the Blob Analysis chapter of the nVision User Guide.
