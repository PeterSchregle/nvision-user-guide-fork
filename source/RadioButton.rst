HMI RadioButton
---------------

Buttons are used to switch or visualize state, as well as to trigger actions. The **RadioButton** is used in groups, where only one of the buttons can be checked.

.. figure:: images/HMIRadioButtonNode.png
   :alt: 

A **RadioButton** is used in a group with other **RadioButton**\ s. Only one of them is checked at a time. If another **RadioButton** in the same group is checked, the previously checked one is unchecked.

At the bottom of the **RadioButton** node is a pin that allows it to connect one child.

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **RadioButton**.

The **RadioButton** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding*, *Foreground*, *Background*, *Font* and *Padding* styles.

Name (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^

The text that is displayed within the button. This text is only displayed, when no child is connected. If a child is connected, the visual definition of the child is used.

Group (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^

The group of the **RadioButton**. **RadioButton**\ s with the same group text are in the same group.

Outputs
~~~~~~~

IsChecked (Type: ``boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``True`` if the radio button is checked, ``False`` otherwise.

Example
~~~~~~~

Here is an example that shows a simple **RadioButton**. This definition:

.. figure:: images/simple_radio_button_def.png
   :alt: 

creates the following user interface:

.. figure:: images/simple_radio_button.png
   :alt: 


