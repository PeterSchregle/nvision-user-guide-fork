Fit Line
--------

Fits a line to a set of two or more points.

.. figure:: images/FitLineNode.png
   :alt: 

Inputs
~~~~~~

Points (Type: ``PointList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of points.

Outputs
~~~~~~~

Line (Type: 'Line')
^^^^^^^^^^^^^^^^^^^

The fitted line.

Comments
~~~~~~~~

The **Fit Line** node fits a line to a set of two or more points.

See Also
~~~~~~~~

There is also the **Robust Fit Line** node, which uses RANSAC and rejects outliers.

Sample
~~~~~~

Here is an **example** that detects several points in an image and fits a line to the set of points. The **example** allows you to interactively explore the parameters of the **Robust Fit Line** node.

You can click on the **hyperlink** to load the sample into **nVision**.
