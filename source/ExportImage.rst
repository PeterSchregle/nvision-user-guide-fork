Export Image
------------

Exports an image to a file.

.. figure:: images/ExportImageNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

An image.

Directory (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A directory in the filesystem. This is the directory, where the saved files will be placed. If the directory does not exist, an error message will be shown.

Filename (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

A filename. You can either enter a filename (with extension) or leave this empty. If you leave it empty, the system will create a name in the form of "image\_00000.tif", where the number will be incremented automatically. If you enter a filename, the file will be overwritten in every execution of the pipeline node.

Comments
~~~~~~~~

The **ExportImage** node saves an image to the filesystem. The directory where the file will be saved can either by typed, or selected with a Search Folder dialog - by clicking on the |image0| button. The node supports a variety of image file formats, such as TIF, PNG, JPG and BMP to name a few. If a file could not be saved for some reason, an appropriate error message is shown.

The **ExportImage** node saves images only in two cases:

1. | The export mode is on (the **Export on/off** button is pressed).
   | |image1|
   |  If the **Export on/off** button is pressed, any time the pipeline runs the file will be saved.

2. | The **Export once** button will be clicked.
   | |image2|
   | If the **Export once** button is clicked, the file will be saved.

Sample
~~~~~~

Here is an example that shows how to used the **ExportImage** node.

.. figure:: images/ImportExportSample.png
   :alt: 

.. |image0| image:: images/EllipsisButton.png
.. |image1| image:: images/ExportOnOffButton.png
.. |image2| image:: images/ExportOnceButton.png
