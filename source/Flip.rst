Flip
----

Flips an image.

.. figure:: images/FlipNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Outputs
~~~~~~~

Flipped (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

This function reverses an image in the vertical direction (turns it upside down).

.. figure:: images/FlippedImage.png
   :alt: 

Sample
~~~~~~

Here is an example that flips an image.

.. figure:: images/FlipSample.png
   :alt: 


