Add Output
----------

Output ports are the means by which data flows out of a subpipeline. A subpipeline can have any number of output ports, including zero. Output ports must be named uniquely, two output ports of the same suppipeline cannot have the same name.

By default a sub-pipeline has one output, named **Output1**.

This is how this looks from the outside:

.. figure:: images/SubPipelineNode.png
   :alt: 

and here how it looks from the inside:

.. figure:: images/SubPipelineOutputInside.png
   :alt: 

With the **Add Output (Ctrl-O)** command (or the ``Ctrl-O`` keyboard shortcut), you can add any number of additional outputs, that are automatically named as well.

.. figure:: images/SubPipelineOutputsInside.png
   :alt: 

To delete an ouotput, right-click on the arrow after of the name and choose the **Delete** command.

To rename an output, click on the name and type.

.. figure:: images/SubPipelineOutputEdit.png
   :alt: 


