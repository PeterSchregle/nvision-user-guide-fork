Union
-----

Calculates the union of two sets.

.. figure:: images/ListUnionNode.png
   :alt: 

Inputs
~~~~~~

ListA (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The first input list, whose distinct elements form the first set for the union.

ListB (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The second input list, whose distinct elements form the second set for the union.

Outputs
~~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The output list, which consists of the set union of the two lists.

Comments
^^^^^^^^

The lists are treated as mathematical sets, which can have distinct objects only.
