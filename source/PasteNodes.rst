Paste
-----

You can use ``Ctrl-V`` or the **Paste (Ctrl-V)** command from the pipeline menu to paste previously copied nodes and their inside connections from the clipboard.

The nodes are pasted at the location of the mouse cursor.

Since outside connections are not copied, the pasted fragments may need to be reconnected to the rest of the pipeline.
