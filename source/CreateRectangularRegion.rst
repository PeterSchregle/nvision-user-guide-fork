|image0| Rectangular Region
---------------------------

Creates a rectangular region.

.. figure:: images/CreateRectangularRegionNode.png
   :alt: 

Inputs
~~~~~~

Size (Type: ``VectorDouble``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The size of the rectangular region.

Direction (Type: ``Direction``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The direction of the rectangular region.

Outputs
~~~~~~~

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The rectangular region.

Comments
~~~~~~~~

The region is centered on the origin. Regions centered on the origin are suitable for morphological operations, such as erosion, dilation, etc. The region is used as a structuring element and the shape of the region affects the morphological operation.

.. |image0| image:: images/rectangular_region.png
