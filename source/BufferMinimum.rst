Minimum
-------

Calculates the minimum value of a buffer.

.. figure:: images/BufferMinimumNode.png
   :alt: 

Inputs
~~~~~~

Buffer (Type: ``Buffer``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input buffer.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

An optional region.

Outputs
~~~~~~~

Minimum (Type: ``Object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The minimum value.

Comments
~~~~~~~~

The **Minimum** node calculates the minimum value of a buffer.

If the *Region* input is connected, the calculation of the minimum is constrained to the pixels within the region only, otherwise the whole buffer is used.

If a color buffer is used, the minimum length color vector is calculated.

Sample
~~~~~~

Here is an example that calculates the minimum of an image.

.. figure:: images/BufferMinimumSample.png
   :alt: 


