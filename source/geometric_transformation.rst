Geometric Transformation
========================

This chapter explains the use of geometric transformations.

Translation
-----------

A point

.. math:: \begin{pmatrix}x\\y\end{pmatrix}

in the two-dimensional plane is translated or shifted by adding offsets to it. This can be written as:

.. math:: x^{'}=x+t_x

.. math:: y^{'}=y+t_y

or in vector form:

.. math:: \begin{pmatrix}x^{'}\\y^{'}\end{pmatrix} = \begin{pmatrix}x{}\\y\end{pmatrix} + \begin{pmatrix}t_x\\t_y\end{pmatrix}

Scaling
-------

A point

.. math:: \begin{pmatrix}x\\y\end{pmatrix}

in the two-dimensional plane is scaled or stretched by multiplying with scaling factors. This can be written as:

.. math:: x^{'}=s_x*x

.. math:: y^{'}=s_y*y

or in matrix form:

.. math:: \begin{pmatrix}x^{'}\\y^{'}\end{pmatrix} = \begin{pmatrix}s_x&0\\0&s_y\end{pmatrix} * \begin{pmatrix}x{}\\y\end{pmatrix}

Rotation
--------

A point

.. math:: \begin{pmatrix}x\\y\end{pmatrix}

in the two-dimensional plane is rotated around the origin by multiplying with a rotation matrix. This can be written as:

.. math:: x^{'}=x*cos(\phi)-y*sin(\phi)

.. math:: y^{'}=x*sin(\phi)+y*cos(\phi)

or in matrix form:

.. math:: \begin{pmatrix}x^{'}\\y^{'}\end{pmatrix} = \begin{pmatrix}cos(\phi)\\sin(\phi)&-sin(\phi)\\cos(\phi)\end{pmatrix} * \begin{pmatrix}x{}\\y\end{pmatrix}


