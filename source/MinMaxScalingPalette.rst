MinMaxScalingPalette
--------------------

Creates a palette with a linear transfer function, given minimum and maximum values of an image.

.. figure:: images/MinMaxScalingPaletteNode.png
   :alt: 

Inputs
~~~~~~

Type (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The type of the palette. Choices are ``PaletteByte``, ``PaletteUInt16``, ``PaletteUInt32``, ``PaletteDouble``, ``PaletteRgbByte``, ``PaletteRgbUInt16``, ``PaletteRgbUInt32`` and ``PaletteRgbDouble``.

Width (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The width of the palette.

Minimum (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The minimal value.

Maximum (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The maximal value.

Outputs
~~~~~~~

Palette (Type: ``Palette``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The output palette.

Comments
~~~~~~~~

This function creates a palette with a linear function.

The minimum and maximum values are usually determined by inspecting an image. When the image is then mapped over a palette created with this function, it will be remapped to gray values between 0 and 255.

Here is an example of a linear curve with a minimum of 32 and a maximum of 200:

.. figure:: images/minmax_32_200_palette.png
   :alt: 


