The browser displays the various loaded elements. **nVision** understands file and folder elements.

Files
-----

Files are displayed with a preview, if possible, and additional information, such as the size of the image and the datatype.

.. figure:: images/browser_file.png
   :alt: 

If you click the tile, the file is displayed in the **nVision** workspace.

If you move the mouse cursor to the file tile, a gray x-shaped button is show in the upper right corner, which closes the file when clicked.

Folders
-------

Folders are displayed without preview. The tile shows the number of files, as well as the currently displayed file index and name.

.. figure:: images/browser_folder.png
   :alt: 

You can use the commands on the ribbon **Pipeline** tab in the **Control** group to cycle through the files in the folder.
