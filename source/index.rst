.. figure:: images/n-vision.png
   :alt: 

User Guide

**Machine Vision Development System**
Release 2017.1

.. figure:: images/monitor.png
   :alt: 

| *Copyright (c) 2017 Impuls Imaging GmbH*
| *All rights reserved.*

.. figure:: images/ImpulsLogo.png
   :alt: 

| **Impuls Imaging GmbH**
| Schlingener Str. 4
| 86842 Türkheim

Germany/European Union

http://www.impuls-imaging.com

.. figure:: images/made-in-germany.png
   :alt: 
   
.. toctree::
   :maxdepth: 2

   introduction
   overview
   ribbon
   tools
   dataflow
   image_processing
   image_analysis
   geometry
   user_interface
   pipelines
   miscellaneous
   reference
   credits