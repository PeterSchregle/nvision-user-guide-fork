Double
------

Create and set a floating point value.

.. figure:: images/ConstantDoubleNode.png
   :alt: 

Outputs
~~~~~~~

Value (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

The floating point value.

Comments
~~~~~~~~

This node provides the means to create and set a floating point value.

The value can be changed by typing the floating point value inside the edit field in the node. Also, the spinner buttons can be used to increase or decrease the value.

The title of the node can be edited.
