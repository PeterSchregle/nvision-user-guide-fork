Text -> Brush
-------------

Converts text to a brush.

Inputs
~~~~~~

Text (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^

The text that is converted to a brush.

Outputs
~~~~~~~

Brush (Type: ``SolidColorBrush``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The output brush.

Comments
~~~~~~~~

There are various ways how you can specify the brush color.

By Name
^^^^^^^

Use the color name, such as ``Red`` or ``Orange``. The name is case insensite, that is you can use ``red`` as well. These colors are available:

.. figure:: images/named_color_table.png
   :alt: 

By Color Components
^^^^^^^^^^^^^^^^^^^

Use a hash sign, followed by three (``#rgb``), four (``#argb``), six (``#rrggbb``) or eight (``#aarrggbb``) hexadecimal numbers. Examples are: ``#00F``, ``#F00F``, ``#0000FF``, ``#FF0000FF``.
