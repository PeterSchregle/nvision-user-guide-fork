Gauss
-----

Filters an image using a gaussian kernel of size 3x3 or 5x5. A gaussian filter is a lowpass and blurs an image.

.. figure:: images/GaussNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

KernelSize (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The kernel size (3 -> 3x3, 5 -> 5x5).

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies an optional area of interest.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

::

    1 2 1  
    2 4 2  
    1 2 1

or a 5x5 matrix with the following values:

::

      2  7  12  7  2
      7 31  52 31  7
     12 52 127 52 12
      7 31  52 31  7
      2  7  12  7  2

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

Here are a few results of the gauss filter with increasing kernel sizes:

Original:

.. figure:: images/GaussianOriginal.png
   :alt: 

KernelSize = 3:

.. figure:: images/Gauss3x3.png
   :alt: 

KernelSize = 5:

.. figure:: images/Gauss5x5.png
   :alt: 

Sample
~~~~~~

Here is an example that shows how to use the gauss filter.

.. figure:: images/GaussSample.png
   :alt: 


