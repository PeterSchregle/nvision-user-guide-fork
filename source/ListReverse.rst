Reverse
-------

Reverses a list.

.. figure:: images/ReverseNode.png
   :alt: 

Inputs
~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The input list.

Outputs
~~~~~~~

Reverse (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The output list, with the items in reverse order.
