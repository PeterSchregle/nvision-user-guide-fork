EditDouble
----------

The **EditDouble** is a HMI control that can be used to enter floating point numbers.

.. figure:: images/HMIEditDouble.png
   :alt: 

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **EditDouble**.

The **EditDouble** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding* and *Background*, *Foreground* and *Font* styles.

TextBrush (Type: ``SolidColorBrush``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The text color.

Font (Type: ``Font``)
^^^^^^^^^^^^^^^^^^^^^

The font.

Outputs
~~~~~~~

Value (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

The number entered by the user.

Example
~~~~~~~

Here is an example that shows the various edit controls. This definition:

.. figure:: images/hmi_edit_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_edit.png
   :alt: 


