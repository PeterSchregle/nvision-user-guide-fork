ResamplePolar
-------------

Performs a transformation from a polar to a rectangular coordinate system.

.. figure:: images/ResamplePolarNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Center (Type: ``Ngi.PointDouble``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The polar center.

LengthOffset (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The radius from the center. This is mapped to the bottom of the resulting image.

LengthScale (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The scaling factor (radius to y-direction).

AngleOffset(Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The starting angle in radians. This is mapped to the left of the resulting image.

AngleScale(Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The scaling factor (angle to x-direction).

Filter (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The geometric interpolation. Available values are ``NearestNeighbor``, ``Box``, ``Triangle``, ``Cubic``, ``Bspline``, ``Sinc`` , ``Lanczos`` and ``Kaiser``. The accuracy of the interpolation increases from ``NearestNeighbor`` to ``Kaiser``, but the performance decreases. The default is set to ``Box``.

Outputs
~~~~~~~

Resampled Polar (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The output image (has the same size as the input image).
