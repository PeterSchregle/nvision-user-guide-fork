Otsu Threshold
--------------

Calculates an optimal threshold with the Otsu method, given a histogram.

.. figure:: images/OtsuThresholdFromHistogramNode.png
   :alt: 

Inputs
~~~~~~

Histogram (Type: ``Histogram``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The input histogram.

Outputs
~~~~~~~

Threshold (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The threshold value.

Comments
~~~~~~~~

The **Otsu Threshold** node calculates a threshold value that can be used to segment an image into foreground and background. The algorithm assumes that the image contains two classes of pixels following bi-modal histogram (foreground pixels and background pixels), it then calculates the optimum threshold separating the two classes so that their combined spread (intra-class variance) is minimal.

Sample
~~~~~~

Here is an example that calculates a threshold with the Otsu method.

.. figure:: images/OtsuThresholdSample.png
   :alt: 

Here is the original image:

.. figure:: images/otsu_algorithm.png
   :alt: 

And here is the binary image thresholded with the Otsu threshold:

.. figure:: images/otsu_binary.png
   :alt: 


