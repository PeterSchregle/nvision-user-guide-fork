HMI Scrollbar
-------------

A **ScrollBar** can be used to scroll to or visualize a position.

.. figure:: images/HMIScrollbarNode.png
   :alt: 

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **ScrollBar**.

The **Border** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding*, *Foreground*, *Background*, *Font* and *Padding* styles.

Orientation (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Orientation of the **Scrollbar**, ``Horizontal`` or ``Vertical``.

Minimum (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The minimum value.

Maximum (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The maximum value.

SmallChange (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The amount that is subtracted from or added to the value when the *Left* or *Right* keyboard cursor keys are pressed.

LargeChange (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The amount that is subtracted from or added to the value when the channel is clicked left or right of the thumb.

Outputs
~~~~~~~

Value (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^

The position.

Example
~~~~~~~

Here is an example that shows a **ScrollBar**. This definition:

.. figure:: images/hmi_scrollbar_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_scrollbar.png
   :alt: 


