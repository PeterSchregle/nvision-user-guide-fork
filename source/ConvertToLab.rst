Convert to LAB
--------------

Converts an image to the LAB color model.

.. figure:: images/ConvertToLabNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image. This image can be of any color model.

Outputs
~~~~~~~

Lab (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^

The output image in the LAB color model.

Comments
~~~~~~~~

This function converts an image to the LAB color model, regardless of the color space of the input image.

Here is an example of the original color image:

.. figure:: images/rgbimg.png
   :alt: 

And here is how the L, a and b channels look side by side.

.. figure:: images/lab_sbs.png
   :alt: 

Sample
~~~~~~

Here is an example:

.. figure:: images/ConvertToLabSample.png
   :alt: 


