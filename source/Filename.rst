Filename
--------

Selects a file.

.. figure:: images/FilenameNode.png
   :alt: 

Inputs
~~~~~~

Filename (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The filename (provided as a string, or selected via the ... button.

Outputs
~~~~~~~

Filename (Type: ``Ngi.Path``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The filename as a path.

Comments
~~~~~~~~

The purpose of this node is to make it easy for the user to select a file from the filesystem.
