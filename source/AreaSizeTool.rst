Area Size Tool
--------------

.. figure:: images/features_area_size.png
   :alt: 

The **Area Size** tool measures the object area in a region of interest.

The tool displays graphical elements to specify the region of interest as well as to display the results.

It displays a rectangle that is used to select a region. You can move the rectangle around on the image by dragging its inside. You can also resize the rectangle by picking it at its boundary lines (on the boundary line or just a bit outside). You can rotate the rectangle by picking it at its corner points (on the corner point of just a bit outside).

The tool is meant to be used on regions where you have contrasting objects, either dark or bright. It automatically detects the objects. In a region without much contrast, using the tool is mostly meaningless because of noise.

The numerical value is displayed in green, if it is in between the tool's minimum and maximum expected values, or in red , if the measured area is not within the expected boundary values.

The area size tool has a configuration panel, which can be used to set parameters.

The **Min** parameter is used to set the minimal accepatable area, the **Max** parameter is used to set the maximal accepatable area.

The **Polarity** can be set to **Dark Objects** or **Bright Objects**.

Objects smaller than a specified amount of pixels can be filtered out when **Filter Objects Smaller Than** is set.

Objects touching the border of the region of interest can be filtered out when **Filter Objects Touching Border** is set.
