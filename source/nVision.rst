.. figure:: images/nVisionLogo.png
   :alt: 

Machine Vision Development Software
-----------------------------------

2017.1

.. figure:: images/nVisionMonitor.png
   :alt: 

**nVision** is a visual development software for industrial image processing.

It combines easy to use tools with interactive useage and intuitive graphical pipeline programming for automation.

This help page provides context sensitive information as you use **nVision**.

If you do not need to see the help information, you can switch the help window off (or back on), using the Show Help |image0| command, (or the F1 key |image1|).

At the top of the help window is the help toolbar with the following commands:

+------------+-----------------------------------------------------------------------+
| |image6|   | Displays this page in the help window.                                |
+------------+-----------------------------------------------------------------------+
| |image7|   | Displays internet links to tutorial videos.                           |
+------------+-----------------------------------------------------------------------+
| |image8|   | Shows information about printable documentation in the help window.   |
+------------+-----------------------------------------------------------------------+
| |image9|   | Browse and load examples                                              |
+------------+-----------------------------------------------------------------------+

Make sure to read the **nVision Quick Start Guide**. It contains the most important information on a single page.

\ |image10|\ 

If you need more information or need to contact **Impuls Imaging GmbH**, please go to the website www.impuls-imaging.com for up-to-date contact information.

.. |image0| image:: images/help.png
.. |image1| image:: images/f1-key.png
.. |image2| image:: images/home.png
.. |image3| image:: images/video.png
.. |image4| image:: images/book.png
.. |image5| image:: images/laboratory.png
.. |image6| image:: images/home.png
.. |image7| image:: images/video.png
.. |image8| image:: images/book.png
.. |image9| image:: images/laboratory.png
.. |image10| image:: images/nVisionQuickStartGuide.png
