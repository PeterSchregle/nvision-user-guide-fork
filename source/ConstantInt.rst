Integer
-------

Create and set an integer value.

.. figure:: images/ConstantIntNode.png
   :alt: 

Outputs
~~~~~~~

Value (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The integer value.

Comments
~~~~~~~~

This node provides the means to create and set an integer value.

The value can be changed by typing the integer value inside the edit field in the node. Also, the spinner buttons can be used to increase or decrease the value.

The title of the node can be edited.
