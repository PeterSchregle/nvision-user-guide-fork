Connected Components
--------------------

Splits a region into a list of multiple spatially disconnected regions.

.. figure:: images/ConnectedComponentsNode.png
   :alt: 

Inputs
~~~~~~

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input region.

DistanceX (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

A horizontal distance. Pixels within this horizontal distance are considered to be connected.

DistanceY (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

A vertical distance. Pixels within this horizontal distance are considered to be connected.

Outputs
~~~~~~~

Blobs (Type: ``RegionList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The resulting list of disconnected regions.

BlobColors
^^^^^^^^^^

A list of colors that can be used to color the regions in a way that expresses their separated nature by using different colors (the list contains six distinct colors that are repeated).

Comments
~~~~~~~~

Usually, connectedness is specified using the terms 4-connected and 8-connected in the literature. 4-connected means that objects touching only at the diagonal pixels are separated, 8-connected means that such objects are not separated. The two distance parameters can be used to specify 4-connectedness (DistanceX == 0, DistanceY == 0) or 8-connectedness (DistanceX == 1, DistanceY == 1). Bigger values of the distance parameters can jump over gaps and still treat objects as being connected.

Using this pipeline:

.. figure:: images/ConnectedComponents3.png
   :alt: 

the following region has been created by thresholding:

.. figure:: images/ConnectedComponents1.png
   :alt: 

and the following list of regions has been created by separating objects with the Connected Components node:

.. figure:: images/ConnectedComponents2.png
   :alt: 


