Take
----

Takes a number of items at the beginning of a list. The remaining items are not put in the output list.

.. figure:: images/TakeNode.png
   :alt: 

Inputs
~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The input list.

Count (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The number of items to take.

Outputs
~~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The output list, consisting of the beginning part of the input list that hast been taken.
