HMI ToolBar
-----------

The **ToolBar** allows you to create a horizontal or vertical toolbar.

.. figure:: images/HMIToolBarNode.png
   :alt: 

At the bottom of the **ToolBar** node is a pin that allows it to connect several children.

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **ToolBar**.

The **ToolBar** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin* and *Background* styles.

Orientation (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Orientation of the **ToolBar**, ``Horizontal`` or ``Vertical``.
