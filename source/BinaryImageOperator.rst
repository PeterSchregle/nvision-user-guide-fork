BinaryImageOperator
-------------------

Applies a binary point operation between two images. Possible operations can be grouped into arithmetic, logic and comparison fields. Binary point operations are applied pixel by pixel, the same way for every pixel, for corresponding pixels of two images.

.. figure:: images/BinaryImageOperatorNode.png
   :alt: 

Arithmetic Operators
''''''''''''''''''''

The following operators are available:

.. figure:: images/BinaryImageOperatorArithmetic.png
   :alt: 

Add
   

Add with saturation.

.. figure:: images/BinaryAdd.png
   :alt: 

Image + Gradient -> Result

Subtract
        

Subtract with saturation.

.. figure:: images/BinarySubtract.png
   :alt: 

Image - Gradient -> Result

Difference
          

Difference with saturation.

.. figure:: images/BinaryDifference.png
   :alt: 

abs ( Image - Gradient ) -> Result

Multiply
        

Multiply without saturation.

.. figure:: images/BinaryMultiply.png
   :alt: 

Image \* Gradient -> Result

Divide
      

Divide without saturation.

.. figure:: images/BinaryDivide.png
   :alt: 

Image / Gradient -> Result

Multiply (Blend)
                

Multiply with saturation.

.. figure:: images/BinaryMultiplyBlend.png
   :alt: 

Image \* Gradient -> Result

Divide (Blend)
              

Divide with saturation.

.. figure:: images/BinaryDivideBlend.png
   :alt: 

Image / Gradient -> Result

Logic Operators
'''''''''''''''

The following operators are available:

.. figure:: images/BinaryImageOperatorLogic.png
   :alt: 

And
   

Logical And.

.. figure:: images/BinaryAnd.png
   :alt: 

Image & Gradient -> Result

Or
  

Logical Or.

.. figure:: images/BinaryOr.png
   :alt: 

Image \| Gradient -> Result

Xor
   

Logical Xor.

.. figure:: images/BinaryXor.png
   :alt: 

Image ^ Gradient -> Result

Comparison Operators
''''''''''''''''''''

The following operators are available:

.. figure:: images/BinaryImageOperatorComparison.png
   :alt: 

Smaller
       

Compare smaller.

.. figure:: images/BinarySmaller.png
   :alt: 

Image < Gradient -> Result

Smaller or Equal
                

Compare smaller or equal.

.. figure:: images/BinarySmallerOrEqual.png
   :alt: 

Image <= Gradient -> Result

Equal
     

Compare equal.

.. figure:: images/BinaryEqual.png
   :alt: 

Image == Gradient -> Result

Bigger or Equal
               

Compare bigger or equal.

.. figure:: images/BinaryBiggerOrEqual.png
   :alt: 

Image >= Gradient -> Result

Bigger
      

Compare bigger.

.. figure:: images/BinaryBigger.png
   :alt: 

Image > Gradient -> Result

Min/Max Operators
'''''''''''''''''

The following operators are available:

.. figure:: images/BinaryImageOperatorMinMax.png
   :alt: 

Min
   

Minimum.

.. figure:: images/BinaryMin.png
   :alt: 

min( Image, Gradient) -> Result

Max
   

Maximum.

.. figure:: images/BinaryMax.png
   :alt: 

max( Image, Gradient) -> Result

Inputs
~~~~~~

ImageA (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^

The first input image.

ImageB (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^

The second input image.

Operator (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the operator.

+---------------+--------------------+
| operator      | operation          |
+===============+====================+
| ``+``         | add                |
+---------------+--------------------+
| ``-``         | subtract           |
+---------------+--------------------+
| ``diff``      | difference         |
+---------------+--------------------+
| ``*``         | multiply           |
+---------------+--------------------+
| ``/``         | divide             |
+---------------+--------------------+
| ``*_blend``   | multiply (blend)   |
+---------------+--------------------+
| ``/_blend``   | divide (blend)     |
+---------------+--------------------+
| ``&``         | and                |
+---------------+--------------------+
| ``|``         | or                 |
+---------------+--------------------+
| ``^``         | xor                |
+---------------+--------------------+
| ``<``         | smaller            |
+---------------+--------------------+
| ``<=``        | smaller or equal   |
+---------------+--------------------+
| ``==``        | equal              |
+---------------+--------------------+
| ``>=``        | bigger or equal    |
+---------------+--------------------+
| ``>``         | bigger             |
+---------------+--------------------+
| ``min``       | minimum            |
+---------------+--------------------+
| ``max``       | maximum            |
+---------------+--------------------+

Outputs
~~~~~~~

Result (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^

The result image.
