Concat Text
-----------

Concatenates two or more text values.

.. figure:: images/StringAppendNode.png
   :alt: 

Inputs
~~~~~~

A (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^

The first operand.

B (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^

The second operand.

C...Z (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^

Additional operands, added on demand.

Outputs
~~~~~~~

Text (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The concatenation of all operands.

Comments
~~~~~~~~

This node concatenates multiple text operands.

The ports for the inputs are added dynamically on demand. The node starts with the two inputs A and B. If both are connected, a third input named C is added. At a maximum, 26 inputs are possible. Inputs can also be deleted by removing the connection to them.
