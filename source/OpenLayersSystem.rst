OpenLayers
----------

Get information about the OpenLayers system of Data Translation.

.. figure:: images/OpenLayersSystemNode.png
   :alt: 

This node initializes the OpenLayers system and delivers a list of connected module names.

.. figure:: images/dt9817.jpg
   :alt: 

Outputs
~~~~~~~

Modules (Type: ``List<string>``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A list of module names.

Version (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The version of the installed OpenLayers software.
