Square Root
-----------

Calculates the square root of every pixel.

.. figure:: images/UnarySquareRootNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Outputs
~~~~~~~

Result (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^

The result image.
