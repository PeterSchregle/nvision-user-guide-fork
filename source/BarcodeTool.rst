Barcode Tool
------------

.. figure:: images/identify_barcode.png
   :alt: 

The **Barcode** tool decodes a barcode inside a region of interest.

The tool displays graphical elements to specify the region of interest as well as to display the results.

It displays a box that is used to select the region of interest. You can move the box around on the image by dragging its inside. You can also resize the box by picking its boundary lines (on the boundary line or just a bit outside) or its corner points (on the corner point of just a bit outside).

Once the search box has been positioned over a code in the image it tries to decode the code. Its position and the decoded text is displayed in green, if it matches the expected text (anything in this case), or red, if the decoded text does not match the expectation:

The **Barcode** tool has a configuration panel, which can be used to set parameters.

The controls in **Pattern Matching** allow you to specify a pattern that the decoded result must follow in order to be correct.

**Regular Expression**: Defines the pattern that the decoded string should match. The pattern follows the rule of a regular expression, specifically the .NET variant of regular expressions.

The following table explains common cases. For full information, look at the `original documentation (https://msdn.microsoft.com/en-us/library/az24scfc(v=vs.110).aspx) <https://msdn.microsoft.com/en-us/library/az24scfc(v=vs.110).aspx>`__.

+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| Pattern           | Description                                                                                                                          |                     |                                           |
+===================+======================================================================================================================================+=====================+===========================================+
| ``.``             | Any character (except newline).                                                                                                      | ``a.c``             | ``abc``, ``aac``, ``acc``, ...            |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``^``             | Start of a string.                                                                                                                   | ``^abc``            | ``abc``, ``abcdefg``, ``abc123``, ...     |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``$``             | End of a string.                                                                                                                     | ``abc$``            | ``abc``, ``endsinabc``, ``123abc``, ...   |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``&#124;``        | Alternation.                                                                                                                         | ``bill&#124;ted``   | ``bill``, ``ted``                         |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``[...]``         | Explicit set of characters to match.                                                                                                 | ``a[bB]c``          | ``abc``, ``aBc``                          |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``*``             | 0 or more of previous expression.                                                                                                    | ``ab*c``            | ``ac``, ``abc``, ``abbc``, ...            |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``+``             | 1 or more of previous expression.                                                                                                    | ``ab+c``            | ``abc``, ``abbc``, ``abbbc``, ...         |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``?``             | 0 or 1 of previous expression; also forces minimal matching when an expression might match several strings within a search string.   | ``ab?c``            | ``ac``, ``abc``                           |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``{``...\ ``}``   | Explicit quantifier notation.                                                                                                        | ``ab{2}c``          | ``abbc``                                  |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``(``...\ ``)``   | Logical grouping of part of an expression.                                                                                           | ``(abc){2}``        | ``abcabc``                                |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``\\``            | Preceding one of the above, it makes it a literal instead of a special character.                                                    | ``a\\.b``           | ``a.b``                                   |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+

**Ignore Case**: If checked the case of the characters is ignored, otherwise the case must match exactly.

The controls in **Enabled Symbologies** allow you to enable or disable specific symbologies. Supported symbologies are Code 39, Code 30 Extended, Code 93, EAN 128, EAN 8, EAN 13, UPC A, UPC E, Databar and Databar Limited.

The controls in **Decoder Parameters** allow you to manipulate locator and decoder behavior.

**Number of Scanning Directions**: 0: vertical, 1: horizontal, 2: both, 3 and more: oblique, every 180/N degrees. The default setting of 4 means the locator scans every 45 degrees.

**Scanning Density**: The spacing between scanning-lines, in pixels; small values favor the decoding rate and increase the running time. 5 is the default.

**Noise**: The noise threshold. Threshold level to get rid of spurious bars caused by noise. 40 is the default.

**Check Quiet Zone**: Check the quiet zone. When checked, a quiet zone as large as the standard requires must be present; when set to false, it is advisable to disable other symbologies to avoid false matches. The default is checked.

**Minimum Bars**: The minimum number of bars for a successfull decode.
