RegionListFilter
----------------

Filters the regions in a list according to a criterium.

.. figure:: images/RegionListFilterNode.png
   :alt: 

Inputs
~~~~~~

Regions (Type: ``RegionList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of regions.

PropertyPath (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the property that is used for filtering. All the blob analysis features (Area, Perimeter, etc.) can be used for filtering.

The full set of features is described in the Blob Analysis chapter of the nVision User Guide.

Some features have sub-features, such as the Centroid. For these features, you can use the dot (``.``), to specify the desired feature, such as ``RegionCentroid.X``.

ComparisonOperator (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the operator (``<``, ``<=``, ``>``, ``>=``, ``==`` or ``!=``).

Constant (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The constant used for the comparison.

Outputs
~~~~~~~

FilteredRegions (Type: ``RegionList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The resulting list of regions, which contains the regions that satisfy the criterium.

Comments
~~~~~~~~

You can chain RegionListFilter nodes to filter for more than one criterium.

You can combine results of several RegionListFilter nodes with the set operations on available for lists (Union, Intersect, Except) to create more complex filters.
