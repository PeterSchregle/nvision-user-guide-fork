HMI GridLength
--------------

The **GridLength** node specifies the size (absolute or relative) of a specific row or column.

.. figure:: images/HMIGridLengthNode.png
   :alt: 

Inputs
~~~~~~

Unit (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^

The possible values are ``Auto``, ``*`` or ``Absolute``.

Value (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^

The size of the row or column. The interpretation of the value is determined by the **Unit** as well.

If the **Unit** is ``Auto``, the **Value** is ignored and the size is determined by the contents of the controls in the column or row.

If the **Unit** is ``Absolute``, the **Value** is interpreted as a size given in pixel.

If the **Unit** is ``*``, the **Value** is interpreted as a relative size. The remaining space in the grid, after all ``Auto`` and ``Abolute`` columns are taken is split in relation to the values given with ``*``.

Comments
~~~~~~~~

In order to use the **GridLength** to specify the settings for rows or columns, one or more **GridLength** nodes must be put in a list.
