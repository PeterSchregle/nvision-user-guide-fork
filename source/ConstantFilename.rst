Filename
--------

Create and set a filename.

.. figure:: images/ConstantFilenameNode.png
   :alt: 

Outputs
~~~~~~~

Filename (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The filename.

Comments
~~~~~~~~

This node provides the means to create and set a filename.

The value can be changed by typing the text inside the edit field in the node, or by selecting a file using the [...] button.

The title of the node can be edited.
