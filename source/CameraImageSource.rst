Image Source
------------

Delivers images from a camera in live acquisition mode.

.. figure:: images/ImageSourceNode.png
   :alt: 

Inputs
~~~~~~

Camera (Type: ``CameraInfo``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the camera.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

Delivers an image from the camera.

Comments
~~~~~~~~

The **Start Acquisition** node starts the acquisition on the specified camera. Once the acquition is running, an **Image Source** node will deliver images from the camera. The **Stop Acquisition** node stops the acquisition on the specified camera.

This **Image Source** node runs asynchronously. When a new image from a camera arrives, the pipeline executor is notified and executes the pipeline.
