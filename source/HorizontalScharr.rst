Horizontal Scharr
-----------------

Filters an image using a horizontal scharr kernel.

.. figure:: images/HorizontalScharrNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies an optional area of interest.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

::

     3  10  3
     0   0  0
    -3 -10 -3

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

Here are a few results of the horizontal scharr filter with increasing kernel sizes:

Original:

.. figure:: images/GaussianOriginal.png
   :alt: 

Result:

.. figure:: images/HorizontalScharr.png
   :alt: 

Sample
~~~~~~

Here is an example that shows how to use the horizontal scharr filter.

.. figure:: images/HorizontalScharrSample.png
   :alt: 

Often, you want to combine the horizontal and the vertical scharr filter to create an edge map. A simple way to do this is shown in the following sample:

.. figure:: images/ScharrEdgeMapSample.png
   :alt: 

Here is an example image of such an edge map:

.. figure:: images/ScharrEdgeMap.png
   :alt: 


