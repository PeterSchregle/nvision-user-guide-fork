Pyramid
-------

Calculates an image pyramid by successively scaling down the image a number of levels.

.. figure:: images/ImagePyramidNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Levels (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^

The number of levels in the pyramid.

Downscale (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The downscale factor from one pyramid level to the next.

Filter (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The geometric interpolation. Available values are ``NearestNeighbor``, ``Box``, ``Triangle``, ``Cubic``, ``Bspline``, ``Sinc`` , ``Lanczos`` and ``Kaiser``. The accuracy of the interpolation increases from ``NearestNeighbor`` to ``Kaiser``, but the performance decreases. The default is set to ``Box``.

Outputs
~~~~~~~

Pyramid (Type: ``ImagePyramid``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The output image pyramid. The image pyramid is a list of images, starting with original, not-downscaled image, followed by ``Levels-1`` images, which are subsequently downscaled by the factor ``Downscale``.
