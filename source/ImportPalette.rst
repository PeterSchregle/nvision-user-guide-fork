ImportPalette
-------------

Imports a palette from a file.

.. figure:: images/ImportPaletteNode.png
   :alt: 

Inputs
~~~~~~

Filename (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

A filename in the filesystem.

Outputs
~~~~~~~

Palette (Type: ``Palette``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The loaded palette.

Comments
~~~~~~~~

The **ImportPalette** node loads a palette from the filesystem. The name of the file can either by typed, or selected with a File Open dialog - by clicking on the |image0| button.

The supported format is a binary file consisting of either 256 bytes (for a monochrome palette) or 768 bytes (for a color palette).

.. |image0| image:: images/EllipsisButton.png
