|image0| Sql Insert
-------------------

Inserts data into a database table.

.. figure:: images/MySqlInsertNode.png
   :alt: 

Inputs
~~~~~~

Connection (Type: ``MySql.Data.MySqlClient.MySqlConnection``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The database connection.

Sync (Type: ``Object``)
^^^^^^^^^^^^^^^^^^^^^^^

This input can be connected to any other object. It is used to establish an order of execution.

Outputs
~~~~~~~

Sync (Type: ``Object``)
^^^^^^^^^^^^^^^^^^^^^^^

This output can be connected to any other object. It is used to establish an order of execution.

Comments
~~~~~~~~

Once the database connection has been established, the node will fill its selection combo box with the tables that are available in the database. Once you have selected the desired table, the node will be populated dynamically with more inputs of type ``String``, which correspond to the column names of the selected table. You can either type values or connect to outputs from elsewhere in the pipeline. If connections are made, you cannot change the table inside the node, because this could break existing connections.

The following example assumes that you have MySql installed and that you can connect to the ``sakila`` sample database.

.. figure:: images/MySqlInsertSample.png
   :alt: 

.. |image0| image:: images/db_insert.png
