Mirror
------

Mirrors an image.

.. figure:: images/MirrorNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Outputs
~~~~~~~

Mirrored (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

This function reverses an image in the horizontal direction (exchanges left and right).

.. figure:: images/MirroredImage.png
   :alt: 

Sample
~~~~~~

Here is an example that mirrors an image.

.. figure:: images/MirrorSample.png
   :alt: 


