Circle Tool
-----------

.. figure:: images/geometry_circle.png
   :alt: 

The **Circle** tool measures a circle in a region of interest.

The tool displays graphical elements to specify the region of interest as well as to display the results.

It displays a yellow box that specifies the region of interest. You can move the box around on the image by dragging its inside. You can also resize the box by picking its boundary lines (on the boundary line or just a bit outside) or its corner points (on the corner point of just a bit outside). Inside the box is a little yellow point that markes the center of the box.

Once the box has been positioned over a circle (or part of a circle) in the image it displays the calculated circle, which is visualized graphically and its numerical diameter is displayed in green, if it is in between the tool's minimum and maximum expected values, or in red, if the measured distance is not within the expected boundary values.

The tool displays its measurement in pixels, or in world units, such as mm, if the system has been calibrated.

The circle tool has a configuration panel, which can be used to set parameters.

The **Min** and **Max** parameters are used to set the expected minimum and maximum diameter.

The parameters in the **Edge Detection Parameters** affect the edge detection.

**Smoothing** is the amount of smoothing used in edge detection (default: 2.7). **Strength** is the minimum strength of an edge (the gray scale slope of the edge, default = 15). Smoothing and Strength are interrelated: the more you smooth the more you lessen the strength of an edge and vice versa.

**Polarity** can be used to select the type of edge: **Both**, **Rising** and **Falling** can be selected (default = Both). Rising means an edge going from dark to bright along the search direction, falling means an edge from bright to dark, and both means both edge types.

**Spacing** is the angular spacing in degrees of the search lines.
