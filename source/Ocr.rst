OCR
---

Performs optical character recognition (OCR) or optical character verification (OCV).

.. figure:: images/OcrNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Filtename (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The OCR font file.

WhiteOnBlack (Type: ``Boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Detect characters printed black on white (default, 0) or white on black (1).

LocalThreshold (Type: ``Boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Use a global threshold (default, 0) or a local threshold (1) for character segmentation.

InsertNewlines (Type: ``Boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Do not insert newline characters (default, 0) or insert newline characters (1) in decoded result.

MinimumScore (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The minimum score for character acceptance. The recongition score is between 0 and 1. The higher the score, the better the character.

DiscardBadScores (Type: ``Boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Do not discard characters with bad scores (default, 0) or discard them (1).

Outputs
~~~~~~~

String (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The decoded text string.

OcrInfo (Type: ``OcrCharacterInfo``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Additional information, such as bounding boxes and scores of segmented characters and the average score.

Comments
~~~~~~~~

The **OCR** node tries to segment characters in the input image. After segmentation, the segmented patterns are compared to teached characters from the font file. Finally, the relative position of the characters is analyzed and a text result is created. Additional information, such as the position of the segmented characters and their match scores is calculated.
