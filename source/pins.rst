Pins
====

**Nodes** may have **Input Pins** and/or **Output Pins**. **Pins** are typed.

|image0| |image1|

**Connections** can be made only between **Pins** with compatible **Types**.

.. figure:: ./images/connection_ok.png
   :alt: 

**Pins** are compatible, if their **Types** are equal or if the **Type** flowing on the connection can be converted automatically.

**Pins** with incompatible **Types** cannot be connected.

.. figure:: ./images/connection_nok.png
   :alt: 

.. |image0| image:: ./images/input_pin_type.png
.. |image1| image:: ./images/output_pin_type.png
