GetListItem
-----------

Takes a specific item out of a list.

.. figure:: images/GetListItemNode.png
   :alt: 

Inputs
~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The list.

Index (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The zero-based index of the item. If the index is out of range, an error will be shown.

Outputs
~~~~~~~

Item (Type: ``T``)
^^^^^^^^^^^^^^^^^^

The specific item of the list.

Sample
~~~~~~

Here is an example:

.. figure:: images/ListItemSample.png
   :alt: 


