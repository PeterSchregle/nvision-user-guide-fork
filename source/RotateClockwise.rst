Rotate Clockwise
----------------

Rotates an image clockwise by 90 degrees.

.. figure:: images/RotateClockwise.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

This function rotates an image by 90 degrees in clockwise direction.

.. figure:: images/RotateClockwiseImage.png
   :alt: 

Sample
~~~~~~

Here is an example that rotates an image.

.. figure:: images/RotateClockwiseSample.png
   :alt: 


