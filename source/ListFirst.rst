First
-----

Takes the first item out of a list.

.. figure:: images/ListFirstNode.png
   :alt: 

Inputs
~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The list.

Outputs
~~~~~~~

Item (Type: ``T``)
^^^^^^^^^^^^^^^^^^

The first item of the list.

Sample
~~~~~~

Here is an example:

.. figure:: images/ListItemSample.png
   :alt: 


