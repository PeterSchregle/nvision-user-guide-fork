nVision - Samples
=================

**nVision** samples can be loaded from this list by clicking on their name. Samples are grouped into the application related tasks of location, measurement, verification and identification. In addition there is a group that contains samples that explain various basic aspects of **nVision**.

Some samples use linear pipelines (the vertical flow of pipeline steps at the left of the Workbench window), others use sub-pipelines (you can reveal the contents of the sub-pipeline by dragging down the blue splitter bar at the top of the Workbench window) or both. Some sample show very basic things, others are complicated and you have to study them in order to understand, how they work and are implemented.

Locate
------

-  

   .. raw:: html

      <p>

   Template Search

   .. raw:: html

      </p>
        

   |image0| Uses a subpipeline to look for matches on an image for a given template. Different images can be processed by using "Process Images" under the ribbon tab "Pipeline".

Measure
-------

-  

   .. raw:: html

      <p>

   Blob Analysis

   .. raw:: html

      </p>
        

   |image1| Uses a vertical pipeline to perform a blob extraction.

-  

   .. raw:: html

      <p>

   Blob Analysis Border Handling

   .. raw:: html

      </p>
        

   |image2| Uses a subpipeline to perform a blob extraction. Blobs that are not completely inside the selected area will be discarded.

-  

   .. raw:: html

      <p>

   Custom Blob Analysis Feature and HTML export

   .. raw:: html

      </p>
        

   |image3| Shows how a custom blob feature can be implemented. In addition, shows how the results can be exported into an HTML file.

-  

   .. raw:: html

      <p>

   Custom Blob Analysis Feature and CSV export

   .. raw:: html

      </p>
        

   |image4| Basically the same example as the one before it, but performs CSV export instead.

Verify
------

-  

   .. raw:: html

      <p>

   Area Check

   .. raw:: html

      </p>
        

   |image5| Uses a subpipeline to create an interactive widget that will test if the part inside the selected region has the correct area.

-  

   .. raw:: html

      <p>

   Check Parts

   .. raw:: html

      </p>
        

   |image6| Using the "Locate Shape" tool, a reference system is created so then the tool "Contrast" can use it to perform a check always on the same part of the clamp, even when it is rotated. Different images can be processed by using "Process Images" under the ribbon tab "Pipeline".

-  

   .. raw:: html

      <p>

   Check Clamps

   .. raw:: html

      </p>
        

   |image7| Using the "Locate Shape" tool, a reference system is created so then the other tools can use it to perform a check always on the same part of the clamp, even when it is rotated. The tool "Distance" will check if the height of the clamp is correct and the tool "Brightness" will check if the metal insertion is present. Different images can be processed by using "Process Images" under the ribbon tab "Pipeline".

-  

   .. raw:: html

      <p>

   Check Nuts

   .. raw:: html

      </p>
        

   |image8| Uses a subpipeline to analyze if all screw nuts are present on a part (IO) and if there is some missing the part will be discarded (NIO). The tool "Distance" will check if the height of the clamp is correct and the tool "Brightness" will check if the metal insertion is present. Different images can be processed by using "Process Images" under the ribbon tab "Pipeline".

-  

   .. raw:: html

      <p>

   Rim Conveyor

   .. raw:: html

      </p>
        

   |image9| Identifies the kind of rim received using template search to search a match on a list of templates, the resul of the match will then be shown on a symple HMI structure. Different images can be processed by using "Process Images" under the ribbon tab "Pipeline".

Identify
--------

-  

   .. raw:: html

      <p>

   Screens Conveyor

   .. raw:: html

      </p>
        

   |image10| Gets the position and orientation of every smartphone screen on the picture and shows it on a display. Different images can be processed by using "Process Images" under the ribbon tab "Pipeline".

-  

   .. raw:: html

      <p>

   Barcode Annotation

   .. raw:: html

      </p>
        

   |image11| Uses a subpipeline to create an interactive widget that will decode a barcode inside the selected area.

-  

   .. raw:: html

      <p>

   OCR

   .. raw:: html

      </p>
        

   |image12| Uses a subpipeline to create an interactive widget that will test if a certain number combination fixed by the user appears inside the selected area.

-  

   .. raw:: html

      <p>

   Code 39

   .. raw:: html

      </p>
        

   |image13| Uses a vertical pipeline to decode a barcode

Basic
-----

-  

   .. raw:: html

      <p>

   Multi Threshold

   .. raw:: html

      </p>
        

   |image14| Uses a subpipeline to perform a double thresholding.

-  

   .. raw:: html

      <p>

   Blob Display

   .. raw:: html

      </p>
        

   |image15| Uses a subpipeline to perform a blob extraction and draw a widget on top of each detected blob.

-  

   .. raw:: html

      <p>

   Gauge Circles

   .. raw:: html

      </p>
        

   |image16| Uses a subpipeline to create an interactive widget that finds the best fitting circle inside a region.

-  

   .. raw:: html

      <p>

   Gauge Ellipses

   .. raw:: html

      </p>
        

   |image17| Uses a subpipeline to create an interactive widget that finds the best fitting circle inside a region.

-  

   .. raw:: html

      <p>

   Gauge Edges

   .. raw:: html

      </p>
        

   |image18| Uses a subpipeline to create an interactive widget that measures the distance between 2 edges.

-  

   .. raw:: html

      <p>

   Selective Search

   .. raw:: html

      </p>
        

   |image19| Uses a subpipeline to create an interactive widget. The pipeline then will find matches for the area selected with said widget and show them on screen.

-  

   .. raw:: html

      <p>

   Get environment variable

   .. raw:: html

      </p>
        

   |image20| Uses a subpipeline to create a Human Machine Interface that will allow the user to show the values of environment variables.

-  

   .. raw:: html

      <p>

   Optic Calculator

   .. raw:: html

      </p>
        

   |image21| Optics calculator with a nice GUI.

.. |image0| image:: images/template_search_icon.png
.. |image1| image:: images/grains_icon.png
.. |image2| image:: images/grains_no_borders_icon.png
.. |image3| image:: images/custom_blob.png
.. |image4| image:: images/custom_blob.png
.. |image5| image:: images/area_check_icon.png
.. |image6| image:: images/check_parts_icon.png
.. |image7| image:: images/check_clamps_icon.png
.. |image8| image:: images/check_nuts_icon.png
.. |image9| image:: images/rim_conveyor_icon.png
.. |image10| image:: images/screens_conveyor_icon.png
.. |image11| image:: images/barcode_annotation_icon.png
.. |image12| image:: images/ocr_card_icon.png
.. |image13| image:: images/code_39_icon.png
.. |image14| image:: images/multi_threshold_icon.png
.. |image15| image:: images/grains_widgets_icon.png
.. |image16| image:: images/gauge_circles_icon.png
.. |image17| image:: images/gauge_ellipses_icon.png
.. |image18| image:: images/gauge_edges_icon.png
.. |image19| image:: images/selective_search_icon.png
.. |image20| image:: images/get_environment_value_icon.png
.. |image21| image:: images/lens_calculator_icon.png
