Count Edges Tool
----------------

.. figure:: images/geometry_count_edges.png
   :alt: 

The **Count** tool counts the number of edges along a linear region of interest.

The tool displays graphical elements to specify the region of interest as well as to display the results.

It displays a yellow line that specifies the search line. You can move the line around on the image by dragging it. You can also pick either of the line endpoints to move just this endpoint.

Once the line has been positioned and finds edges, it visualizes them as blue points and it outputs their number. If the number is within expected bounds, the tool color is green, otherwise it is red.

The tool has a configuration panel, which can be used to set parameters.

The **Min** and **Max** parameters are used to set the expected minimum and maximum diameter.

The parameters in the **Edge Detection Parameters** affect the edge detection.

**Smoothing** is the amount of smoothing used in edge detection (default: 2.7). **Strength** is the minimum strength of an edge (the gray scale slope of the edge, default = 15). Smoothing and Strength are interrelated: the more you smooth the more you lessen the strength of an edge and vice versa.
