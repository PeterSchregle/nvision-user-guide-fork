HMI ListBox (Single Select)
---------------------------

A single select **ListBox** can be used to select one from a number of entries.

.. figure:: images/HMISingleSelectListBoxNode.png
   :alt: 

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **ListBox**.

The **ListBox** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding*, *Foreground*, *Background*, *Font* and *Padding* styles.

Items (Type: ``List<string>``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A list of text items that is displayed in the **ListBox**.

Index (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The initial selection of the **ListBox**. This is a zero-based index. You can use ``-1`` to specify *no selection*. This value is used only when the *Items* pin is connected.

Outputs
~~~~~~~

SelectedItem (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The selected item.

SelectedIndex (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The zero-based index of the selected item, or -1 for *no selection*.

Example
~~~~~~~

Here is an example that shows a **ListBox**. This definition:

.. figure:: images/hmi_singleselect_listbox_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_singleselect_listbox.png
   :alt: 


