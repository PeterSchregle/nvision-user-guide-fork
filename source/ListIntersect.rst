Intersect
---------

Calculates the intersection of two sets.

.. figure:: images/ListIntersectNode.png
   :alt: 

Inputs
~~~~~~

ListA (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The first input list, whose distinct elements form the first set for the intersection.

ListB (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The second input list, whose distinct elements form the second set for the union.

Outputs
~~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The output list, which consists of the set intersection of the two lists.

Comments
^^^^^^^^

The lists are treated as mathematical sets, which can have distinct objects only.
