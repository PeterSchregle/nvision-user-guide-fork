Gauging
=======

**nVision** can be used to measure positions of edges in images. It uses projections along lines, within rectangles or along other curves or within other shapes. The projections are analyzed and edges can be measured fast and with subpixel accuracy. The precision that you can achieve depends on the characteristics of your system (noise in the image, sharpness of the edges, etc.), but in general you can expect 1/20th of a pixel precision with an optimal setup.

The first step in measuring edges is the selection of an area of interest. This can be a line segment, an oriented rectangle, or even a circular ring. From this area of interest, an intensity profile is calculated. Usually you would use real image intensities, but you can also measure edges in a different color space, if you are interested in the location of a color change.

The intensity profile is then smoothed in order to reduce noise and then the gradient is calculated. The extrema within the gradient are searched and their strength is compared to a threshold. If the extrama fall below the threshold, they are discarded; otherwise the extrema represent real edges. The position of the edge is then calculated with subpixel precision, as well as the interpolated grey and gradient values at this location.

Here is the full edge information visualized within a tool.

.. figure:: images/gauging.png
   :alt: 

 
