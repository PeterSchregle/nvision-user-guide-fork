Transform (Select)
------------------

**Transform (Select)** takes items from a list and transforms them to a list of different items. The transformation is specified as a grafical pipeline, for a single item. All transformed items are added to the output list.

.. figure:: images/TransformNode.png
   :alt: 

Outside View
~~~~~~~~~~~~

At the outside, the **Transform (Select)** node appears like a single node, with inputs and outputs.

Inputs
^^^^^^

List (Type: ``List<T>``)
''''''''''''''''''''''''

The input list.

Outputs
^^^^^^^

TransformedList (Type: ``List<U>``)
'''''''''''''''''''''''''''''''''''

The output list, consisting of a list of transformed itmes from the input list.

Inside View
~~~~~~~~~~~

Inside, the **Transform (Select)** node looks like a normal branched pipeline. This pipeline is used to specify the transformation, which is evaluated for each item.

.. figure:: images/TransformNodeInside.png
   :alt: 

Inputs
^^^^^^

Item (Type: ``T``)
''''''''''''''''''

One item of the input list.

ItemIndex (Type: ``Int32``)
'''''''''''''''''''''''''''

The index of the item of the input list.

Outputs
^^^^^^^

Result (Type: ``U``)
''''''''''''''''''''

The result item. The result item can be of the same type as the input item or of a different type.

Comments
~~~~~~~~

**Transform (Select)** nodes can be nested in similar ways as subpipelines.

The nesting is shown with a breadcrumbs control at the top.

.. figure:: images/breadcrumbs.png
   :alt: 

Double-click a **Transform (Select)** node to enter it, and use the breadcrumbs control to exit it and go up one or more levels.

By default, a **Transform (Select)** node has one input and one output.

You can add additional inputs with the **Add Input (Ctrl-I)** command. You can edit the port names by clicking on them and typing a new name. The type of the ports is determined by the first connection made to them, either from the outside or from the inside.

Sample
~~~~~~

Here is an example:

.. figure:: images/TransformSample.png
   :alt: 

.. figure:: images/TransformSample2.png
   :alt: 

It takes items from the first list (0, 1, ..., 9) and transforms it to a list with each item squared (0, 1, ..., 81).
