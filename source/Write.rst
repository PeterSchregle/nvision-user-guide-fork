Write
-----

Write data to an interface.

.. figure:: images/WriteNode.png
   :alt: 

This node writes data to an interface (such as a serial port).

Inputs
~~~~~~

Io (Type: ``IoResource``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The IO interface.

Data (Type: ``DataList``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The data to write in the form of a DataList (a list of bytes).

Sync (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^

This input can be connected to any other object. It is used to establish an order of execution.

Outputs
~~~~~~~

Sync (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^

Synchronizing object. It is used to establish an order of execution.

Comments
~~~~~~~~

The **Sync** input and output can be used to establish an order of execution.

Sometimes, if this is necessary for technical reasons, you can add a **Delay** node in order to introduce additional delay between synchronized nodes.
