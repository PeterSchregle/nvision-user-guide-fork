HMI StackPanel
--------------

The **StackPanel** stacks its child controls in the vertical or horizontal direction.

.. figure:: images/HMIStackPanelNode.png
   :alt: 

At the bottom of the **StackPanel** node is a pin that allows it to connect several children.

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **StackPanel**.

The **StackPanel** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin* and *Background* styles.

Orientation (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Orientation of the **StackPanel**, ``Horizontal`` or ``Vertical``.

Background (Type: ``SolidColorBrushByte``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The background of the **StackPanel**.

Example
~~~~~~~

Here is an example that stacks a text label and an edit control vertically. This definition

.. figure:: images/hmi_stack_def.png
   :alt: 

creates the following úser interface:

.. figure:: images/hmi_stack.png
   :alt: 


