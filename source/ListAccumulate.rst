Accumulate
----------

**Accumulate** takes all items from a list and combines them into a single result. Calculating the sum, or the minimum or the maximum are examples for accumulation. The accumulation is specified as a grafical pipeline, for a single item, which combines a single item with the accumulation so far.

.. figure:: images/AccumulateNode.png
   :alt: 

Outside View
~~~~~~~~~~~~

At the outside, the **Accumulate** node appears like a single node, with inputs and outputs.

Inputs
^^^^^^

List (Type: ``List<T>``)
''''''''''''''''''''''''

The input list.

Init (Type: ``U``)
''''''''''''''''''

The initial accumulator value.

Outputs
^^^^^^^

Result (Type: ``U``)
''''''''''''''''''''

The resulting accumulator value.

Inside View
~~~~~~~~~~~

Inside, the **Accumulate** node looks like a normal branched pipeline. This pipeline is used to specify the accumulation, which is evaluated for each item.

.. figure:: images/AccumulateNodeInside.png
   :alt: 

Inputs
^^^^^^

Item (Type: ``T``)
''''''''''''''''''

One item of the input list.

ItemIndex (Type: ``Int32``)
'''''''''''''''''''''''''''

The index of the item of the input list.

Accumulator (Type: ``U``)
'''''''''''''''''''''''''

The input accumulator value.

Outputs
^^^^^^^

Accumulator (Type: ``U``)
'''''''''''''''''''''''''

The resulting accumulator value.

Comments
~~~~~~~~

**Accumulate** nodes can be nested in similar ways as subpipelines.

The nesting is shown with a breadcrumbs control at the top.

.. figure:: images/breadcrumbs.png
   :alt: 

Double-click an **Accumulate** node to enter it, and use the breadcrumbs control to exit it and go up one or more levels.

By default, a **Accumulate** node has three inputs and one output.

You can add additional inputs with the **Add Input (Ctrl-I)** command. You can edit the port names by clicking on them and typing a new name. The type of the ports is determined by the first connection made to them, either from the outside or from the inside.
