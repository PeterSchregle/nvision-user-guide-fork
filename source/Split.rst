Split
-----

Splits an image in two halfs.

.. figure:: images/SplitNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Direction (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The direction to split the image. This can be ``Horizontal``, ``Vertical`` or ``Planar`` to split in the X, Y, or Z directions, respectively.

Size (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^

The split position as a number between 0 and 1 (both bounds are exclusive). A value of 0.5 splits the image in half.

Outputs
~~~~~~~

A (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^

The first output image.

B (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^

The second output image.

Comments
~~~~~~~~

This function splits an image in two parts, in either the horizontal, vertical or planar directions.

Sample
~~~~~~

Here is an example that extracts the three color channels from an RGB image.

.. figure:: images/SplitSample.png
   :alt: 


