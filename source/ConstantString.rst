Text
----

Create and set a text value.

.. figure:: images/ConstantStringNode.png
   :alt: 

Outputs
~~~~~~~

Value (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^

The text value.

Comments
~~~~~~~~

This node provides the means to create and set a text value.

The value can be changed by typing the text inside the edit field in the node.

You can insert a few escape sequences to enter non-printable characters into the string.

+-------------------+---------------------------+
| escape sequence   | non-printable character   |
+===================+===========================+
| ``\t``            | tab                       |
+-------------------+---------------------------+
| ``\n``            | newline                   |
+-------------------+---------------------------+
| ``\r``            | carriage return           |
+-------------------+---------------------------+

The title of the node can be edited.
