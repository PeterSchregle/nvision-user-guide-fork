Split
-----

Splits text into a list of strings that contains the substrings that are delimited by elements of a specified text.

.. figure:: images/SplitString.png
   :alt: 

Inputs
~~~~~~

In
^^

The text that shall be split into parts.

Separator
^^^^^^^^^

The separator characters. The default separator characters contain the space, comma, semicolon, colon and dash charaters (``,;:-``).

If this input is empty, the string will be split at all whitespace characters.

To split the string at line ends, you can use ``\n``.

To split the string at tabs, you can use ``\t``.

Outputs
~~~~~~~

Out
^^^

A list of strings that are the substrings of the original strings.

Comments
~~~~~~~~

The **Split** node is often used to break text into pieces that is read from file.

Since the output is a list, you can use the **GetListItem** node to pick out a specific item from the list.

Sample
~~~~~~

Here is a sample pipeline that illustrates **Split**:

.. figure:: images/SplitStringSample.png
   :alt: 


