Outer Boundary
--------------

Extracts the outer boundary from a list of regions.

.. figure:: images/OuterBoundaryNode.png
   :alt: 

The outer boundary of a region consists of the pixels of the background that touch the object. The outer boundary lies on the background.

Inputs
~~~~~~

Regions (Type: ``RegionList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of regions.

Outputs
~~~~~~~

Boundary (Type: ``RegionList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The objects reduced to their outer boundary.

Comments
^^^^^^^^

Here is an example of an outer boundary:

.. figure:: images/OuterBoundary.png
   :alt: 


