Skip
----

Skips a number of items at the beginning of a list. The remaining items are put in the output list.

.. figure:: images/SkipNode.png
   :alt: 

Inputs
~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The input list.

Count (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The number of items to skip.

Outputs
~~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The output list, consisting of the part of the input list that remains after skipping.
