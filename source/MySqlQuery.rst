|image0| Sql Query
------------------

Executes a query on a database connection.

.. figure:: images/MySqlQueryNode.png
   :alt: 

Inputs
~~~~~~

Connection (Type: ``MySql.Data.MySqlClient.MySqlConnection``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The database connection.

Query (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^

The query string.

Sync (Type: ``Object``)
^^^^^^^^^^^^^^^^^^^^^^^

This input can be connected to any other object. It is used to establish an order of execution.

Outputs
~~~~~~~

Rows (Type: ``List<List<String>>``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The result of the query.

Comments
~~~~~~~~

This command allows you to execute any query on a database.

See the `MySql documentation <https://dev.mysql.com/doc/>`__ for more information about how to construct SQL queries.

The `MySql tutorial <https://dev.mysql.com/doc/refman/5.7/en/entering-queries.html>`__ may help you in constructing query strings.

Here is an example taken from this tutorial that shows how to build a query ('SELECT VERSION(), CURRENT\_DATE;') and how to read the results. The example assumes that you have MySql installed and that you can connect to the ``sakila`` sample database.

.. figure:: images/MySqlQuerySample.png
   :alt: 

.. |image0| image:: images/db_query.png
