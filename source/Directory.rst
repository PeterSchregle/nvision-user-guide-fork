Directory
---------

Selects a directory.

.. figure:: images/DirectoryNode.png
   :alt: 

Inputs
~~~~~~

Directory (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The directory (provided as a string, or selected via the ... button.

Outputs
~~~~~~~

Directory (Type: ``Ngi.Path``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The directory as a path.

Comments
~~~~~~~~

The purpose of this node is to make it easy for the user to select a directory from the filesystem.
