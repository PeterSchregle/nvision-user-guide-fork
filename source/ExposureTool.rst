Exposure Tool
-------------

.. figure:: images/exposure_control.png
   :alt: 

The **Exposure** tool is used to set the exposure time of a camera. It works with **GenICam** cameras that have an exposure time parameter.

The tool displays the live image and shows over-exposed pixels (> 250 greyvalues by default) in yellow color and under-exposed pixels < 5 greyvalues by default) in blue color. This gives you a quick visual impression of the illumination/exposure of an image. In addition, the percentage of underflow and overflow pixels are displayed, as well as a histogram that shows the overall brightness distribution.

The **Exposure** tool has a configuration panel, which can be used to set parameters.

The **Exposure Time (ms)** is the exposure time in ms (milliseconds). The tool uses GenICam to write the exposure time to the camera, which has to be connected to the global **Camera** port in the system globals.

**Underflow** and **Overflow** show the respective pecentages of the underflow and overflow pixels.

When the exposure time is changed, the blue and yellow image overlays, the percentage values and the histogram graphic are all affected and change. The graphic and the numeric outputs help you to property set illumination or exposure interactively.

The **Parameters** group has seldomly used additional parameters:

The **Underflow Threshold** sets the threshold for detection of underflow pixels (by default this is set to ``5``).

The **Overflow Threshold** sets the threshold for detection of overflow pixels (by default this is set to ``250``).

The **GenICam Parameter Name** sets the parameter name of the GenICam property that is used to set the exposure time. Usually that is ``ExposureTime``, but some cameras use a different name, such as ``ExposureTimeAbs``. You can find out the proper name by inspecting the **Camera Parameters** window (you can open it with the **Show Camera Parameters** command on the **Home** tab of the ribbon in the **Tool Windows** section).
