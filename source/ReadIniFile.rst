Read Ini File
-------------

Reads values from an Ini file. Ini files are used to store configuration information.

.. figure:: images/ReadIniFile.png
   :alt: 

Inputs
~~~~~~

Filename
^^^^^^^^

The path to the file.

Section
^^^^^^^

The section name in the Ini file.

Key
^^^

The key name inside the section.

Default
^^^^^^^

The default value, if the key could not be found.

Outputs
~~~~~~~

Text
^^^^

The value read from the Ini file.

Comments
~~~~~~~~

Wikipedia says the following about Ini files: The Ini file format is an informal standard for configuration files for some platforms or software. Ini files are simple text files with a basic structure composed of sections, properties, and values.

The basic element contained in an Ini file is the key or property. Every key has a name and a value, delimited by an equals sign (=). The name appears to the left of the equals sign.

::

    name=value

Keys may (but need not) be grouped into arbitrarily named sections. The section name appears on a line by itself, in square brackets ([ and ]). All keys after the section declaration are associated with that section. There is no explicit "end of section" delimiter; sections end at the next section declaration, or the end of the file. Sections may not be nested.

::

    [section]
    a=10
    b=20

Section and property names are not case sensitive in the Windows implementation.

The Read Ini File checks the time the file was last written to, and compares it with the time it last read from the file. If the file has been written to after it has been last read, the node reads the file again.

Sample
~~~~~~

Here is a sample pipeline that illustrates **ReadIniFile**:

.. figure:: images/ReadIniFileSample.png
   :alt: 


