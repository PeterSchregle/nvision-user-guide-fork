ImageTranslate
--------------

Translates an image by a translation vector.

.. figure:: images/ImageTranslateNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Translation (Type: ``Ngi.VectorDouble``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The translation vector.

Filter (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The geometric interpolation. Available values are ``NearestNeighbor``, ``Box``, ``Triangle``, ``Cubic``, ``Bspline``, ``Sinc`` , ``Lanczos`` and ``Kaiser``. The accuracy of the interpolation increases from ``NearestNeighbor`` to ``Kaiser``, but the performance decreases. The default is set to ``Box``.

Extend Size (Type: ``bool``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The geometric transformation may produce pixels that are outside of the input image bounds. If the parameter is set, the size of the output image is adapted to make room for these pixels. If the parameter is cleared, the size of the output image is kept the same as the input image.

Outputs
~~~~~~~

Translated (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The output image.
