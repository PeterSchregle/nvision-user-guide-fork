Identify
--------

The **Identify** tab groups commands that can read barcodes or text.

The **Edit** group contains commands for pipeline editing.

.. figure:: images/RibbonEdit.png
   :alt: 

The first command deletes a node in the linear pipeline. The second command commits the result of the selected node to the browser, where it can be stored into the filesystem. These two commands are replicated on most tabs of the ribbon.

The **Codes** group contains commands for part identification with barcodes or matrix codes.

.. figure:: images/RibbonCodes.png
   :alt: 

The **Barcode** and **Matrixcode** commands can be used to decode or verify 1-dimensional barcodes or 2-dimensional matrix codes of various symbologies.

The **Text** group contains the command for part identification with text.

.. figure:: images/RibbonText.png
   :alt: 

The **OCR** command can be used to decode or verify text.

The tools are meant to be chained one after the other, where one of the location tools is often the first tool in a chain of tools.
