Vertical Scharr
---------------

Filters an image using a vertical scharr kernel.

.. figure:: images/VerticalScharrNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies an optional area of interest.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

::

     3 0  -3
    10 0 -10
     3 0  -3

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

Here are a few results of the horizontal sobel filter with increasing kernel sizes:

Original:

.. figure:: images/GaussianOriginal.png
   :alt: 

Result:

.. figure:: images/VerticalScharr.png
   :alt: 

Sample
~~~~~~

Here is an example that shows how to use the vertical scharr filter.

.. figure:: images/VerticalScharrSample.png
   :alt: 

Often, you want to combine the horizontal and the vertical scharr filter to create an edge map. A simple way to do this is shown in the following sample:

.. figure:: images/ScharrEdgeMapSample.png
   :alt: 

Here is an example image of such an edge map:

.. figure:: images/ScharrEdgeMap.png
   :alt: 


