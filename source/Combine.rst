Combine
-------

Combines two images to form a combined image.

.. figure:: images/CombineNode.png
   :alt: 

Inputs
~~~~~~

A (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^

The first input image.

B (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^

The second input image.

Direction (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The direction to combine the image. This can be ``Horizontal``, ``Vertical`` or ``Planar`` to combine in the X, Y, or Z directions, respectively.

The other dimensions of the image must match, otherwise the combine node issues and error message.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The combined output image.

Comments
~~~~~~~~

This function combines an image in two parts, in either the horizontal, vertical or planar directions.

Sample
~~~~~~

Here is an example:

.. figure:: images/CombineSample.png
   :alt: 


