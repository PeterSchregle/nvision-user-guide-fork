|image0| Sql Select
-------------------

Selects data from a database table.

.. figure:: images/MySqlSelectNode.png
   :alt: 

Inputs
~~~~~~

Connection (Type: ``MySql.Data.MySqlClient.MySqlConnection``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The database connection.

QueryTail (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The node constructs a query consisting of a Sql Select statement, such as ``SELECT column FROM table``. The contents of the query tail is appended to this string and allows you to add clauses like ``WHERE ...``, ``ORDER BY ...``, etc. You do not need to add a semicolon, this will be appended automatically.

Sync (Type: ``Object``)
^^^^^^^^^^^^^^^^^^^^^^^

This input can be connected to any other object. It is used to establish an order of execution.

Outputs
~~~~~~~

Sync (Type: ``Object``)
^^^^^^^^^^^^^^^^^^^^^^^

This output can be connected to any other object. It is used to establish an order of execution.

Comments
~~~~~~~~

Once the database connection has been established, the node will fill its selection combo box with the tables that are available in the database. Once you have selected the desired table, the node will be populated dynamically with more inputs of type ``Boolean``, which correspond to the column names of the selected table. You can either select ``1`` (true) or ``0`` (false) or connect to outputs from elsewhere in the pipeline. If connections are made, you cannot change the table inside the node, because this could break existing connections.

See the `MySql documentation <https://dev.mysql.com/doc/>`__ for more information about how to construct SQL select statements.

The following example assumes that you have MySql installed and that you can connect to the ``sakila`` sample database.

.. figure:: images/MySqlSelectSample.png
   :alt: 

.. |image0| image:: images/db_select.png
