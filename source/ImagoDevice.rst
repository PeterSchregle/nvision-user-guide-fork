Imago Device
------------

A device implementation.

.. figure:: images/ImagoDeviceNode.png
   :alt: 

Inputs
~~~~~~

System (Type: ``VIB_NET.VIBSystem``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The factory instance.

Index (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The index of the device. The device itself is selected with the combobox inside the node.

Outputs
~~~~~~~

Device
^^^^^^

The device. The specifiy type depends on the device type selected within the node.

Comments
~~~~~~~~

The first step in using any Imago device functionality is to create the system. With a system, you can then create an Imago device, and with a device you can call any method or property on it to carry out the respective operation.

For a more detailed description see the Imago .NET documentation (VIBSystem::OpenDevice method).
