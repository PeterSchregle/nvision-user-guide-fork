Match Pattern Tool
------------------

.. figure:: images/features_match_pattern.png
   :alt: 

The **Match Pattern** tool locates a part using geometric pattern matching.

The tool displays graphical elements to specify the region of interest as well as to display the results.

The tool displays a yellow region of interest that you can drag around to specify both the pattern as well as the search region of the pattern. You can move the box around on the image by dragging its inside. You can also resize the box by picking it at its boundary lines (on the boundary line or just a bit outside) or at its corner points (on the corner point of just a bit outside).

Usually, the box is put tightly around an area where you expect a specific pattern. Once the box has been positioned, make sure that the filename in the **Template File** edit control is correct and press **Teach**. This saves the pattern.

When the pattern has been taught, it may be beneficial to make the search region bigger, so that there is some tolerance in finding the pattern.

If the tool finds the pattern, it displays the match score (between 0 and 1, where 1 is a perfect match). Scores below 0.5 are mostly meaningless. Please note that the characteristic of the score is different to the one used in the **Match Contour** tool.

If the tool cannot find the pattern at all, or if the score is too low, it outputs the text "No Match" in red.

The tool has a configuration panel, which can be used to set parameters.

The **Template File** parameter specifies the match template, which is loaded from disk. If the template exists, it is displayed here.

The **Min Score** parameter is used to set the minimum score for an acceptable match (default = 0.5). Matches below this score are not considered.

The **Teach** button saves the pattern to the disk.
