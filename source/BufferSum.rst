Sum
---

Calculates the sum of all pixels in a buffer.

.. figure:: images/BufferSumNode.png
   :alt: 

Inputs
~~~~~~

Buffer (Type: ``Buffer``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input buffer.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

An optional region.

Outputs
~~~~~~~

Sum (Type: Object)
^^^^^^^^^^^^^^^^^^

The sum.

Comments
~~~~~~~~

The **Sum** node calculates the sum of all pixel values of a buffer.

If the *Region* input is connected, the calculation of the sum is constrained to the pixels within the region only, otherwise the whole buffer is used.

If a color buffer is used, the sum is calculated channel-wise.

Sample
~~~~~~

Here is an example that calculates the sum of an image.

.. figure:: images/BufferSumSample.png
   :alt: 


