GetPixelValue
-------------

Reads a pixel value from a buffer.

.. figure:: images/GetPixelValueNode.png
   :alt: 

Inputs
~~~~~~

Buffer
^^^^^^

The buffer to read from. This can be an image, or a palette, histogram, profile, etc.

Index3d
^^^^^^^

The three-dimensional index of the pixel.

Outputs
~~~~~~~

PixelValue
^^^^^^^^^^

The value of the pixel in the buffer.

Comments
~~~~~~~~

Buffers are three-dimensional: x is the horizontal dimension (the positive x-axis extents to the right), y is the vertical dimension (the positive y-axis extends downwards) and z is the planar dimension (the positive z-axis extends into the paper/screen plane). The origin is at the upper left corner.

Buffers have a variant type: they can be greyscale or color (in different color spaces) with different numeric types (8 bit unsigned, 16 bit unsigned, 32 bit signed, or 64 bit float per channel).

Buffers come in various forms: images are most common, but palettes, histograms or profiles are also buffers.

Sample
~~~~~~

Here is a sample pipeline that illustrates **GetPixelValue**:

.. figure:: images/GetPixelValueSample.png
   :alt: 


