HMI Separator
-------------

The **Separator** allows you to specify a separator inside a toolbar.

.. figure:: images/HMISeparatorNode.png
   :alt: 

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **Separator**.
