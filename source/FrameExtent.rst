Frame (Extent)
--------------

Puts a frame around an image.

.. figure:: images/FrameExtent.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Size (Type: ``Extent3d``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The frame size.

Framing (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The framing method (``Nearest``, ``Reflective (Middle)``, ``Reflective (Border)``, \`Periodic\`\`).

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

The **Frame (Extent)** node puts a border around the input image.

The border can be put around in various ways. *Nearest* takes the nearest pixel value to the image border and repeats it.

.. figure:: images/FrameExtentNearest.png
   :alt: 

*Reflective (Middle)* reflects the image at the middle of the border pixel.

.. figure:: images/FrameExtentReflectiveMiddle.png
   :alt: 

*Reflective (Border)* reflects the image at the border of the border pixel.

.. figure:: images/FrameExtentReflectiveBorder.png
   :alt: 

*Periodic* periodically repeats the image.

.. figure:: images/FrameExtentPeriodic.png
   :alt: 


