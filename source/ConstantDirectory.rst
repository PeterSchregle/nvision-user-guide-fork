Directory
---------

Create and set a directory name.

.. figure:: images/ConstantDirectoryNode.png
   :alt: 

Outputs
~~~~~~~

Directory (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The directory name.

Comments
~~~~~~~~

This node provides the means to create and set a directory name.

The value can be changed by typing the text inside the edit field in the node, or by selecting a directory using the [...] button.

The title of the node can be edited.
