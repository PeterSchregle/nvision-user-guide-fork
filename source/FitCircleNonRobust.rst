Fit Circle
----------

Fits a circle to a set of three or more points.

.. figure:: images/FitCircleNode.png
   :alt: 

Inputs
~~~~~~

Points (Type: ``PointList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of points.

Outputs
~~~~~~~

Circle (Type: 'Circle')
^^^^^^^^^^^^^^^^^^^^^^^

The fitted circle.

Comments
~~~~~~~~

The **Fit Circle** node fits a circle to a set of three or more points. It implements the circle fit by Taubin.

See Also
~~~~~~~~

There is also the **Robust Fit Circle** node, which uses RANSAC and rejects outliers.

Sample
~~~~~~

Here is an **example** that detects several points in an image and fits a circle to the set of points. The **example** allows you to interactively explore the parameters of the **Robust Fit Circle** node.

You can click on the **hyperlink** to load the sample into **nVision**.
