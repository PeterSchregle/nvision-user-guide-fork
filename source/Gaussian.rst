Gaussian
--------

Filters an image using a gaussian kernel. A gaussian filter is a lowpass and blurs an image. The amount of blurring is determined by the parameter sigma, which in turn determines the size and the parameters of the convolution kernel.

.. figure:: images/GaussianNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies an optional area of interest.

Sigma (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

Determines the size of the kernel.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation (at least 5x5, i.e. sigma > 5/3). It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma determines to the width of the bell-shaped curve and

.. math:: r = (x^2 + y^2)

is the distance from the center:

.. math:: G(x, y) = e ^ -(x^2 + y^2)/(2*sigma^2)

Here are a few results of the gaussian filter with increasing sigma values:

Original:

.. figure:: images/GaussianOriginal.png
   :alt: 

Sigma = 2:

.. figure:: images/Gaussian2.png
   :alt: 

Sigma = 4:

.. figure:: images/Gaussian4.png
   :alt: 

Sigma = 8:

.. figure:: images/Gaussian8.png
   :alt: 

Sigma = 16:

.. figure:: images/Gaussian16.png
   :alt: 

Sigma = 32:

.. figure:: images/Gaussian32.png
   :alt: 

Sigma = 64:

.. figure:: images/Gaussian64.png
   :alt: 

Sample
~~~~~~

Here is an example that shows how to use the gaussian filter.

.. figure:: images/GaussianSample.png
   :alt: 


