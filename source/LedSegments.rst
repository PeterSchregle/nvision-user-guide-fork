HMI LedSegments
---------------

An **LedSegments** can be used to display text in a segmented led-style display.

.. figure:: images/HMILedSegmentsNode.png
   :alt: 

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **Led**.

The **WebBrowser** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment* and *Margin* styles.

Text (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^

The text.

Length (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^

The number of characters in the display.

State (Type: ``boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The **LedSegments** is one if this is ``True``, the **LedSegments** is off otherwise.

Foreground (Type: ``SolidColorBrush``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The color of the **LedSegments**.

Example
~~~~~~~

Here is an example that shows an **LedSegments**. This definition:

.. figure:: images/hmi_led_segments_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_led_segments.png
   :alt: 


