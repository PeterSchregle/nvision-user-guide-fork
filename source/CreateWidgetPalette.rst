Palette
-------

The **Palette** node is used to display a palette.

.. figure:: images/WidgetPaletteNode.png
   :alt: 

At the top of the **Palette** node is a pin that allows it to connect to a parent widget. The palette will be displayed only, if a parent connection is made.

At the bottom of the **Palette** node is a pin that allows it to connect children to the palette widget. The parent, grandparent, children and grandchildren define the layering of the widgets and the graphical outcome.

Children are layered on top of the parent.

Inputs
~~~~~~

Palette (Type: ``Palette``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The palette to display inside the widget.

Box (Type: ``BoxDouble``)
^^^^^^^^^^^^^^^^^^^^^^^^^

A position for the palette display.

Orientation (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Orientation of the palette display: *LeftToRight*, *RightToLeft*, *TopToBottom* or *BottomToTop*.

Alpha (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

The opacity of the palette display.

Example
~~~~~~~

Here is an example that displays an palette on top of an image. This pipeline

.. figure:: images/PaletteDisplayExample.png
   :alt: 

shows the following result:

.. figure:: images/PaletteDisplayExample2.png
   :alt: 


