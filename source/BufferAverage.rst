Average
-------

Calculates the average value of all pixels in a buffer.

.. figure:: images/BufferAverageNode.png
   :alt: 

Inputs
~~~~~~

Buffer (Type: ``Buffer``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input buffer.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

An optional region.

Outputs
~~~~~~~

Average (Type: Object)
^^^^^^^^^^^^^^^^^^^^^^

The average value.

Comments
~~~~~~~~

The **Average** node calculates the average value of a buffer. It sums all pixels and divides by the number of pixels.

If the *Region* input is connected, the calculation of the average is constrained to within the region only, otherwise the whole buffer is used.

If a color buffer is used, the average is calculated channel-wise.

Sample
~~~~~~

Here is an example that calculates the average of an image.

.. figure:: images/BufferAverageSample.png
   :alt: 


