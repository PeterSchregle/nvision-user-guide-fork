MorphologicalRegionListOperation
--------------------------------

Applies a morphological operation to all regions in a list.

.. figure:: images/MorphologicalRegionListOperatorNode.png
   :alt: 

The following operators are available: erosion, dilation, closing, opening and morphological gradient.

Erosion
       

Calculates the erosion of regions in a list (regions are all the holes in the part).

.. figure:: images/regions_erosion.png
   :alt: 

(Erosion with a circular structuring element of radius 10).

Dilation
        

Calculates the erosion of regions in a list (regions are all the holes in the part).

.. figure:: images/regions_dilation.png
   :alt: 

(Dilation with a circular structuring element of radius 10).

Closing
       

Calculates the closing of regions in a list (regions are all the holes in the part).

.. figure:: images/regions_closing.png
   :alt: 

(Closing with a circular structuring element of radius 10).

Opening
       

Calculates the opening of regions in a list (regions are all the holes in the part).

.. figure:: images/regions_opening.png
   :alt: 

(Opening with a circular structuring element of radius 10).

Morphological Gradient
                      

Calculates the morphological gradient of regions in a list (regions are all the holes in the part).

.. figure:: images/regions_morphological_gradient.png
   :alt: 

(Morphological gradient with a circular structuring element of radius 5).

Inputs
~~~~~~

Regions (Type: ``RegionList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of regions.

Operator (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the operator (``Erosion``, ``Dilation``, ``Closing``, ``Opening`` or ``MorphologicalGradient``).

Structel (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The structuring element (in the form of a region).

Outputs
~~~~~~~

Regions (Type: ``RegionList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The resulting list of regions.
