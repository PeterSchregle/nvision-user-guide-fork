Define Scale Tool
-----------------

.. figure:: images/define_scale.png
   :alt: 

The **Define Scale** tool is used to define an overall scale in the image. You can use it if your optical axis is orthogonal to the measurement plane, and if the desired measurement accuracy is not too high.

The tool calculates a calibration factor that is used to convert pixel measurements to world units, such as µm, mm, cm or m (or any other spatial unit that is needed in your application). Once a calibration has been established, other length measurement tools respect the calibration and output their measurments in world unit (as opposed to image units in pixels).

The **Define Scale** tool can use a line to establish a scale. Alternatively, it can use a circle to establish a scale. Alternatively, if the image has been acquired with a scanner, you can select the DPI setting to establish a scale.

The **Define Scale** tool has a configuration panel where you can select whether you want to use a line or a circle for setting the scale, or where you can select the DPI setting of the image.

Once you have aligned the line or circle with a know distance in your image, type the distance as well as the unit and press the **Teach** button.
