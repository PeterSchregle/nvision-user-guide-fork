Robust Fit Ellipse
------------------

Fits an ellipse to a set of five or more points. The fit is a robust one, i.e. it detects outliers, which are not used in the fit. Besides the fitted ellipse, the node also returns the set of inliers (i.e. the points that have been used to fit the ellipse) as well as the outliers (i.e. the points that have been rejected and were not used to fit the ellipse).

.. figure:: images/RobustFitEllipseNode.png
   :alt: 

Inputs
~~~~~~

Points (Type: ``PointList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of points.

Sigma (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

This is the standard deviation of the measurment error (default = 1.0). It affects the behaviour of the RANSAC algorithm.

Alpha (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

This is the probabilty that at least one of the random samples of points is free from outliers (default = 0.95). It affects the behaviour of the RANSAC algorithm.

Outputs
~~~~~~~

Ellipse (Type: 'Ellipse')
^^^^^^^^^^^^^^^^^^^^^^^^^

The fitted ellipse.

Inliers (Type: 'PointList')
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The points that are considered inliers, and are therefore used to fit the ellipse.

Outliers (Type: 'PointList')
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The points that are considered outliers, and do not take part in the ellipse fit.

Comments
~~~~~~~~

The **Robust Fit Ellipse** node fits an ellipse to a set of three or more points. It implements the ellipse fit by Fitzgibbon. In addition, it used the RANSAC algorithm to detect outliers. The ellipse is fitted to the inliers only.

See Also
~~~~~~~~

There is also the **Fit Ellipse** node, which does not use RANSAC and fits the ellipse to all points.

Sample
~~~~~~

Here is an **example** that detects several points in an image and fits a ellipse to the set of points. The **example** allows you to interactively explore the parameters of the **Robust Fit Ellipse** node.

You can click on the **hyperlink** to load the sample into **nVision**.
