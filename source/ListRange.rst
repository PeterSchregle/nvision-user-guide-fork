ListRange
---------

Creates a list of increasing integer numbers.

.. figure:: images/ListRangeNode.png
   :alt: 

By default the list starts at ``0`` and has ``1`` entry.

The ListRange node is often used to provide the equivalent of a loop in **nVision's** dataflow pipelines.

Inputs
~~~~~~

Start (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The start of the list of numbers.

Count (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The count of increasing numbers.

Outputs
~~~~~~~

Range (Type: ``List<Int32>``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of numbers.

Sample
~~~~~~

Here is an example that calculates the sum of all numbers from 0 to 9:

.. figure:: images/ListRangeSample.png
   :alt: 

The summation is carried out with the following Accumulate node:

.. figure:: images/ListRangeSample2.png
   :alt: 


