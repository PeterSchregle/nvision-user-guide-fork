HMI TabControl
--------------

The **TabControl** creates a tab control and displays its children in tabs.

.. figure:: images/HMITabControlNode.png
   :alt: 

At the bottom of the **TabControl** node is a pin that allows it to connect multiple children (use **TabItem** here).

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **TabControl**.

The **TabControl** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding* and *Background*, *Foreground* and *Font* styles.

TabStripPlacement (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The placement of the tabstrip. The values ``Bottom``, ``Left``, ``Top`` and ``Right`` are possible.

Outputs
~~~~~~~

SelectedIndex (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The index of the selected tab.
