PredefinedPalette
-----------------

Creates a palette with a predefined transfer function.

.. figure:: images/PredefinedPaletteNode.png
   :alt: 

Inputs
~~~~~~

name (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The type of the palette. Choices are ``Linear``, ``Inverse``, ``Square``, ``SquareInverse``, ``Cube``, ``CubeInverse``, ``CubicRoot``, ``CubicRootInverse``, ``Red``, ``Green``, ``Blue``, ``Cyan``, ``Magenta``, ``Yellow``, ``BlueOrange``, ``Cool``, ``CyanHot``, ``MagentaHot``, ``OrangeHot``, ``RedHot``, ``YellowHot``, ``Ice``, ``Fire``, ``GreenFireBlue``, ``Rainbow`` and ``Spectrum``.

Outputs
~~~~~~~

Palette (Type: ``Palette``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The output palette.

Comments
~~~~~~~~

This function creates a palette with a predefined function. It can be used to create monochrome or color palettes. The following palettes can be chosen:

**Linear**

.. figure:: images/linear_palette.png
   :alt: 

**Inverse**

.. figure:: images/inverse_palette.png
   :alt: 

**Square**

.. figure:: images/square_palette.png
   :alt: 

**SquareInverse**

.. figure:: images/square_inverse_palette.png
   :alt: 

**Cube**

.. figure:: images/cube_palette.png
   :alt: 

**CubeInverse**

.. figure:: images/cube_inverse_palette.png
   :alt: 

**CubicRoot**

.. figure:: images/cubic_root_palette.png
   :alt: 

**CubicRootInverse**

.. figure:: images/cubic_root_inverse_palette.png
   :alt: 

**Red**

.. figure:: images/red_palette.png
   :alt: 

**Green**

.. figure:: images/green_palette.png
   :alt: 

**Blue**

.. figure:: images/blue_palette.png
   :alt: 

**Cyan**

.. figure:: images/cyan_palette.png
   :alt: 

**Magenta**

.. figure:: images/magenta_palette.png
   :alt: 

**Yellow**

.. figure:: images/yellow_palette.png
   :alt: 

**BlueOrange**

.. figure:: images/blue_orange_palette.png
   :alt: 

**Cool**

.. figure:: images/cool_palette.png
   :alt: 

**CyanHot**

.. figure:: images/cyan_hot_palette.png
   :alt: 

**MagentaHot**

.. figure:: images/magenta_hot_palette.png
   :alt: 

**OrangeHot**

.. figure:: images/orange_hot_palette.png
   :alt: 

**RedHot**

.. figure:: images/red_hot_palette.png
   :alt: 

**YellowHot**

.. figure:: images/yellow_hot_palette.png
   :alt: 

**Ice**

.. figure:: images/ice_palette.png
   :alt: 

**Fire**

.. figure:: images/fire_palette.png
   :alt: 

**GreenFireBlue**

.. figure:: images/green_fire_blue_palette.png
   :alt: 

**Rainbow**

.. figure:: images/rainbow_palette.png
   :alt: 

**Spectrum**

.. figure:: images/spectrum_palette.png
   :alt: 


