HMI TabItem
-----------

The **TabItem** puts an expandable and collapsibal area around its child.

.. figure:: images/HMITabItemNode.png
   :alt: 

At the bottom of the **TabItem** node is a pin that allows it to connect one child.

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **Expander**.

The **Expander** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding* and *Background*, *Foreground* and *Font* styles.

Header (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The title of the **TabItem**.
