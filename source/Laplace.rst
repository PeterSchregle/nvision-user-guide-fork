Laplace
-------

Filters an image using a laplace kernel of size 3x3 or 5x5.

.. figure:: images/LaplaceNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

KernelSize (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The kernel size (3 -> 3x3, 5 -> 5x5).

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies an optional area of interest.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

::

    -1 -1 -1
    -1  8 -1
    -1 -1 -1

or a 5x5 kernel with the following values:

::

    -1 -3 -4 -3 -1
    -3  0  6  0 -3 
    -4  6 20  6 -4
    -3  0  6  0 -3 
    -1 -3 -4 -3 -1

The effect of a laplace filter is that it amplifies high frequencies and attenuates lowfrequencies. The strength of the laplace filter depends on the size of the kernel.

Here are a few results of the laplace filter with increasing kernel sizes:

Original:

.. figure:: images/GaussianOriginal.png
   :alt: 

KernelSize = 3:

.. figure:: images/Laplace3x3.png
   :alt: 

KernelSize = 5:

.. figure:: images/Laplace5x5.png
   :alt: 

Sample
~~~~~~

Here is an example that shows how to use the laplace filter.

.. figure:: images/LaplaceSample.png
   :alt: 


