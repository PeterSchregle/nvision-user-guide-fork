Render PDF
----------

Renders a page of a loaded PDF file into an image.

.. figure:: images/RenderPdfNode.png
   :alt: 

Inputs
~~~~~~

Document (Type: ``PdfLoadedDocument``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A PDF document.

Page (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^

Specifies the page in the PDF file that should be rendered.

Dpi (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^

Specifies the dots per inch used to render the PDF.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The page contents rendered into an image.

Comments
~~~~~~~~

Renders a page of a PDF file into an image with a specific Dpi setting.

Before a PDF can be rendered into an image, it must be loaded with the **Import PDF** or **Import PDF From Memory** nodes.
