Morphological Image Operation
-----------------------------

Performs a morphological image operation. Possible operations are erosion, dilation, opening, closing and morphological gradient.

.. figure:: images/MorphologicalImageOperation.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Constrains the function to a region of interest.

Operator (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Possible operators are *Erosion*, *Dilation*, *Closing*, *Opening* and *MorphologicalGradient*.

Structel (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

A structuring element.

Outputs
~~~~~~~

Flipped (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

This function performs several morphological operations.

Given the following original image, we show the results of different morphological operations:

.. figure:: images/GaussianOriginal.png
   :alt: 

Here is an example of an erosion with a circular structuring element of diameter 10:

.. figure:: images/ErosionImage.png
   :alt: 

Here is an example of a dilation with a circular structuring element of diameter 10:

.. figure:: images/DilationImage.png
   :alt: 

Here is an example of closing with a circular structuring element of diameter 10:

.. figure:: images/ClosingImage.png
   :alt: 

Here is an example of opening with a circular structuring element of diameter 10:

.. figure:: images/OpeningImage.png
   :alt: 

Here is an example of the morphological gradient with a circular structuring element of diameter 3:

.. figure:: images/MorphologicalGradientImage.png
   :alt: 

Sample
~~~~~~

Here is an example that erodes an image.

.. figure:: images/MorphSample.png
   :alt: 


