|image0| Elliptical Region
--------------------------

Creates an elliptical region.

.. figure:: images/CreateEllipticalRegionNode.png
   :alt: 

Inputs
~~~~~~

Diameter (Type: ``VectorDouble``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The diameter of the elliptical region.

Direction (Type: ``Direction``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The direction of the elliptical region.

Outputs
~~~~~~~

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The elliptical region.

Comments
~~~~~~~~

The region is centered on the origin. Regions centered on the origin are suitable for morphological operations, such as erosion, dilation, etc. The region is used as a structuring element and the shape of the region affects the morphological operation.

.. |image0| image:: images/elliptical_region.png
