Geometry
========

Many functions in image processing and image analysis deal with geometric entities, i.e. when you determine the position of a template or the length of a structure. **nVision** has a set of geometric features, which help managing, visualizing and manipulating geometries.

**nVision** provides a set of geometric primitives as well as a set of geometric transformations. Geometric primitives are things like points, vectors, lines, etc. Geometric transformations are translations, rotations, scalings, etc. Geometric transformations allow to transform geometric primitives, i.e. move, scale or rotate them, etc. Geometric primitives have features: the length and orientation of a line segment are examples. In addition, geometric primitives can be used to perform geometric calculations: examples are the calculation of the intersection between two lines, or the calculation of a convex hull polygon, given a set of points.

Besides the geometric primitives, **nVision** has graphic counterparts for some to support the visualization and the interactive manipulation of the geometric primites. For example, a **PointWidget** displays a point on some surface with a color and diameter that can be chosen via a **Pen**. The point widget also supports interactive manipulation, i.e. the user can drag the point on the surface with the mouse.
