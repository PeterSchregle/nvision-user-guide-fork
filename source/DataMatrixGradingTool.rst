DataMatrix Grading Tool
-----------------------

.. figure:: images/datamatrixgrade.png
   :alt: 

The **DataMatrix Grading** tool decodes and grades a two-dimensional barcode inside a region of interest.

The tool displays graphical elements to specify the region of interest as well as to display the results.

It displays a box that is used to select the region of interest. You can move the box around on the image by dragging its inside. You can also resize the box by picking its boundary lines (on the boundary line or just a bit outside) or its corner points (on the corner point of just a bit outside).

Once the search box has been positioned over a code in the image it tries to decode and grade the code. Its position and the decoded text is displayed in green, if the grade matches the expected grade, or red, if the grade does not match the expectation:

The **DataMatrix Grading** tool has a configuration panel, which can be used to set parameters.

The controls in **Grade Matching** allow you to specify a pattern that the decoded grade must follow in order to be correct.

**Regular Expression**: Defines the pattern that the decoded grade should match. The pattern follows the rule of a regular expression, specifically the .NET variant of regular expressions.

The default pattern is "A\|B", which mathes grades A or B. Any other grades are not matched by this pattern. Thus, grades A or B would be ok by default, other grades would be not ok.

The following table explains common cases. For full information, look at the `original documentation (https://msdn.microsoft.com/en-us/library/az24scfc(v=vs.110).aspx) <https://msdn.microsoft.com/en-us/library/az24scfc(v=vs.110).aspx>`__.

+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| Pattern           | Description                                                                                                                          |                     |                                           |
+===================+======================================================================================================================================+=====================+===========================================+
| ``.``             | Any character (except newline).                                                                                                      | ``a.c``             | ``abc``, ``aac``, ``acc``, ...            |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``^``             | Start of a string.                                                                                                                   | ``^abc``            | ``abc``, ``abcdefg``, ``abc123``, ...     |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``$``             | End of a string.                                                                                                                     | ``abc$``            | ``abc``, ``endsinabc``, ``123abc``, ...   |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``&#124;``        | Alternation.                                                                                                                         | ``bill&#124;ted``   | ``bill``, ``ted``                         |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``[...]``         | Explicit set of characters to match.                                                                                                 | ``a[bB]c``          | ``abc``, ``aBc``                          |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``*``             | 0 or more of previous expression.                                                                                                    | ``ab*c``            | ``ac``, ``abc``, ``abbc``, ...            |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``+``             | 1 or more of previous expression.                                                                                                    | ``ab+c``            | ``abc``, ``abbc``, ``abbbc``, ...         |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``?``             | 0 or 1 of previous expression; also forces minimal matching when an expression might match several strings within a search string.   | ``ab?c``            | ``ac``, ``abc``                           |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``{``...\ ``}``   | Explicit quantifier notation.                                                                                                        | ``ab{2}c``          | ``abbc``                                  |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``(``...\ ``)``   | Logical grouping of part of an expression.                                                                                           | ``(abc){2}``        | ``abcabc``                                |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+
| ``\\``            | Preceding one of the above, it makes it a literal instead of a special character.                                                    | ``a\\.b``           | ``a.b``                                   |
+-------------------+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+-------------------------------------------+

The controls in **Decoder Parameters** allow you to manipulate locator and decoder behavior.

**Polarities**: Selects the possible contrast of the marking. **Auto** (default), **Black on White** or **White on Black**.

**Views**: Selects the possible orientation of the marking. **Auto** (default), **Straight** or **Mirrored**.
