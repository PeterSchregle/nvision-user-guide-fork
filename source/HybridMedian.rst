HybridMedian
------------

Perform a hybrid median filter.

.. figure:: images/HybridMedianNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

KernelSize (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The kernel size.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies an optional area of interest.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

Here are a few results of the median filter with increasing kernel sizes:

Original:

.. figure:: images/GaussianOriginal.png
   :alt: 

KernelSize = 3:

.. figure:: images/HybridMedian3x3.png
   :alt: 

KernelSize = 5:

.. figure:: images/HybridMedian5x5.png
   :alt: 

Sample
~~~~~~

Here is an example that shows how to use the hybrid median filter.

.. figure:: images/HybridMedianSample.png
   :alt: 


