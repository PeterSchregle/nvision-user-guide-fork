Import PDF From Memory
----------------------

Imports a file in PDF format from a block of memory.

.. figure:: images/ImportPdfFromMemoryNode.png
   :alt: 

Inputs
~~~~~~

Address (Type: ``Int32Ptr``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A pointer to the data in memory.

Size (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^

The size in bytes of the data in memory.

Outputs
~~~~~~~

Document (Type: ``PdfLoadedDocument``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The loaded PDF document.

Comments
~~~~~~~~

The **Import PDF From Memory** node loads a PDF file from memory. The memory can be loaded by different means, i.e. by loading via FTP into memory.

Once a PDF file has been loaded, any page in it can be rendered into an image with the **Render PDF** node.
