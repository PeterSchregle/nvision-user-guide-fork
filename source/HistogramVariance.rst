Histogram Variance
------------------

Calculates the variance of all pixels in a buffer, given a histogram.

.. figure:: images/HistogramVarianceNode.png
   :alt: 

Inputs
~~~~~~

Histogram (Type: ``Histogram``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The input histogram.

Outputs
~~~~~~~

Variance (Type: Object)
^^^^^^^^^^^^^^^^^^^^^^^

The variance.

Comments
~~~~~~~~

The **Histogram Variance** node calculates the variance of all pixel values of a buffer, given a histogram to it.

If a color buffer is used, the variance is calculated channel-wise.

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The variance is calculated according to this formula:

:math:`\sigma^2 = \frac {1}{n}  \sum x^2 -  \mu^2`

Sample
~~~~~~

Here is an example that calculates the variance of a histogram.

.. figure:: images/BufferVarianceSample.png
   :alt: 


