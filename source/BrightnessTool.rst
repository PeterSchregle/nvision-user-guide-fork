Brightness Tool
---------------

.. figure:: images/features_brightness.png
   :alt: 

The **Brightness** tool measures the average brightness in a region of interest.

The tool displays graphical elements to specify the region of interest as well as to display the results.

It displays a rectangle that is used to select a region. You can move the rectangle around on the image by dragging its inside. You can also resize the rectangle by picking it at its boundary lines (on the boundary line or just a bit outside). You can rotate the rectangle by picking it at its corner points (on the corner point of just a bit outside).

The numerical value of the brightness is displayed in green, if it is in between the tool's minimum and maximum expected values, or in red if it is outside the expected values.

The brightness tool has a configuration panel, which can be used to set parameters.

The **Min Brightness** parameter is used to set the minimal accepatable brightness (default = 127), the **Max Brightness** parameter is used to set the maximal accepatable brightness (default = 255).
