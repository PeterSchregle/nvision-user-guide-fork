Histogram
---------

Calculates a histogram of an image to show the greyscale/color distribution.

.. figure:: images/HistogramNode.png
   :alt: 

The Histogram node can be followed by several other nodes to yield statistic information about an image or find an optimal threshold (Otsu).

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

An optional region of interest. If this parameter is connected, the histogram operation is constrained to pixels within the region only.

Outputs
~~~~~~~

Histogram (Type: ``Histogram``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The histogram.

Sample
~~~~~~

Here is an example:

.. figure:: images/HistogramSample.png
   :alt: 


