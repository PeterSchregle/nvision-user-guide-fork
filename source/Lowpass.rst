Lowpass
-------

Filters an image using a lowpass kernel of rectangular size. A lowpass filter blurs an image.

.. figure:: images/LowpassNode.png
   :alt: 

The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The kernel size is anisotropic and can be different in horizontal and vertical directions.

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

KernelSizeX (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The horizontal kernel size (>= 1).

KernelSizeY (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The vertical kernel size (>= 1).

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies an optional area of interest.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Sample
~~~~~~

Here is an example:

.. figure:: images/LowpassSample.png
   :alt: 


