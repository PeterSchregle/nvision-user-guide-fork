Horizontal Sobel
----------------

Filters an image using a horizontal sobel kernel of size 3x3 or 5x5.

.. figure:: images/HorizontalSobelNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

KernelSize (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The kernel size (3 -> 3x3, 5 -> 5x5).

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies an optional area of interest.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

::

     1  2  1
     0  0  0
    -1 -2 -1

or a 5x5 kernel with the following values:

::

     1  4   6  4  1
     2  8  12  8  2
     0  0   0  0  0
    -2 -8 -12 -8 -2
    -1 -4  -6 -4 -1

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

Here are a few results of the horizontal sobel filter with increasing kernel sizes:

Original:

.. figure:: images/GaussianOriginal.png
   :alt: 

KernelSize = 3:

.. figure:: images/HorizontalSobel3x3.png
   :alt: 

KernelSize = 5:

.. figure:: images/HorizontalSobel5x5.png
   :alt: 

Sample
~~~~~~

Here is an example that shows how to use the horizontal sobel filter.

.. figure:: images/HorizontalSobelSample.png
   :alt: 

Often, you want to combine the horizontal and the vertical sobel filter to create an edge map. A simple way to do this is shown in the following sample:

.. figure:: images/SobelEdgeMapSample.png
   :alt: 

Here is an example image of such an edge map:

.. figure:: images/Sobel3x3EdgeMap.png
   :alt: 


