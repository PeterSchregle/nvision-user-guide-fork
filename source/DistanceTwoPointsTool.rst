Distance Two Points Tool
------------------------

.. figure:: images/geometry_distance_two_points.png
   :alt: 

The **Distance (Two Points)** measures the distance between two points.

The tool displays graphical elements to specify the region of interest as well as to display the results.

It displays two yellow lines labelled A and B that specify the search linees. You can move the lines around on the image by dragging them. You can also pick either of the line endpoints to prolong the line or move just this endpoint (move the mouse a little bit away from the endpoint, until the cursor changes). If you move the mouse in the vicinity of the line, a little bit outside, you can drag the thickness of the line. The thickness affects the edge detection, because it specifies the area that is averaged.

Once the search lines have been positioned over two edges in the image the calculated distance, which is visualized graphically and its numerical value are displayed.

The tool displays its measurement in pixels, or in world units, such as mm, if the system has been calibrated.

The **Distance (Two Points)** tool has a configuration panel, which can be used to set parameters.

The **Min** and **Max** parameters are used to set the expected minimum and maximum distance.

The distance type between the two points can be the straight, horizontal or vertical distances.

The parameters in the **Edge Detection Parameters** affect the edge detection.

**Smoothing** is the amount of smoothing used in edge detection (default: 2.7).

**Strength** is the minimum strength of an edge (the gray scale slope of the edge, default = 15). Smoothing and Strength are interrelated: the more you smooth the more you lessen the strength of an edge and vice versa.
