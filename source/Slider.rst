HMI Slider
----------

A **Slider** can be used to move to or visualize a position.

.. figure:: images/HMISliderNode.png
   :alt: 

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **Slider**.

The **Slider** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding*, *Foreground*, *Background*, *Font* and *Padding* styles.

Orientation (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Orientation of the **Slider**, ``Horizontal`` or ``Vertical``.

Minimum (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The minimum value.

Maximum (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The maximum value.

SmallChange (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The amount that is subtracted from or added to the value when the *Left* or *Right* keyboard cursor keys are pressed.

LargeChange (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The amount that is subtracted from or added to the value when the channel is clicked left or right of the thumb.

TickFrequency (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The distance between tick marks.

TickPlacement (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The placement of the ticks, ``Both``, ``BottomLeft``, ``None`` or ``TopRight``.

Outputs
~~~~~~~

Value (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^

The position.

Example
~~~~~~~

Here is an example that shows a **Slider**. This definition:

.. figure:: images/hmi_slider_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_slider.png
   :alt: 


