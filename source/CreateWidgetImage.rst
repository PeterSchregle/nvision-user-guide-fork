Image
-----

The **Image** node is used to display an image.

.. figure:: images/WidgetImageNode.png
   :alt: 

At the top of the **Image** node is a pin that allows it to connect to a parent widget (usually a **Display** widget node, but other widget nodes can be used as well). The image will be displayed only, if a parent connection is made.

At the bottom of the **Image** node is a pin that allows it to connect children to the image widget. The parent, grandparent, children and grandchildren define the layering of the widgets and the graphical outcome.

Children are layered on top of the parent.

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The image to display inside the widget.

Alpha (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

The opacity of the image display.

Example
~~~~~~~

Here is an example that displays an image inside a display:

.. figure:: images/ImageDisplayExample.png
   :alt: 

Below the **Window** node is a **Display** and below the **Display** is an **Image**.
