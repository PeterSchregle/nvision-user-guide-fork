Pen
---

Creates a pen. A pen is used to specify outlines.

.. figure:: images/CreatePenByteNode.png
   :alt: 

Inputs
~~~~~~

Brush (Type: ``SolidColorBrushByte``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The brush.

Width (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The width of the pen. A positive width scales with the zoom factor, a negative width does not scale.

DashStyle (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The dash style, ``Solid``, ``Dashed``, ``Dotted``, ``Dashdotted`` or ``Dashdotdotted``.

DashOffset (Type : ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The offse into the dash pattern.

StartCapStyle (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The cap style of the start point, ``FlatCap``, ``SquareCap``, ``RoundCap`` or ``TriangleCap``.

EndCapStyle (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The cap style of the end point, ``FlatCap``, ``SquareCap``, ``RoundCap`` or ``TriangleCap``.

DashCapStyle (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The dash cap style, ``FlatCap``, ``SquareCap``, ``RoundCap`` or ``TriangleCap``.

MiterLimit (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The miter limit.

LineJoinStyle (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The line join style, ``MiterJoin``, ``BevelJoin``, ``RoundJoin`` or ``MiterOrBevelCap``.

StartLineEndStyle (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The line end style of the start point, ``NormalEnd``, ``FilledDiskEnd``, ``FilledSquareEnd``, ``FilledDiamondEnd``, ``FilledArrowOutEnd``, ``FilledArrowInEnd``, ``BarEnd``, ``BackslashEnd``, ``SlashEnd``, ``ArrowOutEnd``, ``ArrowInEnd``, ``FilledSlimArrowOutEnd``, ``FilledSlimArrowInEnd``, ``SlimArrowOutEnd`` or ``SlimArrowInEnd``.

StartLineEndSize (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The size of the start line end.

StopLineEndStyle (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The line end style of the stop point, ``NormalEnd``, ``FilledDiskEnd``, ``FilledSquareEnd``, ``FilledDiamondEnd``, ``FilledArrowOutEnd``, ``FilledArrowInEnd``, ``BarEnd``, ``BackslashEnd``, ``SlashEnd``, ``ArrowOutEnd``, ``ArrowInEnd``, ``FilledSlimArrowOutEnd``, ``FilledSlimArrowInEnd``, ``SlimArrowOutEnd`` or ``SlimArrowInEnd``.

StopLineEndSize (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The size of the stop line end.

Outputs
~~~~~~~

Pen (Type: ``PenByte``)
^^^^^^^^^^^^^^^^^^^^^^^

The pen.

Sample
~~~~~~

Here is an example that creates a yellowish half transparent brush and a wide pen using this brush.

.. figure:: images/BrushPenSample.png
   :alt: 


