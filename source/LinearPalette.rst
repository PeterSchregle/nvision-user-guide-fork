LinearPalette
-------------

Creates a palette with a linear transfer function.

.. figure:: images/LinearPaletteNode.png
   :alt: 

Inputs
~~~~~~

Type (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The type of the palette. Choices are ``PaletteByte``, ``PaletteUInt16``, ``PaletteUInt32``, ``PaletteDouble``, ``PaletteRgbByte``, ``PaletteRgbUInt16``, ``PaletteRgbUInt32`` and ``PaletteRgbDouble``.

Width (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The width of the palette.

A (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^

The slope of the linear curve.

B (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^

The intercept of the linear curve.

Outputs
~~~~~~~

Palette (Type: ``Palette``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The output palette.

Comments
~~~~~~~~

This function creates a palette with a linear function.

Here is an example of a linear curve with a slope of 2 and an intercept of -127:

.. figure:: images/linear_2_127_palette.png
   :alt: 


