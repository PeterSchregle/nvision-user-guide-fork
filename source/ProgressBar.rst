HMI ProgressBar
---------------

A **ProgressBar** can be used to move to or visualize a position.

.. figure:: images/HMIProgressBarNode.png
   :alt: 

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **ProgressBar**.

The **ProgressBar** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding*, *Foreground*, *Background*, *Font* and *Padding* styles.

Orientation (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Orientation of the **ProgressBar**, ``Horizontal`` or ``Vertical``.

Value (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^

The position.

Minimum (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The minimum value.

Maximum (Type: ``double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The maximum value.

Foreground (Type: ``SolidColorBrush``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The foreground.

Background (Type: ``SolidColorBrush``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The background.

Example
~~~~~~~

Here is an example that shows a **ProgressBar** coupled to a **Slider**. This definition:

.. figure:: images/hmi_progressbar_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_progressbar.png
   :alt: 


