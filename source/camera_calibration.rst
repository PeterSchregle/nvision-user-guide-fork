Camera Calibration
==================

In many measurement applications, measurements are to be taken in world units, such as meters. A typical camera transforms those world units into pixels. Camera calibration aims to perform the inverse transformation, in order to convert distances expressed in pixels back to world units. Camera calibration establishes a correspondence between coordinates in the real world and coordinates in the camera image.

.. figure:: images/camera_calibration.png
   :alt: Real World and Camera Correspondence

   Real World and Camera Correspondence
 

The real world is three-dimensional and a camera creates a two-dimensional picture of it. The resulting projection usually is a perspective projection, where distances between points far away from the camera are smaller than the same distances between points nearer to the camera.

If you can control the optical setup in a way that the optical axis is orthogonal to an observed planar scene, you can setup the calibration by defining a scaling factor only, like in the following example.

.. figure:: images/define_scale_example.png
   :alt: Define a scaling factor

   Define a scaling factor
 

The screenshot shows the definition of a scale with the **Define Scale** tool int the **Adjust Camera** group of the **Home** tab on the ribbon.

Once the scale has been defined, it will be used by the spatial measurement tools, and they will return their measured values in the world coordinates that have been defined with the **Define Scale** tool. It is important to know that the method with the scaling factor will only work, if you have an orthogonal camera setup. If the real setup deviates, the accuracy of the method will suffer.

Another, more automatic way to calibrate the camera is to use a calibration target. **nVision** uses calibration targets in order to find the correspondence between coordinates in the camera coordinate system and the respective coordinates in the real world. The resultant calibration is valid only for the two-dimensional plane where the calibration target has been put in the real world.

.. figure:: images/calibration_target_sample.png
   :alt: Using a calibration target

   Using a calibration target
 

The calibration target consists of a pattern with known geometry and known distances. The calibration procedure takes an image of the pattern and measures the pattern. The measured positions are with respect to the camera coordinate system. Since the coordinates of the calibration target are known, they can be related to the measured positions and the corespondence can be established. Once this correspondence is known, any coordinate in the image coordinate system can be transformed to the real world coordinates (in the plane of the calibration target).

**nVision** has a set of tools that help with camera calibration. The **Define Scale** and **Calibrate** tools can be used to determine the calibration. The **Distance**, **Distance (Two Points)**, **Circle** and **Area Size** tools return their results in world units if used with calibration.

+-----------------------------+------------+-------------------------------------------------------------------------------+
| Tool                        | Icon       | Description                                                                   |
+=============================+============+===============================================================================+
| **Define Scale**            | |image6|   | Interactively define a scaling factor to be used in camera calibration.       |
+-----------------------------+------------+-------------------------------------------------------------------------------+
| **Calibrate**               | |image7|   | Use a calibration target to automatically determine the camera calibration.   |
+-----------------------------+------------+-------------------------------------------------------------------------------+
| **Distance**                | |image8|   | Measure a distance along a line in world coordinates.                         |
+-----------------------------+------------+-------------------------------------------------------------------------------+
| **Distance (Two Points)**   | |image9|   | Measure a distance between two points in world coordinates.                   |
+-----------------------------+------------+-------------------------------------------------------------------------------+
| **Circle**                  | |image10|  | Measure a circle position and radius in world coordinates.                    |
+-----------------------------+------------+-------------------------------------------------------------------------------+
| **Area Size**               | |image11|  | Measure the size of an area in world coordinates.                             |
+-----------------------------+------------+-------------------------------------------------------------------------------+

Mathematically, the background of the camera calibration is a perspective transformation between pixel coordinates and real world coordinates. To find the values of the perspective transformation, the correspondence of points in the real world (they come from the known distance of the calibration target) and the points in the image (they are measured from the image of the calibration target) is needed. At least four correspondent points are needed, but **nVision** can handle any number of additional points. Once this correspondence is known, it is saved as a prespective transformation matrix along with the unit of the real world coordinates.

Various vendors produce high quality calibration targets in different size and made from different material. See the Edmund Optics website for more information: http://www.edmundoptics.com/test-targets/distortion-test-targets/fixed-frequency-grid-distortion-targets, http://www.edmundoptics.com/test-targets/distortion-test-targets/diffuse-reflectance-grid-distortion-targets/3046/.

A cheap way to create calibration targets is to print them out with a printer. The dots should be arranged in a rectangular way and the horizontal and vertical distances of all dots to their neighbors must be the same.

.. |image0| image:: images/define_scale.png
.. |image1| image:: images/calibrate.png
.. |image2| image:: images/geometry_distance.png
.. |image3| image:: images/geometry_distance_two_points.png
.. |image4| image:: images/geometry_circle.png
.. |image5| image:: images/features_area_size.png
.. |image6| image:: images/define_scale.png
.. |image7| image:: images/calibrate.png
.. |image8| image:: images/geometry_distance.png
.. |image9| image:: images/geometry_distance_two_points.png
.. |image10| image:: images/geometry_circle.png
.. |image11| image:: images/features_area_size.png
