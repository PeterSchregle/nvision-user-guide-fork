Quantize
--------

Quantize an image down to 256 or less colors.

.. figure:: images/QuantizeNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Colors (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^

The number of colors in the output image.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Palette (Type: ``Palette``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The output color palette.

Comments
~~~~~~~~

This function reduces the number of colors in an image to the specified number of 256 or less. The result is a one-channel image and an associated color palette.

Color image quantized to 256 colors:

.. figure:: images/quantize_256.png
   :alt: 

Color image quantized to 16 colors:

.. figure:: images/quantize_16.png
   :alt: 

Color image quantized to 4 colors:

.. figure:: images/quantize_2.png
   :alt: 

Sample
~~~~~~

Here is an example:

.. figure:: images/QuantizeSample.png
   :alt: 


