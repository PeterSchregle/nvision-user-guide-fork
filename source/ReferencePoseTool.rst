Reference Pose Tool
-------------------

.. figure:: images/reference.png
   :alt: 

The **Reference** tool modifies the pose established by any location tool upstream.

The tool displays graphical elements to specify the new reference pose.

The tool displays a yellow box that you can drag and rotate to set the new pose. You can move the box around on the image by dragging its inside. You can also resize the box by picking it at its boundary lines (on the boundary line or just a bit outside) or rotate it by picking it at its corner points (on the corner point of just a bit outside).

Both the original pose (in gray) and the new pose are displayed, as well as two dimmer lines to help with alignment.

Subsequent tools use the pose to position their ROIs, such that the ROIs are always in the correct position, even if the part moves considerably.

**Stabilize Image** keeps the image steady for downstream tools. It translates and rotates the image so that the selected pose becomes the new center.
