Add Input
---------

Input ports are the means by which data flows into a subpipeline. A subpipeline can have any number of input ports, including zero. Input ports must be named uniquely, two input ports of the same suppipeline cannot have the same name.

By default a sub-pipeline has one input, named **Input1**.

This is how this looks from the outside:

.. figure:: images/SubPipelineNode.png
   :alt: 

and here how it looks from the inside:

.. figure:: images/SubPipelineInputInside.png
   :alt: 

With the **Add Input (Ctrl-I)** command (or the ``Ctrl-I`` keyboard shortcut), you can add any number of additional inputs, that are automatically named as well.

.. figure:: images/SubPipelineInputsInside.png
   :alt: 

To delete an input, right-click on the arrow in front of the name and choose the **Delete** command.

To rename an input, click on the name and type.

.. figure:: images/SubPipelineInputEdit.png
   :alt: 


