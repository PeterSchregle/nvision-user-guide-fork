Ribbon
======

The ribbon shows commands used to control **nVision**. The commands are grouped into tabs and then further into groups.

.. figure:: images/ribbon.png
   :alt: The ribbon.

   The ribbon.
 

At the top of the ribbon there are some quick access commands.

+------------+--------------+-----------+-----------------------------------------------------------+
|            | Shortcut     | Command   | Description                                               |
+============+==============+===========+===========================================================+
| |image6|   | ``CTRL-O``   |           | Opens a file.                                             |
+------------+--------------+-----------+-----------------------------------------------------------+
| |image7|   |              |           | Saves to a file.                                          |
+------------+--------------+-----------+-----------------------------------------------------------+
| |image8|   |              |           | Saves to a file under a new name.                         |
+------------+--------------+-----------+-----------------------------------------------------------+
| |image9|   |              |           | Saves all files.                                          |
+------------+--------------+-----------+-----------------------------------------------------------+
| |image10|  |              |           | Deletes the selected node.                                |
+------------+--------------+-----------+-----------------------------------------------------------+
| |image11|  |              |           | Commits the results of the selected node as a document.   |
+------------+--------------+-----------+-----------------------------------------------------------+

At the right of the ribbon, besides the **nVision** logo, there is the Help button. If you click on this button, the help window is shown. The help window provides context sensitive help as well as internet links to educational videos and this manual.

+-------------+------------+-----------+-----------------------------------------------+
|             | Shortcut   | Command   | Description                                   |
+=============+============+===========+===============================================+
| |image13|   | F1         | Help      | Displays documentation in a browser window.   |
+-------------+------------+-----------+-----------------------------------------------+

Besides, still at the right, there is a button with a little upwards pointing arrow. Click on this button to minimize the ribbon.

.. figure:: images/ribbon_minimized.png
   :alt: The ribbon in its minimized state.

   The ribbon in its minimized state.
 

If the ribbon is in minimized mode, you can still access the commands by first clicking on the respective tab.

File
----

The file tab at the very left shows commands to open and save files, as well as to import and export pipelines.

.. figure:: images/file_menu.png
   :alt: The file menu with the list of recently opened files.

   The file menu with the list of recently opened files.
 

In addition, it shows a list of recently loaded documents. Documents on the recently loaded list can be pinned, so that they are kept at the top of the list.

At the bottom, there is also a button to shut down **nVision**.

Home
----

The **Home** tab shows the most common commands.

.. figure:: images/home_tab.png
   :alt: The home tab.

   The home tab.
 

At the left – in the **Acquire** group - there are selection boxes for cameras and the color mode of the camera (these are filled with entries, once a supported camera is properly installed) as well as two buttons that allow you to take a snapshot (Start Snap) from a camera or put the camera into life acquisition (Start Live/Stop Live).

+-------------+------------+-------------------+-------------------------------------+
|             | Shortcut   | Command           | Description                         |
+=============+============+===================+=====================================+
| |image16|   |            | Start/Stop Snap   | Snaps a picture.                    |
+-------------+------------+-------------------+-------------------------------------+
| |image17|   |            | Start/Stop Live   | Starts or stops live acquisition.   |
+-------------+------------+-------------------+-------------------------------------+

Then - in the **Adjust Camera** - there are commands to adjust exposure and calibration of the camera.

+-------------+------------+------------+--------------------------------------+
|             | Shortcut   | Command    | Description                          |
+=============+============+============+======================================+
| |image21|   |            | Exposure   | Inserts the exposure control tool.   |
+-------------+------------+------------+--------------------------------------+
| |image22|   |            | Exposure   | Inserts the define scale tool.       |
+-------------+------------+------------+--------------------------------------+
| |image23|   |            | Exposure   | Inserts the calibration tool.        |
+-------------+------------+------------+--------------------------------------+

Then – in the **Zoom** group – there are commands to set the zoom factor. You can **Zoom In**, **Zoom Out**, or reset the zoom (**Reset Zoom**). The **Reset Zoom** button has a drop-down menu with additional zoom commands: **Zoom to Fit Width**, **Zoom to Fit Height** and **Zoom to Fit**. These commands allow you to zoom an image to fit the window width or height or both.

+-------------+--------------+--------------+--------------------------------------+
|             | Shortcut     | Command      | Description                          |
+=============+==============+==============+======================================+
| |image27|   | CTRL+ +      | Zoom In      | Zoom in to show more details.        |
+-------------+--------------+--------------+--------------------------------------+
| |image28|   | CTRL+ -      | Zoom Out     | Zoom out to show more surrounding.   |
+-------------+--------------+--------------+--------------------------------------+
| |image29|   | CTRL+ALT+0   | Reset Zoom   | Resets the zoom factor.              |
+-------------+--------------+--------------+--------------------------------------+

Below the **Reset Zoom** command there is a submenu with additional related commands.

+-------------+------------+----------------------+---------------------------------------------------+
|             | Shortcut   | Command              | Description                                       |
+=============+============+======================+===================================================+
| |image33|   |            | Zoom Fit             | Set the zoom so that both width and height fit.   |
+-------------+------------+----------------------+---------------------------------------------------+
| |image34|   |            | Zoom to Fit Width    | Set the zoom so that the width fits.              |
+-------------+------------+----------------------+---------------------------------------------------+
| |image35|   |            | Zoom to Fit Height   | Set the zoom so that the height fits.             |
+-------------+------------+----------------------+---------------------------------------------------+

Then – in the **Tool Windows** group – there are buttons that allow you to show or hide various additional windows.

+-------------+------------+--------------------------+-------------------------------------------+
|             | Shortcut   | Command                  | Description                               |
+=============+============+==========================+===========================================+
| |image40|   | F11        | Show Browser             | Show/hide the browser.                    |
+-------------+------------+--------------------------+-------------------------------------------+
| |image41|   | F1         | Show Help                | Show/hide the help window.                |
+-------------+------------+--------------------------+-------------------------------------------+
| |image42|   |            | Show Camera Parameters   | Show/hide the camera parameters window.   |
+-------------+------------+--------------------------+-------------------------------------------+
| |image43|   |            | Show System Globals      | Show/hide the system globals window.      |
+-------------+------------+--------------------------+-------------------------------------------+

Locate
------

The **Locate** tab shows commands that insert tools for part location. Part location very often is the first step in an inspection task.

.. figure:: images/locate_tab.png
   :alt: The locate tab.

   The locate tab.
 

The **Edit** group contains the commands for pipeline editing, which are repeated on most ribbon tabs for good accessability.

+-------------+------------+---------------+-----------------------------------------------------------+
|             | Shortcut   | Command       | Description                                               |
+=============+============+===============+===========================================================+
| |image46|   |            | Delete Node   | Deletes the selected node.                                |
+-------------+------------+---------------+-----------------------------------------------------------+
| |image47|   |            | Commit        | Commits the results of the selected node as a document.   |
+-------------+------------+---------------+-----------------------------------------------------------+

The **Locate** group contains the commands for localization.

+-------------+------------+--------------------+-------------------------------------------------------------------+
|             | Shortcut   | Command            | Description                                                       |
+=============+============+====================+===================================================================+
| |image51|   |            | Locate (Shape)     | Locates a part using shapes (geometrical shape matching).         |
+-------------+------------+--------------------+-------------------------------------------------------------------+
| |image52|   |            | Locate (Pattern)   | Locates a part using pattern matching (normalized correlation).   |
+-------------+------------+--------------------+-------------------------------------------------------------------+
| |image53|   |            | Teach              | Teaches a part for subsequent location.                           |
+-------------+------------+--------------------+-------------------------------------------------------------------+

Measure
-------

The **Measure** tab shows commands that insert tools for part measurement. Measurement of intensity or color, of geometric dimensions, as well as counting of features are often steps in an inspection task.

.. figure:: images/measure_tab.png
   :alt: The measure tab.

   The measure tab.
 

The **Edit** group contains the commands for pipeline editing, which are repeated on most ribbon tabs for good accessability.

+-------------+------------+---------------+-----------------------------------------------------------+
|             | Shortcut   | Command       | Description                                               |
+=============+============+===============+===========================================================+
| |image56|   |            | Delete Node   | Deletes the selected node.                                |
+-------------+------------+---------------+-----------------------------------------------------------+
| |image57|   |            | Commit        | Commits the results of the selected node as a document.   |
+-------------+------------+---------------+-----------------------------------------------------------+

The **Intensity** Group contains the commands for intensity based measurement.

+-------------+------------+--------------+--------------------------------------------------------+
|             | Shortcut   | Command      | Description                                            |
+=============+============+==============+========================================================+
| |image60|   |            | Brightness   | Inspects the brightness inside a region of interest.   |
+-------------+------------+--------------+--------------------------------------------------------+
| |image61|   |            | Contrast     | Inspects the contrast inside a region of interest.     |
+-------------+------------+--------------+--------------------------------------------------------+

The **Geometry** Group contains the commands for geometry based measurement.

+-------------+------------+-------------+----------------------+
|             | Shortcut   | Command     | Description          |
+=============+============+=============+======================+
| |image66|   |            | Distance    | Measures a length.   |
+-------------+------------+-------------+----------------------+
| |image67|   |            | Circle      | Measures a circle.   |
+-------------+------------+-------------+----------------------+
| |image68|   |            | Angle       | Measures an angle.   |
+-------------+------------+-------------+----------------------+
| |image69|   |            | Area Size   | Measures an area.    |
+-------------+------------+-------------+----------------------+

The **Count** Group contains the commands for feature counting.

+-------------+------------+------------------------+--------------------------------------------+
|             | Shortcut   | Command                | Description                                |
+=============+============+========================+============================================+
| |image73|   |            | Count Edges            | Counts the number of edges along a line.   |
+-------------+------------+------------------------+--------------------------------------------+
| |image74|   |            | Count Contour Points   | Counts the number of contour points.       |
+-------------+------------+------------------------+--------------------------------------------+
| |image75|   |            | Count Areas            | Counts the number of distinct blobs.       |
+-------------+------------+------------------------+--------------------------------------------+

Verify
------

The **Verify** tab shows commands that insert tools for part verification.

.. figure:: images/verify_tab.png
   :alt: The verify tab.

   The verify tab.
 

The **Edit** group contains the commands for pipeline editing, which are repeated on most ribbon tabs for good accessability.

+-------------+------------+---------------+-----------------------------------------------------------+
|             | Shortcut   | Command       | Description                                               |
+=============+============+===============+===========================================================+
| |image78|   |            | Delete Node   | Deletes the selected node.                                |
+-------------+------------+---------------+-----------------------------------------------------------+
| |image79|   |            | Commit        | Commits the results of the selected node as a document.   |
+-------------+------------+---------------+-----------------------------------------------------------+

The **Features** group contains the commands for pattern matching.

+-------------+------------+-----------------+----------------------------------------------------------------+
|             | Shortcut   | Command         | Description                                                    |
+=============+============+=================+================================================================+
| |image82|   |            | Match Contour   | Matches a region of a part using geometrical shape matching.   |
+-------------+------------+-----------------+----------------------------------------------------------------+
| |image83|   |            | Match Pattern   | Matches a region of apart using normalized correlation.        |
+-------------+------------+-----------------+----------------------------------------------------------------+

Identify
--------

The **Identify** tab shows commands that insert tools for part identification, such as decoding barcodes and matrix codes, as well as reading texts.

.. figure:: images/identify_tab.png
   :alt: The identify tab.

   The identify tab.
 

The **Edit** group contains the commands for pipeline editing, which are repeated on most ribbon tabs for good accessability.

+-------------+------------+---------------+-----------------------------------------------------------+
|             | Shortcut   | Command       | Description                                               |
+=============+============+===============+===========================================================+
| |image86|   |            | Delete Node   | Deletes the selected node.                                |
+-------------+------------+---------------+-----------------------------------------------------------+
| |image87|   |            | Commit        | Commits the results of the selected node as a document.   |
+-------------+------------+---------------+-----------------------------------------------------------+

The **Codes** group contains the commands for barcode and matrix code decoding.

+-------------+------------+--------------+--------------------------------------------------+
|             | Shortcut   | Command      | Description                                      |
+=============+============+==============+==================================================+
| |image90|   |            | Barcode      | Decodes a barcode in a region of interest.       |
+-------------+------------+--------------+--------------------------------------------------+
| |image91|   |            | Matrixcode   | Decodes a matrix code in a region of interest.   |
+-------------+------------+--------------+--------------------------------------------------+

The **Text** group contains the commands for optical character recognition.

+-------------+------------+-----------+---------------------------------------------------------------------------------+
|             | Shortcut   | Command   | Description                                                                     |
+=============+============+===========+=================================================================================+
| |image93|   |            | OCR       | Reads text in a region of interest using optical character recognition (OCR).   |
+-------------+------------+-----------+---------------------------------------------------------------------------------+

Process
-------

The **Process** tab has commands for basic image processing. The commands are grouped into statistic operations, point processing (pixel-wise), color transformation, filtering, morphology and geometric transformations.

.. figure:: images/process_tab.png
   :alt: The process tab.

   The process tab.
 

The **Edit** group contains the commands for pipeline editing, which are repeated on most ribbon tabs for good accessability.

+-------------+------------+---------------+-----------------------------------------------------------+
|             | Shortcut   | Command       | Description                                               |
+=============+============+===============+===========================================================+
| |image96|   |            | Delete Node   | Deletes the selected node.                                |
+-------------+------------+---------------+-----------------------------------------------------------+
| |image97|   |            | Commit        | Commits the results of the selected node as a document.   |
+-------------+------------+---------------+-----------------------------------------------------------+

The **Statistics** group contains commands for statistical processing.

+--------------+------------+------------------------------+-----------------------------------------------------------------+
|              | Shortcut   | Command                      | Description                                                     |
+==============+============+==============================+=================================================================+
| |image101|   |            | Histogram                    | Calculates a histogram.                                         |
+--------------+------------+------------------------------+-----------------------------------------------------------------+
| |image102|   |            | Horizontal Profile           | Calculates a horizontal profile.                                |
+--------------+------------+------------------------------+-----------------------------------------------------------------+
| |image103|   |            | Horizontal Profile Overlay   | Calculates a horizontal profile and overlays it on the image.   |
+--------------+------------+------------------------------+-----------------------------------------------------------------+

The **Crop** group contains the **Crop Box** command.

+--------------+------------+------------+--------------------------------------------+
|              | Shortcut   | Command    | Description                                |
+==============+============+============+============================================+
| |image105|   |            | Crop Box   | Crops a rectangular portion of an image.   |
+--------------+------------+------------+--------------------------------------------+

The **Bin** group contains the **Bin** command.

+--------------+------------+-----------+----------------------------------+
|              | Shortcut   | Command   | Description                      |
+==============+============+===========+==================================+
| |image107|   |            | Bin       | Resize an image using binning.   |
+--------------+------------+-----------+----------------------------------+

The **Point** group contains point operations between an image and a constant. These operations look at single pixels only and ignore their neighborhood.

+--------------+------------+---------------------+---------------------------------------------------------------------------------+
|              | Shortcut   | Command             | Description                                                                     |
+==============+============+=====================+=================================================================================+
| |image128|   |            | Not                 | Inverts every pixel of an image (using 1's complement).                         |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image129|   |            | Negate              | Inverts every pixel of an image (using 2's complement).                         |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image130|   |            | Increment           | Increments every pixel of an image by 1.                                        |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image131|   |            | Decrement           | Decrements every pixel of an image by 1.                                        |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image132|   |            | Absolute            | Takes the absolute value of every pixel of an image.                            |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image133|   |            | Add                 | Adds a constant value to every pixel of an image.                               |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image134|   |            | Subtract            | Subtracts a constant value from every pixel of an image.                        |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image135|   |            | Difference          | Takes the absolute difference between every pixel of an image and a constant.   |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image136|   |            | Multiply            | Multiplies every pixel of an image by a constant value.                         |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image137|   |            | Divide              | Divides every pixel of an image through a constant value.                       |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image138|   |            | Logical And         | And of every pixel of an image and a constant value.                            |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image139|   |            | Logical Or          | Or of every pixel of an image and a constant value.                             |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image140|   |            | Logical Xor         | Xor of every pixel of an image and a constant value.                            |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image141|   |            | Equal               | Compares every pixel of an image with a constant.                               |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image142|   |            | Bigger              | Compares every pixel of an image with a constant.                               |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image143|   |            | Bigger or Equal     | Compares every pixel of an image with a constant.                               |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image144|   |            | Smaller             | Compares every pixel of an image with a constant.                               |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image145|   |            | Smaller or Equal<   | Compares every pixel of an image with a constant.                               |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image146|   |            | Minimum             | Calculates the minimum of every pixel of an image and a constant.               |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+
| |image147|   |            | Maximum             | Calculates the maximum of every pixel of an image and a constant.               |
+--------------+------------+---------------------+---------------------------------------------------------------------------------+

The **Color** group contains commands for colorspace conversions.

+--------------+------------+-------------------------+----------------------------------------------------------------------------------------------+
|              | Shortcut   | Command                 | Description                                                                                  |
+==============+============+=========================+==============================================================================================+
| |image153|   |            | Convert to Monochrome   | Converts every pixel of a color image to monochrome.                                         |
+--------------+------------+-------------------------+----------------------------------------------------------------------------------------------+
| |image154|   |            | Convert to Rgb          | Converts every pixel of a color image to the RGB (red, green, blue) color space.             |
+--------------+------------+-------------------------+----------------------------------------------------------------------------------------------+
| |image155|   |            | Convert to Hls          | Converts every pixel of a color image to the HLS (hue, luminance, saturation) color space.   |
+--------------+------------+-------------------------+----------------------------------------------------------------------------------------------+
| |image156|   |            | Convert to Hsi          | Converts every pixel of a color image to the HSI (hue, saturation, intensity) color space.   |
+--------------+------------+-------------------------+----------------------------------------------------------------------------------------------+
| |image157|   |            | Convert to Lab          | Converts every pixel of a color image to the L\ *A*\ B\* color space.                        |
+--------------+------------+-------------------------+----------------------------------------------------------------------------------------------+

The **Frame** group contains the **Frame** command.

+--------------+------------+-----------+---------------------------------+
|              | Shortcut   | Command   | Description                     |
+==============+============+===========+=================================+
| |image159|   |            | Frame     | Puts a frame around an image.   |
+--------------+------------+-----------+---------------------------------+

The **Filter** group contains linear filters. Linear filters produce their results by taking their neighbor pixels into consideration. Various filters use different filter kernels to create various filter characteristics.

+--------------+------------+----------------------+--------------------------------------------------------------------+
|              | Shortcut   | Command              | Description                                                        |
+==============+============+======================+====================================================================+
| |image173|   |            | Median               | Calculates a median filter.                                        |
+--------------+------------+----------------------+--------------------------------------------------------------------+
| |image174|   |            | Hybrid Median        | Calculates a hybrid median which better preserves edges.           |
+--------------+------------+----------------------+--------------------------------------------------------------------+
| |image175|   |            | Gaussian             | Blurs an image with a gaussian averaging filter of varying size.   |
+--------------+------------+----------------------+--------------------------------------------------------------------+
| |image176|   |            | Gauss                | Blurs an image with a fixed size Gaussian kernel.                  |
+--------------+------------+----------------------+--------------------------------------------------------------------+
| |image177|   |            | Lowpass              | Blurs an image with a lowpass filter.                              |
+--------------+------------+----------------------+--------------------------------------------------------------------+
| |image178|   |            | Hipass               | Sharpens an image with a sharpening filter.                        |
+--------------+------------+----------------------+--------------------------------------------------------------------+
| |image179|   |            | Laplace              | Calculates a laplace filter.                                       |
+--------------+------------+----------------------+--------------------------------------------------------------------+
| |image180|   |            | Horizontal Sobel     | Emphasizes edges with a horizontal Sobel filter.                   |
+--------------+------------+----------------------+--------------------------------------------------------------------+
| |image181|   |            | Horizontal Prewitt   | Emphasizes edges with a horizontal Prewitt filter.                 |
+--------------+------------+----------------------+--------------------------------------------------------------------+
| |image182|   |            | Horizontal Scharr    | Emphasizes edges with a horizontal Scharr filter.                  |
+--------------+------------+----------------------+--------------------------------------------------------------------+
| |image183|   |            | Vertical Sobel       | Emphasizes edges with a vertical Sobel filter.                     |
+--------------+------------+----------------------+--------------------------------------------------------------------+
| |image184|   |            | Vertical Prewitt     | Emphasizes edges with a vertical Prewitt filter.                   |
+--------------+------------+----------------------+--------------------------------------------------------------------+
| |image185|   |            | Vertical Scharr      | Emphasizes edges with a vertical Scharr filter.                    |
+--------------+------------+----------------------+--------------------------------------------------------------------+

The **Morphology** group contains morphological operations, i.e. operations that change the shape. Some of these operations can be applied to images and to regions. Region versions are binary in nature and extremely fast because of their run-length implementation.

+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+
|              | Shortcut   | Command              | Description                                                                                                              |
+==============+============+======================+==========================================================================================================================+
| |image199|   |            | Dilate               | Performs a dilation on image pixels. A dilation makes bright areas bigger and dark areas smaller.                        |
+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+
| |image200|   |            | Dilate (Regions)     | Performs a dilation on a region. A dilation makes a region bigger.                                                       |
+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+
| |image201|   |            | Erode                | Performs an erosion on image pixels. An erosion makes bright areas smaller and dark areas bigger.                        |
+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+
| |image202|   |            | Erode (Regions)      | Performs an erosion on a region. An erosion makes a region smaller.                                                      |
+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+
| |image203|   |            | Open                 | Performs an opening on an image. Opening is an erosion followed by a dilation.                                           |
+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+
| |image204|   |            | Open (Regions)       | Performs an opening on a region. Opening is an erosion followed by a dilation.                                           |
+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+
| |image205|   |            | Close                | Performs a closing on an image. Closing is a dilation followed by an erosion.                                            |
+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+
| |image206|   |            | Close (Regions)      | Performs a closing on a region. Closing is a dilation followed by an erosion.                                            |
+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+
| |image207|   |            | Gradient             | Performs a morphological gradient on an image. Closing is a dilation minus an erosion.                                   |
+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+
| |image208|   |            | Gradient (Regions)   | Performs a morphological gradient on a region. Closing is a dilation minus an erosion.                                   |
+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+
| |image209|   |            | Inner Boundary       | Calculates the inner boundary of a region. The inner boundary consists of the region pixels that touch the background.   |
+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+
| |image210|   |            | Outer Boundary       | Calculates the outer boundary of a region. The outer boundary consists of the background pixels that touch the region.   |
+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+
| |image211|   |            | Fill Holes           | Fills holes in a region.                                                                                                 |
+--------------+------------+----------------------+--------------------------------------------------------------------------------------------------------------------------+

The **Geometry** tab contains commands for geometric transformations.

+--------------+------------+----------------------------+----------------------------------------------------+
|              | Shortcut   | Command                    | Description                                        |
+==============+============+============================+====================================================+
| |image217|   |            | Mirror                     | Reverses right and left of an image.               |
+--------------+------------+----------------------------+----------------------------------------------------+
| |image218|   |            | Flip                       | Reverses top and bottom of an image.               |
+--------------+------------+----------------------------+----------------------------------------------------+
| |image219|   |            | Rotate Counter Clockwise   | Rotate an image by 90 degrees counter clockwise.   |
+--------------+------------+----------------------------+----------------------------------------------------+
| |image220|   |            | Rotate 180 Degrees         | Rotates an image by 180 degrees.                   |
+--------------+------------+----------------------------+----------------------------------------------------+
| |image221|   |            | Rotate Clockwise           | Rotates an image by 90 degrees clockwise.          |
+--------------+------------+----------------------------+----------------------------------------------------+

Segmentation
------------

The **Segmentation** tab groups commands related to segmentation and blob analysis.

.. figure:: images/segmentation_tab.png
   :alt: The segmentation tab.

   The segmentation tab.
 

The **Edit** group contains the commands for pipeline editing, which are repeated on most ribbon tabs for good accessability.

+--------------+------------+---------------+-----------------------------------------------------------+
|              | Shortcut   | Command       | Description                                               |
+==============+============+===============+===========================================================+
| |image224|   |            | Delete Node   | Deletes the selected node.                                |
+--------------+------------+---------------+-----------------------------------------------------------+
| |image225|   |            | Commit        | Commits the results of the selected node as a document.   |
+--------------+------------+---------------+-----------------------------------------------------------+

The **Segmentation** group contains the commands for thresholding, connected components separation and region filtering.

+--------------+------------+------------------------+--------------------------------------------------------------------------------------+
|              | Shortcut   | Command                | Description                                                                          |
+==============+============+========================+======================================================================================+
| |image229|   |            | Threshold              | Thresholds an image and returns a region.                                            |
+--------------+------------+------------------------+--------------------------------------------------------------------------------------+
| |image230|   |            | Connected Components   | Splits a region into its connected components, that is a list of separate regions.   |
+--------------+------------+------------------------+--------------------------------------------------------------------------------------+
| |image231|   |            | Filter Regions         | Filters regions according to a criterium.                                            |
+--------------+------------+------------------------+--------------------------------------------------------------------------------------+

The **Blob Analysis** group contains the commands for blob analysis and graphical plotting.

+--------------+------------+-----------------+------------------------------------------------+
|              | Shortcut   | Command         | Description                                    |
+==============+============+=================+================================================+
| |image234|   |            | Blob Analysis   | Performs blob analysis on a list of regions.   |
+--------------+------------+-----------------+------------------------------------------------+
| |image235|   |            | Plot            | Plots the results of a blob analysis.          |
+--------------+------------+-----------------+------------------------------------------------+

Pipeline
--------

The **Pipeline** tab groups pipeline related commands.

.. figure:: images/pipeline_tab.png
   :alt: The pipeline tab.

   The pipeline tab.
 

The **Edit** group contains the commands for pipeline editing, which are repeated on most ribbon tabs for good accessability.

+--------------+------------+---------------+-----------------------------------------------------------+
|              | Shortcut   | Command       | Description                                               |
+==============+============+===============+===========================================================+
| |image238|   |            | Delete Node   | Deletes the selected node.                                |
+--------------+------------+---------------+-----------------------------------------------------------+
| |image239|   |            | Commit        | Commits the results of the selected node as a document.   |
+--------------+------------+---------------+-----------------------------------------------------------+

The **Pipeline** group contains the commands for sub-pipeline creation as well as import and export of pipelines.

+--------------+------------+-------------------+-----------------------------------+
|              | Shortcut   | Command           | Description                       |
+==============+============+===================+===================================+
| |image243|   |            | Sub-Pipeline      | Creates a sub-pipeline.           |
+--------------+------------+-------------------+-----------------------------------+
| |image244|   |            | Import Pipeline   | Imports a pipeline from a file.   |
+--------------+------------+-------------------+-----------------------------------+
| |image245|   |            | Export Pipeline   | Exports a pipeline to a file.     |
+--------------+------------+-------------------+-----------------------------------+

The **Control** group contains the commands for batch processing of images in folders.

+--------------+------------+------------------+---------------------------------------------------+
|              | Shortcut   | Command          | Description                                       |
+==============+============+==================+===================================================+
| |image253|   |            | First Image      | Jumps to the first image in an image folder.      |
+--------------+------------+------------------+---------------------------------------------------+
| |image254|   |            | Previous Image   | Jumps to the previous image in an image folder.   |
+--------------+------------+------------------+---------------------------------------------------+
| |image255|   |            | Next Image       | Jumps to the next image in an image folder.       |
+--------------+------------+------------------+---------------------------------------------------+
| |image256|   |            | Process Images   | Processes all images in an image folder.          |
+--------------+------------+------------------+---------------------------------------------------+
| |image257|   |            | Stop             | Stops processing of images in an image folder.    |
+--------------+------------+------------------+---------------------------------------------------+
| |image258|   |            | Export On/Off    | Turns export nodes on or off.                     |
+--------------+------------+------------------+---------------------------------------------------+
| |image259|   |            | Export Once      | Causes all export nodes to export once.           |
+--------------+------------+------------------+---------------------------------------------------+

The **Timer** group allows to set the polling interval for polling nodes.

+--------------+------------+-----------------+--------------------------+
|              | Shortcut   | Command         | Description              |
+==============+============+=================+==========================+
| |image261|   |            | Update On/Off   | Switch polling on/off.   |
+--------------+------------+-----------------+--------------------------+

The **Execution Info** group provides information about program execution. It shows the last execution time of the program as well as the execution state.

+--------------+------------+--------------------+-------------------------------------------------------------+
|              | Shortcut   | Command            | Description                                                 |
+==============+============+====================+=============================================================+
| |image263|   |            | Show Node Timing   | Shows timing information of every node in a sub-pipeline.   |
+--------------+------------+--------------------+-------------------------------------------------------------+

The **Execution Statistics** group provides statistic information about program execution. It shows the average execution time as well as the achieved framerate.

+--------------+------------+-------------------------+------------------------------------------+
|              | Shortcut   | Command                 | Description                              |
+==============+============+=========================+==========================================+
| |image265|   |            | Statistics Start/Stop   | Switch statistics accumulation on/off.   |
+--------------+------------+-------------------------+------------------------------------------+

The **Result** group contains the **Result** tool, which supports simple communication with a machine.

+--------------+------------+-----------+-----------------------------------------------------------+
|              | Shortcut   | Command   | Description                                               |
+==============+============+===========+===========================================================+
| |image267|   |            | Result    | This tool provides simple communication with a machine.   |
+--------------+------------+-----------+-----------------------------------------------------------+

Interactive Measurement
-----------------------

The **Measure** tab shows commands that are related to interactive measurement.

.. figure:: images/measure_tab.png
   :alt: The measure tab.

   The measure tab.
 

The **Calibrate** group contains the commands for interactive calibration.

+--------------+------------+--------------------+--------------------------------------------------+
|              | Shortcut   | Command            | Description                                      |
+==============+============+====================+==================================================+
| |image270|   |            | Calibrate Origin   | Toggles the origin calibration tool on or off.   |
+--------------+------------+--------------------+--------------------------------------------------+
| |image271|   |            | Calibrate Scale    | Toggles the scale calibration tool on or off.    |
+--------------+------------+--------------------+--------------------------------------------------+

The **Overlay** group contains commands that switch graphical overlays on or off.

+--------------+------------+--------------------+---------------------------------------------------+
|              | Shortcut   | Command            | Description                                       |
+==============+============+====================+===================================================+
| |image275|   |            | Rectangular Grid   | Toggles the rectangular grid overlay on or off.   |
+--------------+------------+--------------------+---------------------------------------------------+
| |image276|   |            | Circular Grid      | Toggles the circular grid overlay on or off.      |
+--------------+------------+--------------------+---------------------------------------------------+
| |image277|   |            | Scale Bar          | Toggles the scale bar overlay on or off.          |
+--------------+------------+--------------------+---------------------------------------------------+

The **Picker** group contains the pixel value picker.

+--------------+------------+-----------+---------------------------------------+
|              | Shortcut   | Command   | Description                           |
+==============+============+===========+=======================================+
| |image279|   |            | Picker    | Interactively inspect pixel values.   |
+--------------+------------+-----------+---------------------------------------+

The **Measure** group contains commands for interactive measurement.

+--------------+------------+---------------------------------------+---------------------------------------------+
|              | Shortcut   | Command                               | Description                                 |
+==============+============+=======================================+=============================================+
| |image284|   |            | Commit Results                        | Commits the results of the selected tool.   |
+--------------+------------+---------------------------------------+---------------------------------------------+
| |image285|   |            | Toggle the counting tool on or off.   | Toggle the counting tool on or off.         |
+--------------+------------+---------------------------------------+---------------------------------------------+
| |image286|   |            | Reset Counting Tool                   | Resets the counting tool count.             |
+--------------+------------+---------------------------------------+---------------------------------------------+
| |image287|   |            | Measure Length Tool                   | Toggle the measure length tool on or off.   |
+--------------+------------+---------------------------------------+---------------------------------------------+

The **Gauging** group contains the edge inspector.

+--------------+------------+-----------------------------+-----------------------------------------------------------+
|              | Shortcut   | Command                     | Description                                               |
+==============+============+=============================+===========================================================+
| |image289|   |            | Horizontal Edge Inspector   | This tool helps finding parameters for edge inspection.   |
+--------------+------------+-----------------------------+-----------------------------------------------------------+

.. |image0| image:: images/file_open.png
.. |image1| image:: images/save.png
.. |image2| image:: images/save_as.png
.. |image3| image:: images/save_all.png
.. |image4| image:: images/delete.png
.. |image5| image:: images/commit_document.png
.. |image6| image:: images/file_open.png
.. |image7| image:: images/save.png
.. |image8| image:: images/save_as.png
.. |image9| image:: images/save_all.png
.. |image10| image:: images/delete.png
.. |image11| image:: images/commit_document.png
.. |image12| image:: images/help.png
.. |image13| image:: images/help.png
.. |image14| image:: images/snap.png
.. |image15| image:: images/live.png
.. |image16| image:: images/snap.png
.. |image17| image:: images/live.png
.. |image18| image:: images/exposure_control.png
.. |image19| image:: images/define_scale.png
.. |image20| image:: images/calibrate.png
.. |image21| image:: images/exposure_control.png
.. |image22| image:: images/define_scale.png
.. |image23| image:: images/calibrate.png
.. |image24| image:: images/zoom_in.png
.. |image25| image:: images/zoom_out.png
.. |image26| image:: images/zoom_reset.png
.. |image27| image:: images/zoom_in.png
.. |image28| image:: images/zoom_out.png
.. |image29| image:: images/zoom_reset.png
.. |image30| image:: images/zoom_fit.png
.. |image31| image:: images/zoom_fit_width.png
.. |image32| image:: images/zoom_fit_height.png
.. |image33| image:: images/zoom_fit.png
.. |image34| image:: images/zoom_fit_width.png
.. |image35| image:: images/zoom_fit_height.png
.. |image36| image:: images/show_workspace.png
.. |image37| image:: images/help.png
.. |image38| image:: images/show_camera_parameters.png
.. |image39| image:: images/show_system_globals.png
.. |image40| image:: images/show_workspace.png
.. |image41| image:: images/help.png
.. |image42| image:: images/show_camera_parameters.png
.. |image43| image:: images/show_system_globals.png
.. |image44| image:: images/delete.png
.. |image45| image:: images/commit_document.png
.. |image46| image:: images/delete.png
.. |image47| image:: images/commit_document.png
.. |image48| image:: images/locate_shape.png
.. |image49| image:: images/locate_pattern.png
.. |image50| image:: images/teach.png
.. |image51| image:: images/locate_shape.png
.. |image52| image:: images/locate_pattern.png
.. |image53| image:: images/teach.png
.. |image54| image:: images/delete.png
.. |image55| image:: images/commit_document.png
.. |image56| image:: images/delete.png
.. |image57| image:: images/commit_document.png
.. |image58| image:: images/features_brightness.png
.. |image59| image:: images/features_contrast.png
.. |image60| image:: images/features_brightness.png
.. |image61| image:: images/features_contrast.png
.. |image62| image:: images/geometry_distance.png
.. |image63| image:: images/geometry_circle.png
.. |image64| image:: images/geometry_angle.png
.. |image65| image:: images/features_area_size.png
.. |image66| image:: images/geometry_distance.png
.. |image67| image:: images/geometry_circle.png
.. |image68| image:: images/geometry_angle.png
.. |image69| image:: images/features_area_size.png
.. |image70| image:: images/geometry_count_edges.png
.. |image71| image:: images/features_count_contour_points.png
.. |image72| image:: images/features_count_areas.png
.. |image73| image:: images/geometry_count_edges.png
.. |image74| image:: images/features_count_contour_points.png
.. |image75| image:: images/features_count_areas.png
.. |image76| image:: images/delete.png
.. |image77| image:: images/commit_document.png
.. |image78| image:: images/delete.png
.. |image79| image:: images/commit_document.png
.. |image80| image:: images/features_match_contour.png
.. |image81| image:: images/features_match_pattern.png
.. |image82| image:: images/features_match_contour.png
.. |image83| image:: images/features_match_pattern.png
.. |image84| image:: images/delete.png
.. |image85| image:: images/commit_document.png
.. |image86| image:: images/delete.png
.. |image87| image:: images/commit_document.png
.. |image88| image:: images/identify_barcode.png
.. |image89| image:: images/identify_matrixcode.png
.. |image90| image:: images/identify_barcode.png
.. |image91| image:: images/identify_matrixcode.png
.. |image92| image:: images/ocr.png
.. |image93| image:: images/ocr.png
.. |image94| image:: images/delete.png
.. |image95| image:: images/commit_document.png
.. |image96| image:: images/delete.png
.. |image97| image:: images/commit_document.png
.. |image98| image:: images/histogram.png
.. |image99| image:: images/horizontal_profile.png
.. |image100| image:: images/horizontal_profile.png
.. |image101| image:: images/histogram.png
.. |image102| image:: images/horizontal_profile.png
.. |image103| image:: images/horizontal_profile.png
.. |image104| image:: images/crop.png
.. |image105| image:: images/crop.png
.. |image106| image:: images/bin.png
.. |image107| image:: images/bin.png
.. |image108| image:: images/not.png
.. |image109| image:: images/negate.png
.. |image110| image:: images/increment.png
.. |image111| image:: images/decrement.png
.. |image112| image:: images/abs.png
.. |image113| image:: images/add.png
.. |image114| image:: images/subtract.png
.. |image115| image:: images/difference.png
.. |image116| image:: images/multiply.png
.. |image117| image:: images/divide.png
.. |image118| image:: images/and.png
.. |image119| image:: images/or.png
.. |image120| image:: images/xor.png
.. |image121| image:: images/equal.png
.. |image122| image:: images/bigger.png
.. |image123| image:: images/bigger_or_equal.png
.. |image124| image:: images/smaller.png
.. |image125| image:: images/smaller_or_equal.png
.. |image126| image:: images/min.png
.. |image127| image:: images/max.png
.. |image128| image:: images/not.png
.. |image129| image:: images/negate.png
.. |image130| image:: images/increment.png
.. |image131| image:: images/decrement.png
.. |image132| image:: images/abs.png
.. |image133| image:: images/add.png
.. |image134| image:: images/subtract.png
.. |image135| image:: images/difference.png
.. |image136| image:: images/multiply.png
.. |image137| image:: images/divide.png
.. |image138| image:: images/and.png
.. |image139| image:: images/or.png
.. |image140| image:: images/xor.png
.. |image141| image:: images/equal.png
.. |image142| image:: images/bigger.png
.. |image143| image:: images/bigger_or_equal.png
.. |image144| image:: images/smaller.png
.. |image145| image:: images/smaller_or_equal.png
.. |image146| image:: images/min.png
.. |image147| image:: images/max.png
.. |image148| image:: images/convert_monochrome.png
.. |image149| image:: images/rgb.png
.. |image150| image:: images/hls.png
.. |image151| image:: images/hsi.png
.. |image152| image:: images/lab.png
.. |image153| image:: images/convert_monochrome.png
.. |image154| image:: images/rgb.png
.. |image155| image:: images/hls.png
.. |image156| image:: images/hsi.png
.. |image157| image:: images/lab.png
.. |image158| image:: images/frame_filter.png
.. |image159| image:: images/frame_filter.png
.. |image160| image:: images/median_fIlter.png
.. |image161| image:: images/median_fIlter.png
.. |image162| image:: images/blur.png
.. |image163| image:: images/blur.png
.. |image164| image:: images/blur.png
.. |image165| image:: images/sharpen.png
.. |image166| image:: images/sharpen.png
.. |image167| image:: images/horizontal_edge.png
.. |image168| image:: images/horizontal_edge.png
.. |image169| image:: images/horizontal_edge.png
.. |image170| image:: images/vertical_edge.png
.. |image171| image:: images/vertical_edge.png
.. |image172| image:: images/vertical_edge.png
.. |image173| image:: images/median_fIlter.png
.. |image174| image:: images/median_fIlter.png
.. |image175| image:: images/blur.png
.. |image176| image:: images/blur.png
.. |image177| image:: images/blur.png
.. |image178| image:: images/sharpen.png
.. |image179| image:: images/sharpen.png
.. |image180| image:: images/horizontal_edge.png
.. |image181| image:: images/horizontal_edge.png
.. |image182| image:: images/horizontal_edge.png
.. |image183| image:: images/vertical_edge.png
.. |image184| image:: images/vertical_edge.png
.. |image185| image:: images/vertical_edge.png
.. |image186| image:: images/dilation.png
.. |image187| image:: images/dilation_regions.png
.. |image188| image:: images/erosion.png
.. |image189| image:: images/erosion_regions.png
.. |image190| image:: images/opening.png
.. |image191| image:: images/opening_regions.png
.. |image192| image:: images/closing.png
.. |image193| image:: images/closing_regions.png
.. |image194| image:: images/morphological_gradient.png
.. |image195| image:: images/morphological_gradient_regions.png
.. |image196| image:: images/boundary.png
.. |image197| image:: images/boundary.png
.. |image198| image:: images/fill_holes.png
.. |image199| image:: images/dilation.png
.. |image200| image:: images/dilation_regions.png
.. |image201| image:: images/erosion.png
.. |image202| image:: images/erosion_regions.png
.. |image203| image:: images/opening.png
.. |image204| image:: images/opening_regions.png
.. |image205| image:: images/closing.png
.. |image206| image:: images/closing_regions.png
.. |image207| image:: images/morphological_gradient.png
.. |image208| image:: images/morphological_gradient_regions.png
.. |image209| image:: images/boundary.png
.. |image210| image:: images/boundary.png
.. |image211| image:: images/fill_holes.png
.. |image212| image:: images/mirror.png
.. |image213| image:: images/flip.png
.. |image214| image:: images/rotate90ccw.png
.. |image215| image:: images/rotate180.png
.. |image216| image:: images/rotate90cw.png
.. |image217| image:: images/mirror.png
.. |image218| image:: images/flip.png
.. |image219| image:: images/rotate90ccw.png
.. |image220| image:: images/rotate180.png
.. |image221| image:: images/rotate90cw.png
.. |image222| image:: images/delete.png
.. |image223| image:: images/commit_document.png
.. |image224| image:: images/delete.png
.. |image225| image:: images/commit_document.png
.. |image226| image:: images/thresholding.png
.. |image227| image:: images/connected_components.png
.. |image228| image:: images/filter.png
.. |image229| image:: images/thresholding.png
.. |image230| image:: images/connected_components.png
.. |image231| image:: images/filter.png
.. |image232| image:: images/blob_analysis.png
.. |image233| image:: images/Chart-Line.png
.. |image234| image:: images/blob_analysis.png
.. |image235| image:: images/Chart-Line.png
.. |image236| image:: images/delete.png
.. |image237| image:: images/commit_document.png
.. |image238| image:: images/delete.png
.. |image239| image:: images/commit_document.png
.. |image240| image:: images/sub_pipeline.png
.. |image241| image:: images/pipeline_import.png
.. |image242| image:: images/pipeline_export.png
.. |image243| image:: images/sub_pipeline.png
.. |image244| image:: images/pipeline_import.png
.. |image245| image:: images/pipeline_export.png
.. |image246| image:: images/mm-First.png
.. |image247| image:: images/mm-Previous.png
.. |image248| image:: images/mm-Play.png
.. |image249| image:: images/Play.png
.. |image250| image:: images/mm-Stop.png
.. |image251| image:: images/Save-Tick.png
.. |image252| image:: images/Save-and-New.png
.. |image253| image:: images/mm-First.png
.. |image254| image:: images/mm-Previous.png
.. |image255| image:: images/mm-Play.png
.. |image256| image:: images/Play.png
.. |image257| image:: images/mm-Stop.png
.. |image258| image:: images/Save-Tick.png
.. |image259| image:: images/Save-and-New.png
.. |image260| image:: images/timer.png
.. |image261| image:: images/timer.png
.. |image262| image:: images/timer.png
.. |image263| image:: images/timer.png
.. |image264| image:: images/execute.png
.. |image265| image:: images/execute.png
.. |image266| image:: images/tool_result.png
.. |image267| image:: images/tool_result.png
.. |image268| image:: images/calibrate_origin.png
.. |image269| image:: images/calibrate_scale.png
.. |image270| image:: images/calibrate_origin.png
.. |image271| image:: images/calibrate_scale.png
.. |image272| image:: images/rectangular_grid.png
.. |image273| image:: images/circular_grid.png
.. |image274| image:: images/scale_bar.png
.. |image275| image:: images/rectangular_grid.png
.. |image276| image:: images/circular_grid.png
.. |image277| image:: images/scale_bar.png
.. |image278| image:: images/picker.png
.. |image279| image:: images/picker.png
.. |image280| image:: images/commit_data_row.png
.. |image281| image:: images/counting_tool.png
.. |image282| image:: images/reset_zero.png
.. |image283| image:: images/ruler_scale.png
.. |image284| image:: images/commit_data_row.png
.. |image285| image:: images/counting_tool.png
.. |image286| image:: images/reset_zero.png
.. |image287| image:: images/ruler_scale.png
.. |image288| image:: images/gauge_hpos.png
.. |image289| image:: images/gauge_hpos.png
