Pixel Based Blob Feature
------------------------

Measures pixel-based features of a region and an associated image.

.. figure:: images/PixelBasedBlobFeatureNode.png
   :alt: 

Inputs
~~~~~~

Blob (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^

The region that defines the blob.

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The associated image.

Outputs
~~~~~~~

Feature
^^^^^^^

The blob analysis result data. The type of the result depends on the selected feature.

Comments
~~~~~~~~

The **Pixel Based Blob Feature** node calculates the feature for a single region and an associated image.

The features can be selected inside the node. The full set of features is described in the Blob Analysis chapter of the nVision User Guide.
