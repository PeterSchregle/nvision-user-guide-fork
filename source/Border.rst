HMI Border
----------

The **Border** puts a border around its child.

.. figure:: images/HMIBorderNode.png
   :alt: 

At the bottom of the **Border** node is a pin that allows it to connect one child.

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **Border**.

The **Border** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding* and *Background* styles.

Background (Type: ``SolidColorBrushByte``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The background of the **Border**.

BorderBrush (Type: ``SolidColorBrushByte``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The brush of the **Border** frame.

Thickness (Type: ``Thickness``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The thickness of the **Border** frame.

CornerRadius (Type: 'CornerRadius')
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The corner radius of the **Border** frame.

Example
~~~~~~~

Here is an example that puts a border around an image. This definition:

.. figure:: images/hmi_border_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_border.png
   :alt: 


