Imago Device Operation
----------------------

An operation on an Image device.

Inputs
~~~~~~

Device
^^^^^^

The device. The specifiy type depends on the device type selected within the node.

Sync (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^

This input can be connected to any other object. It is used to establish an order of execution.

Outputs
~~~~~~~

Sync (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^

Synchronizing object. It is used to establish an order of execution.

Comments
~~~~~~~~

The first step in using any Imago device functionality is to create the system. With a system, you can then create an Imago device, and with a device you can call any method or property on it to carry out the respective operation.

An imago system may have the following devices:

+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| device                | purpose                                                                                                                              |
+=======================+======================================================================================================================================+
| CameraLink            | Represents the CameraLink-Input module which allows capturing images over CameraLink.                                                |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| CameraTrigger         | Represents the camera trigger output.                                                                                                |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| DigitalInput          | Represents the digital inputs.                                                                                                       |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| DigitalOutput         | Represents the digital outputs.                                                                                                      |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| IOScheduler           | Controls the IOScheduler which allows to strobe out bit-values in hard real-time.                                                    |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Led                   | Represetns the Leds.                                                                                                                 |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Multiplexer           | Represents the multiplexer between trigger sources and sinks.                                                                        |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| PowerOverEthernet     | Represents the PowerOverEthernet (PoE) module, which allows gathering information about the current state of attached PoE devices.   |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| RS232                 | Represents the RS232 which allows sending and receiving date over a RS232 port.                                                      |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| RS422                 | Represents the RS422 differential IO lines.                                                                                          |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Service               | Contains everything that is no physical interface.                                                                                   |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Strobe                | Represent the strobe (current) sources.                                                                                              |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| TriggerGenerator      | This module contains functions which control the FPGA Trigger Unit.                                                                  |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| TriggerOverEthernet   | Represents the TriggerOverEthernet (ToE) module which allows the generation of GigE Action Commands.                                 |
+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------+

For a more detailed description see the Imago .NET documentation (VIB\_NET.iDevice interface and derived classes).
