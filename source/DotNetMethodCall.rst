.NET Static Method Call
-----------------------

Call a static method in a .Net assembly.

.. figure:: images/DotNetMethodCallNode.png
   :alt: 

Inputs
~~~~~~

The number and types of the inputs are determined when the assembly file name, the type name and the method name have been specified. They are determined by reflection and are the parameters of the method.

Outputs
~~~~~~~

The output type is determined when the assembly file name, the type name and the method name have been specified. It is determined by reflection and is the return value of the method (if not void).

Comments
~~~~~~~~

The parameters that specify which method to call are only editable when the ports of the node are not connected.

The first parameter is the filename of the assembly that contains the static method. You can use the browse button to browse to the desired assembly. An example of an assembly that you can try is: ``C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETCore\v4.5.1\mscorlib.dll``.

The second parameter is the class that contains the method, prefixed by its namespace, i.e. ``System.Environment``.

The third parameter is the static method to call, i.e. ``GetEnvironmentVariable(String variable)``.

Once all parameters have been specified, and it is clear which method to call, the title of the node will adapt and show a more suitable name.

Here is an example that calls the method ``GetEnvironmentVariable(String variable)`` from the ``System.Environment`` class in the ``mscorlib.dll`` assembly.

.. figure:: images/CallGetEnvironmentValue.png
   :alt: 

The example reads the value of the ``windir`` environment value and returns the string ``C:\Windows``.
