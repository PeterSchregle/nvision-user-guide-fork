Verify
------

The **Verify** tab groups commands for pattern matching.

The **Edit** group contains commands for pipeline editing.

.. figure:: images/RibbonEdit.png
   :alt: 

The first command deletes a node in the linear pipeline. The second command commits the result of the selected node to the browser, where it can be stored into the filesystem. These two commands are replicated on most tabs of the ribbon.

The **Features** group contains commands for pattern matching.

.. figure:: images/RibbonFeatures.png
   :alt: 

The commands use pattern mathching with geometric or correlation methods to calculate the similarity. Both tools allow you to teach the look of a part and match the actual image against this template.

The tools are meant to be chained one after the other, where one of the location tools is often the first tool in a chain of tools.
