BinaryRegionListOperator
------------------------

Applies a binary operation between the regions in a list.

.. figure:: images/BinaryRegionListOperatorNode.png
   :alt: 

The following operators are available: union and intersection.

Union (``|``)
             

Calculates the union of all regions in the list.

Intersection (``&``)
                    

Calculates the intersection of all regions in the list.

Inputs
~~~~~~

Regions (Type: ``RegionList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of input regions.

Operator (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the operator.

+------------+----------------+
| operator   | operation      |
+============+================+
| ``|``      | union          |
+------------+----------------+
| ``&``      | intersection   |
+------------+----------------+

Outputs
~~~~~~~

Result (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The result region.
