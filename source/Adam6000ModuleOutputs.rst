Adam 6000 DigOut
----------------

Writes digital electrical outputs to an Adam-60xx ethernet module from Adavantech.

.. figure:: images/Adam6000DigOutNode.png
   :alt: 

This node writes digital signals to an Adam-60xx module.

Inputs
~~~~~~

Module (Type: ``Adam6000Module``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An Adam 60xx module instance.

DigOut (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^

The numeric value that is written to the electrical outputs.

Sync (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^

This input can be connected to any other object. It is used to establish an order of execution.

.. figure:: images/AdamDigOutSync.png
   :alt: 

Here, the digital output is written **after** the digital input has been read.

Sometimes, if this is necessary for technical reasons, you can add a **Delay** node in order to introduce additional delay between synchronized nodes.

Outputs
~~~~~~~

Sync (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^

Synchronizing object. It is used to establish an order of execution.

Sample
~~~~~~

Here is an example:

.. figure:: images/Adam6000ModuleSample.png
   :alt: 


