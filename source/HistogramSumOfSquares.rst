Histogram Sum of Squares
------------------------

Calculates the sum of squares of all pixels in a buffer, given a histogram.

.. figure:: images/HistogramSumOfSquaresNode.png
   :alt: 

Inputs
~~~~~~

Histogram (Type: ``Histogram``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The input histogram.

Outputs
~~~~~~~

Sum (Type: Object)
^^^^^^^^^^^^^^^^^^

The sum of squares.

Comments
~~~~~~~~

The **Histogram Sum of Squares** node calculates the sum of squares of all pixel values of a buffer, given a histogram to it.

If a color buffer is used, the sum of squares is calculated channel-wise.

Sample
~~~~~~

Here is an example that calculates the sum of squares of a histogram.

.. figure:: images/BufferSumOfSquaresSample.png
   :alt: 


