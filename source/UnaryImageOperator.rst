Unary Image Operator
--------------------

Applies a unary point operation to an image. Unary point operations are applied pixel by pixel, the same way for every pixel.

.. figure:: images/UnaryImageOperatorNode.png
   :alt: 

The following operators are available:

.. figure:: images/UnaryImageOperator.png
   :alt: 

Absolute
        

Absolute.

.. figure:: images/UnaryAbsolute.png
   :alt: 

abs ( Image ) -> Result

Decrement
         

Decrement by 1.

.. figure:: images/UnaryDecrement.png
   :alt: 

Image - 1 -> Result

Increment
         

Increment by 1.

.. figure:: images/UnaryIncrement.png
   :alt: 

Image + 1 -> Result

Negate
      

Negate (2's complement).

.. figure:: images/UnaryNegate.png
   :alt: 

- Image -> Result

Not
   

Not (1's complement).

.. figure:: images/UnaryNot.png
   :alt: 

~ Image -> Result

Square Root
           

Square root.

.. figure:: images/UnarySquareRoot.png
   :alt: 

sqrt( Image ) -> Result

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Operator (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the operator.

+------------+---------------------------+
| operator   | operation                 |
+============+===========================+
| ``++``     | increment                 |
+------------+---------------------------+
| ``--``     | decrement                 |
+------------+---------------------------+
| ``-``      | negate (2's complement)   |
+------------+---------------------------+
| ``!``      | not (1's complement)      |
+------------+---------------------------+
| ``abs``    | absolute                  |
+------------+---------------------------+
| ``sqrt``   | square root               |
+------------+---------------------------+

Outputs
~~~~~~~

Result (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^

The result image.
