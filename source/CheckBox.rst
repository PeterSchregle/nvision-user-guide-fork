HMI CheckBox
------------

Buttons are used to switch or visualize state, as well as to trigger actions. The **CheckBox** has two states, checked or not.

.. figure:: images/HMICheckBoxNode.png
   :alt: 

A **CheckBox** toggles between the checked and unchecked states when it is clicked.

At the bottom of the **CheckBox** node is a pin that allows it to connect one child.

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **CheckBox**.

The **CheckBox** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding*, *Foreground*, *Background*, *Font* and *Padding* styles.

Name (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^

The text that is displayed within the button. This text is only displayed, when no child is connected. If a child is connected, the visual definition of the child is used.

Outputs
~~~~~~~

IsChecked (Type: ``boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``True`` if the checkbox is checked, ``False`` otherwise.

Example
~~~~~~~

Here is an example that shows a simple **CheckBox**. This definition:

.. figure:: images/simple_checkbox_def.png
   :alt: 

creates the following user interface:

.. figure:: images/simple_checkbox.png
   :alt: 


