StdDev
------

Calculates the standard deviation of all pixels in a buffer.

.. figure:: images/BufferStdDevNode.png
   :alt: 

Inputs
~~~~~~

Buffer (Type: ``Buffer``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input buffer.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

An optional region.

Outputs
~~~~~~~

StdDev (Type: Object)
^^^^^^^^^^^^^^^^^^^^^

The standard deviation.

Comments
~~~~~~~~

The **StdDev** node calculates the standard deviation of all pixel values of a buffer.

If the *Region* input is connected, the calculation of the standard deviation is constrained to the pixels within the region only, otherwise the whole buffer is used.

If a color buffer is used, the standard deviation is calculated channel-wise.

The standard deviation is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The standard deviation is a measure of how much a statistical distribution is spread out. The standard deviation is the square root of the variance.

The standard deviation is calculated according to this formula:

:math:`\sigma =  \sqrt[]{ \frac {1}{n}  \sum x^2 -  \mu^2 }`

Sample
~~~~~~

Here is an example that calculates the standard deviation of an image.

.. figure:: images/BufferStdDevSample.png
   :alt: 


