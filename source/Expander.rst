HMI Expander
------------

The **Expander** puts an expandable and collapsibal area around its child.

.. figure:: images/HMIExpanderNode.png
   :alt: 

At the bottom of the **Expander** node is a pin that allows it to connect one child.

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **Expander**.

The **Expander** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding* and *Background*, *Foreground* and *Font* styles.

Header (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The title of the **Expander**.

Example
~~~~~~~

Here is an example that puts an expander around a profile. This definition:

.. figure:: images/hmi_expander_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_expander.png
   :alt: 

The profile display can be hidden or shown by clicking the little arrow button at the top-right corner of the expander.
