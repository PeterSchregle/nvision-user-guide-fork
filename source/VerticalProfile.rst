Vertical Profile
----------------

Calculates a vertical profile of an image to show the greyscale/color distribution.

.. figure:: images/VerticalProfileNode.png
   :alt: 

The Vertical Profile node gives you information about the greyscale/color distribution along a vertical line. The line can have a certain thickness.

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Position (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The horizontal coordinate of the vertical line.

Thickness (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The thickness in pixel of the vertical line.

Outputs
~~~~~~~

Profile (Type: ``Profile``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The profile.

Sample
~~~~~~

Here is an example:

.. figure:: images/ProfileSample.png
   :alt: 


