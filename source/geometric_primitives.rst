Geometric Primitives
--------------------

The geometric primitives are used to model geometries. They are drawn here with respect to a coordinate system used in images, where the positive x-axis extends to the right and the positive y-axis extends downwards. In such a coordinate system, positive angles are in clockwise direction.

Direction
=========

A ``Direction`` is a vector of unit length in two-dimensional space. It serves the same purpose as an angle, i.e. it represents a direction in the two-dimensional Euclidean space *R*\ 2.

.. figure:: images/direction.png
   :alt: The direction geometric primitive.

   The direction geometric primitive.
You can default-construct a direction (corresponding to an angle of 0 degrees), you can convert it to an angle, calculate the orthogonal and opposite directions, add and subtract directions as well as find the bisector between two directions.

Vector
======

A ``Vector`` is a vector of arbitrary length in two-dimensional Euclidean space *R*\ 2.

.. figure:: images/vector.png
   :alt: The vector geometric primitive.

   The vector geometric primitive.
You can default-construct a vector (of length 0) or you can pass a coordinate pair. With a given vector, you can calculate its direction and length. You can also read back its horizontal and vertical extents. You can also find its direction, normal vector or calculate the dot product with another vector.

Point
=====

A ``Point`` is a point at an arbitrary position in two-dimensional Euclidean space *R*\ 2.

.. figure:: images/point.png
   :alt: The point geometric primitive.

   The point geometric primitive.
You can default-construct a point (at the origin) or you can pass a coordinate pair. You can also read back it’s horizontal and vertical position. The difference of two points is the directed distance between them, i.e. it is a vector.

Line
====

A ``Line`` is a line at an arbitrary position and direction in two-dimensional Euclidean space *R*\ 2 that extends into infinity at both ends.

.. figure:: images/line.png
   :alt: The line geometric primitive.

   The line geometric primitive.
You can default-construct a line (horizontal at the origin) or you can pass a point and a direction. There are various ways you can modify a line, which boil down to the fact that you can modify its origin and direction.

Ray
===

A ``Ray`` is a line starting at an arbitrary position and extending into infinity along its direction in two-dimensional Euclidean space *R*\ 2.

.. figure:: images/ray.png
   :alt: The ray geometric primitive.

   The ray geometric primitive.
You can default-construct a ray (horizontal starting at origin along the positive x-axis) or you can pass a point and a direction. There are various ways you can modify a ray, which boil down to the fact that you can modify its origin and direction.

LineSegment
===========

A ``LineSegment`` is a line between two arbitrary points in two-dimensional space *R*\ 2.

.. figure:: images/line_segment.png
   :alt: The line segment geometric primitive.

   The line segment geometric primitive.
You can default-construct a line segment (between origin and point [1, 0]) or you can pass a point, direction and extent, or you can pass two points. There are various ways you can modify a line segment, which boil down to the fact that you can modify its origin, direction and extent.

Box
===

A ``Box`` is a rectangle of arbitrary size located at an arbitrary point in two-dimensional Euclidean space *R*\ 2, whose sides are parallel to the coordinate axes.

.. figure:: images/box.png
   :alt: The box geometric primitive.

   The box geometric primitive.
You can default-construct a box or you can pass a point and a vector.

Rectangle
=========

A ``Rectangle`` is characterized by a center point *P*, two radii *r*\ \ *x*\  and *r*\ \ *y*\  and an angle *α*.

.. figure:: images/rectangle.png
   :alt: The rectangle geometric primitive.

   The rectangle geometric primitive.
A rectangle can be created from a center point, a diagonal vector and an angle given in radians.

Circle
======

A ``Circle`` is located at its center point *P* and contains the set of points having distance radius to the center point.

.. figure:: images/circle.png
   :alt: The circle geometric primitive.

   The circle geometric primitive.
You can default-construct a circle or you can pass a point and a radius. A circle is characterized by a center point P and the radius r.

Ellipse
=======

An ``Ellipse`` is characterized by a center point *P*, two radii *r*\ \ *x*\  and *r*\ \ *y*\  and an angle *α*.

.. figure:: images/ellipse.png
   :alt: The ellipse geometric primitive.

   The ellipse geometric primitive.
An ellipse can be created from a center point, a diagonal vector and an angle given in radians.
