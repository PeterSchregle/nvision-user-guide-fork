Convert to Rgb
--------------

Converts an image to the RGB color model.

.. figure:: images/ConvertToRgbNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image. This image can be of any color model.

Outputs
~~~~~~~

Rgb (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^

The output image in the RGB color model.

Comments
~~~~~~~~

This function converts an image to the RGB color model, regardless of the color space of the input image.
