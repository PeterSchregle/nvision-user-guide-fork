Convert to HSI
--------------

Converts an image to the HSI color model.

.. figure:: images/ConvertToHsiNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image. This image can be of any color model.

Outputs
~~~~~~~

Hsi (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^

The output image in the HSI color model.

Comments
~~~~~~~~

This function converts an image to the HSI color model, regardless of the color space of the input image.

Here is an example of the original color image:

.. figure:: images/rgbimg.png
   :alt: 

And here is how the hue, saturation and intensity channels look side by side.

.. figure:: images/hsi_sbs.png
   :alt: 

Sample
~~~~~~

Here is an example:

.. figure:: images/ConvertToHsiSample.png
   :alt: 


