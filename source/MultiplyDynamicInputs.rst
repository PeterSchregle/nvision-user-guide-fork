Multiply
--------

Multiplies two or more numbers.

.. figure:: images/MultiplyNode.png
   :alt: 

Inputs
~~~~~~

A (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^

The first operand.

B (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^

The second operand.

C...Z (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

Additional operands, added on demand.

Outputs
~~~~~~~

Product (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The product of all operands.

Comments
~~~~~~~~

This node calculates the product of multiple operands.

The ports for the inputs are added dynamically on demand. The node starts with the two inputs A and B. If both are connected, a third input named C is added. At a maximum, 26 inputs are possible. Inputs can also be deleted by removing the connection to them.
