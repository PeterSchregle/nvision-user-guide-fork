Take While
----------

**Take While** takes items from a list as long as a predicate is true. The predicate is specified itself as a graphical pipeline. The predicate is evaluated for each item of the list, beginning with the first item. As long as the predicate evaluates to true, list items are added to the output list. As soon as the predicate evaluates to false, the iteration is stopped.

.. figure:: images/TakeWhileNode.png
   :alt: 

Outside View
~~~~~~~~~~~~

At the outside, the **Take While** node appears like a single node, with inputs and outputs.

Inputs
^^^^^^

List (Type: ``List<T>``)
''''''''''''''''''''''''

The input list.

Outputs
^^^^^^^

FilteredList (Type: ``List<T>``)
''''''''''''''''''''''''''''''''

The output list, consisting of a part of the input list, as determined by consecutive execution of the predicate.

Inside View
~~~~~~~~~~~

Inside, the **Take While** node looks like a normal branched pipeline. This pipeline is used to specify the predicate, which is evaluated for each item, until the predicate turns false.

.. figure:: images/TakeWhileNodeInside.png
   :alt: 

Inputs
^^^^^^

Item (Type: ``T``)
''''''''''''''''''

One item of the input list.

ItemIndex (Type: ``Int32``)
'''''''''''''''''''''''''''

The index of the item of the input list.

Outputs
^^^^^^^

Result (Type: ``Boolean``)
''''''''''''''''''''''''''

The boolean predicate.

Comments
~~~~~~~~

**Take While** nodes can be nested in similar ways as subpipelines.

The nesting is shown with a breadcrumbs control at the top.

.. figure:: images/breadcrumbs.png
   :alt: 

Double-click a **Take While** node to enter it, and use the breadcrumbs control to exit it and go up one or more levels.

By default, a **Take While** node has one input and one output.

You can add additional inputs with the **Add Input (Ctrl-I)** command. You can edit the port names by clicking on them and typing a new name. The type of the ports is determined by the first connection made to them, either from the outside or from the inside.

Sample
~~~~~~

Here is an example:

.. figure:: images/TakeWhileSample.png
   :alt: 

.. figure:: images/TakeWhileSample2.png
   :alt: 

It takes items from the first list (0, 1, ..., 9) while the predicate (< 2) is true and returns the resulting list (0, 1).
