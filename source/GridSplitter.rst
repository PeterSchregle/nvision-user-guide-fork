HMI GridSplitter
----------------

The **GridSplitter** allows you to enter a splitter inside a grid that enables interactive resize of the neighboring columns or rows.

.. figure:: images/HMIGridSplitterNode.png
   :alt: 

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **GridSplitter**.

The **GridSplitter** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin* and *Background* styles.

Comments
~~~~~~~~

The **GridSplitter** should get it's own grid cell(s), so that it is always visible.
