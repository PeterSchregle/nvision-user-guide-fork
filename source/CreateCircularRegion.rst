|image0| Circular Region
------------------------

Creates a circular region.

.. figure:: images/CreateCircularRegionNode.png
   :alt: 

Inputs
~~~~~~

Diameter (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The diameter of the circular region.

Outputs
~~~~~~~

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The circular region.

Comments
~~~~~~~~

The region is centered on the origin. Regions centered on the origin are suitable for morphological operations, such as erosion, dilation, etc. The region is used as a structuring element and the shape of the region affects the morphological operation.

.. |image0| image:: images/circular_region.png
