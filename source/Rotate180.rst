Rotate 180
----------

Rotates an image by 180 degrees.

.. figure:: images/Rotate180Node.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

This function rotates an image by 180 degrees.

.. figure:: images/Rotated180.png
   :alt: 

Sample
~~~~~~

Here is an example that rotates an image.

.. figure:: images/Rotate180Sample.png
   :alt: 


