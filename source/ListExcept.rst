Except
------

Calculates the difference of two sets.

.. figure:: images/ListExceptNode.png
   :alt: 

Inputs
~~~~~~

ListA (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The first input list, whose distinct elements form the first set for the set difference operation.

ListB (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The second input list, whose distinct elements form the second set for the set difference operation.

Outputs
~~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The output list, which consists of the set difference of the two lists, i.e. the output list consists of the items of the first input list that are not also in the second input list.

Comments
^^^^^^^^

The lists are treated as mathematical sets, which can have distinct objects only.
