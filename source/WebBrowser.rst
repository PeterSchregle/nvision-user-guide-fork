HMI WebBrowser
--------------

A **WebBrowser** can be used to display a webpage.

.. figure:: images/HMIWebBrowserNode.png
   :alt: 

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **WebBrowser**.

The **WebBrowser** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment* and *Margin* styles.

Source (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The URL of the webpage.

Example
~~~~~~~

Here is an example that shows a **WebBrowser**. This definition:

.. figure:: images/hmi_webbrowser_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_webbrowser.png
   :alt: 


