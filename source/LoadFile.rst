Load File
---------

Loads an entire file.

.. figure:: images/LoadFile.png
   :alt: 

Inputs
~~~~~~

Filename
^^^^^^^^

The path to the file.

Outputs
~~~~~~~

Text
^^^^

The contents of the file.

Comments
~~~~~~~~

The **Load File** node checks the time the file was last written to, and compares it with the time it last read from the file. If the file has been written to after it has been last read, the node reads the file again.

Sample
~~~~~~

You can use standard text operations to work with the file contents. Here is an example that splits a file into lines.

.. figure:: images/LoadFileSample.png
   :alt: 


