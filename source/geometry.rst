Geometry
========

.. toctree::
   :maxdepth: 2
   
   geometry_intro
   geometric_primitives
   geometric_transformations
   graphics
