Circular Edge Points Info
-------------------------

Finds edges and their positions in an image, along linear search rays.

.. figure:: images/CircularEdgePointsInfoNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The source image. This should be a monochrome image. If the image is a color image, it is internally converted to monochrome, before the edges are searched.

Orientation (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The orientation, the default is ``InsideToOutside``. The search lines start at the center of the image and extend outwards radially. ``InsideToOutside`` or ``OutsideToInside`` specifies the direction of the edge search along these lines.

Polarity (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The type of the grayscale transition to look for: ``Rising``, ``Falling`` or ``Both``, the default is ``Rising``. ``Rising`` only finds edges going from dark to bright, ``Falling`` only finds edges going from bright to dark, when searching in the direction specified by **Orientation**.

Selector (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The selection of the edges: ``First`` finds the first edge and ``Last`` finds the last edge when searching in the direction specified by **Orientation**. ``Best`` (the default) finds the best edge, where the best edge is the edge with the steepest gradient. ``All`` finds all edges.

Spacing (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The angular spacing of the search lines. The spacing is expressed in radians and is the angular difference between two search lines.

Thickness (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The thickness of the search lines, expressed in pixels.

Sigma (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the **Sigma** parameter of a gaussian smoothing filter, which is applied to the averaged profile data of the search line. The default is 2, which specifies considerable smoothing. Thus, the overall smoothing of the greyvalue profile comes both from the smoothing effect of averaging multiple rows or columns, combined with additional smoothing using a gaussian filter.

The effect of the smoothing on the greyvalue profile can be illustrated with the following pictures. First, no smoothing, i.e. **Sigma == 0**:

.. figure:: images/gauge_edges_info_profile_sigma0.png
   :alt: 

Then, considerable smoothing, i.e. **Sigma == 5**:

.. figure:: images/gauge_edges_info_profile_sigma5.png
   :alt: 

You can see that stronger smoothing in the profile flattens out weak edges and makes stronger edges less steep.

Threshold (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To find edges, the grayvalue profile is differentiated twice in order to find inflection points. The inflection points correspond to the subpixel position of the edge (zero crossings in the second derivative), and the value of the first derivative corresponds to the steepness of the edge. The threshold parameter specifies the minimum steepness and thereby controls what is considered to be an edge. The default is 15, which specifies a considerable steep edge.

The effect of the smoothing on the gradient profile can be illustrated with the following pictures. First, no smoothing, i.e. **Sigma == 0**:

.. figure:: images/gauge_edges_info_gradient_sigma0.png
   :alt: 

Then, considerable smoothing, i.e. **Sigma == 5**:

.. figure:: images/gauge_edges_info_gradient_sigma5.png
   :alt: 

You can see how stronger smoothing in the profile weakens the edge gradients.

Outputs
~~~~~~~

Info (Type: 'Edge2dInfoList')
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A list of ``Edge2dInfo`` objects. An ``Edge2dInfo`` contains a **Rectangle**. which enconmpasses the region of interest of the specific search line, the **Profile**. which is the averaged brightness profile along the specific search line, the **Gradient**, which is the gradient information along the search line and the **Edges**, an ``Edge2dList`` which contains the edges along the specific searchline. An ``Edge2d`` contains a two-dimensional subpixel **Position**, the interpolated **Grey** value at the position and the **Strength**, which is the interpolated gradient value or steepness at the position. The **Position**\ s of the edges are with respect to top-left corner of the image. They are not affected by the **Orientation** of the search, i.e. ``LeftToRight`` or ``RightToLeft``, or also ``TopToBottom`` or ``BottomToTop`` return the same position values.

Comments
~~~~~~~~

The **Circular Edge Points info** node finds edge positions in an image. Usually, the found edge positions are used to fit some geometric figure.

See Also
~~~~~~~~

There is also the **Circular Edge Points** node, which calculates the edge information only.

The **Fit Line** and **Robust Fit Line** nodes can be used to fit a line to a set of points.

The **Fit Circle** and **Robust Fit Circle** nodes can be used to fit a circle to a set of points.

The **Fit Ellipse** and **Robust Fit Ellipse** nodes can be used to fit an ellipse to a set of points.
