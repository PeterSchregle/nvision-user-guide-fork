Point
-----

Creates a point.

.. figure:: images/PointInt32Node.png
   :alt: 

A point is a geometric entity, representing a location in the two-dimensional cartesian plane.

.. figure:: images/point.png
   :alt: 

Inputs
~~~~~~

X (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^

The horizontal coordinate.

Y (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^

The vertical coordinate.

Outputs
~~~~~~~

PointInt32 (Type: ``PointInt32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The point.
