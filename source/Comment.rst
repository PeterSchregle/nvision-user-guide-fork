Comment
-------

Comments a pipeline.

.. figure:: images/CommentNode.png
   :alt: 

A **Comment** can be used to add text to a pipeline. It does not participate in pipeline execution, but it serves as documentation for a pipeline.

A **Comment** can be moved around freely on the pipeline canvas. The size of a comment is determined by the text included in the comment.

Text can be entered by clicking into the comment area and typing.
