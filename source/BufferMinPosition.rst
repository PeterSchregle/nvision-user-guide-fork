Min Position
------------

Calculates the position of the minimum value of a buffer.

.. figure:: images/BufferMinPositionNode.png
   :alt: 

Inputs
~~~~~~

Buffer (Type: ``Buffer``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input buffer.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

An optional region.

Outputs
~~~~~~~

MinPosition (Type: ``Index3d``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The position of the minimum value.

Comments
~~~~~~~~

The **Min Position** node calculates the (first occurrence of the) position of the minimum value of a buffer.

If the *Region* input is connected, the calculation of the minimum is constrained to the pixels within the region only, otherwise the whole buffer is used.

If a color buffer is used, the minimum length color vector is calculated.
