Robust Fit Line
---------------

Fits a line to a set of two or more points. The fit is a robust one, i.e. it detects outliers, which are not used in the fit. Besides the fitted line, the node also returns the set of inliers (i.e. the points that have been used to fit the line) as well as the outliers (i.e. the points that have been rejected and were not used to fit the line).

.. figure:: images/RobustFitLineNode.png
   :alt: 

Inputs
~~~~~~

Points (Type: ``PointList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of points.

Sigma (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

This is the standard deviation of the measurment error (default = 1.0). It affects the behaviour of the RANSAC algorithm.

Alpha (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

This is the probabilty that at least one of the random samples of points is free from outliers (default = 0.95). It affects the behaviour of the RANSAC algorithm.

Outputs
~~~~~~~

Line (Type: 'Line')
^^^^^^^^^^^^^^^^^^^

The fitted line.

Inliers (Type: 'PointList')
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The points that are considered inliers, and are therefore used to fit the line.

Outliers (Type: 'PointList')
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The points that are considered outliers, and do not take part in the line fit.

Comments
~~~~~~~~

The **Robust Fit Line** node fits a line to a set of two or more points. It uses the RANSAC algorithm to detect outliers. The line is fitted to the inliers only.

See Also
~~~~~~~~

There is also the **Fit Line** node, which does not use RANSAC and fits the line to all points.

Sample
~~~~~~

Here is an **example** that detects several points in an image and fits a line to the set of points. The **example** allows you to interactively explore the parameters of the **Robust Fit Line** node.

You can click on the **hyperlink** to load the sample into **nVision**.
