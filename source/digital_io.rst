Digital IO
==========

**nVision** has support for digital IO. This is helpful to control machinery from image processing applications or to react upon input signals sent from machinery. Digital IO can either use digital IO modules or a PLC (programmable logic controller).

Advantech Adam-60xx Ethernet Modules
------------------------------------

**nVision** supports various digital IO modules from Advantech (`http:www.advantech.com <http://www.advantech.com>`__).

.. figure:: images/ADAM-6050_S.jpg
   :alt: 

The modules are connected via LAN or wireless LAN to a PC and support a variety of input/output lines. The modules may have additional features, such as counters, etc. but these are not supported.

.. raw:: html

   <table>
   <colgroup>
   <col width="20%" />
   <col width="12%" />
   <col width="56%" />
   </colgroup>
   <thead>
   <tr class="header">
   <th align="left">

module

.. raw:: html

   </th>
   <th align="left">

bus

.. raw:: html

   </th>
   <th align="left">

features

.. raw:: html

   </th>
   </tr>
   </thead>
   <tbody>
   <tr class="odd">
   <td align="left">

ADAM-6050

.. raw:: html

   </td>
   <td align="left">

LAN

.. raw:: html

   </td>
   <td align="left">

12 digital inputs, 6 digital outputs

.. raw:: html

   </td>
   </tr>
   <tr class="even">
   <td align="left">

ADAM-6050W

.. raw:: html

   </td>
   <td align="left">

WLAN

.. raw:: html

   </td>
   <td align="left">

12 digital inputs, 6 digital outputs

.. raw:: html

   </td>
   </tr>
   <tr class="odd">
   <td align="left">

ADAM-6051

.. raw:: html

   </td>
   <td align="left">

LAN

.. raw:: html

   </td>
   <td align="left">

12 digital inputs, 2 digital outputs

.. raw:: html

   </td>
   </tr>
   <tr class="even">
   <td align="left">

ADAM-6051W

.. raw:: html

   </td>
   <td align="left">

WLAN

.. raw:: html

   </td>
   <td align="left">

12 digital inputs, 2 digital outputs

.. raw:: html

   </td>
   </tr>
   <tr class="odd">
   <td align="left">

ADAM-6052

.. raw:: html

   </td>
   <td align="left">

LAN

.. raw:: html

   </td>
   <td align="left">

8 digital inputs, 8 digital outputs

.. raw:: html

   </td>
   </tr>
   <tr class="even">
   <td align="left">

ADAM-6060

.. raw:: html

   </td>
   <td align="left">

LAN

.. raw:: html

   </td>
   <td align="left">

6 digital inputs, 6 digital outputs

.. raw:: html

   </td>
   </tr>
   <tr class="odd">
   <td align="left">

ADAM-6060W

.. raw:: html

   </td>
   <td align="left">

WLAN

.. raw:: html

   </td>
   <td align="left">

6 digital inputs, 6 digital outputs

.. raw:: html

   </td>
   </tr>
   <tr class="even">
   <td align="left">

ADAM-6066

.. raw:: html

   </td>
   <td align="left">

LAN

.. raw:: html

   </td>
   <td align="left">

6 digital outputs

.. raw:: html

   </td>
   </tr>
   </tbody>
   </table>

The following nodes are available:

.. raw:: html

   <table>
   <colgroup>
   <col width="14%" />
   <col width="85%" />
   </colgroup>
   <thead>
   <tr class="header">
   <th align="left">

node

.. raw:: html

   </th>
   <th align="left">

purpose

.. raw:: html

   </th>
   </tr>
   </thead>
   <tbody>
   <tr class="odd">
   <td align="left">

Adam6000 Module

.. raw:: html

   </td>
   <td align="left">

An Advantech Adam-6000 module abstraction, which is needed to intialize the module and get information from it.

.. raw:: html

   </td>
   </tr>
   <tr class="even">
   <td align="left">

Read Inputs

.. raw:: html

   </td>
   <td align="left">

This is needed to read from digital inputs.

.. raw:: html

   </td>
   </tr>
   <tr class="odd">
   <td align="left">

Write Outputs

.. raw:: html

   </td>
   <td align="left">

This needed to write to digital outputs.

.. raw:: html

   </td>
   </tr>
   </tbody>
   </table>

The **Adam6000 Module** node establishes a connection to an Adam device. The IP Address and Port inputs are used to identify the module. The Type input is used to select the connected type of the module. The Module output can be fed into the **Read Inputs** or **Write Outputs** nodes.

The **Read Inputs** node reads the values from the electrical inputs. The DigIn output contains the state of the electrical inputs in the form of a binary integer.

The **Write Outputs** node writes the value from its DigOut input to its electrical outputs.

The Sync inputs of both the **Read Inputs** and **Write Outputs** nodes can be used to establish a defined order.

The **Read Inputs** node can be polled at regular intervals using the **nVision** timer.

Beckhoff TwinCAT3
-----------------

**nVision** supports direct connection to a Beckhoff PLC. (`http:www.beckhoff.de <http://www.beckhoff.de>`__).

You need to install the Beckhoff TwinCAT3 software in order to use the modules from within **nVision**.

The following nodes are available:

.. raw:: html

   <table>
   <colgroup>
   <col width="16%" />
   <col width="83%" />
   </colgroup>
   <thead>
   <tr class="header">
   <th align="left">

node

.. raw:: html

   </th>
   <th align="left">

purpose

.. raw:: html

   </th>
   </tr>
   </thead>
   <tbody>
   <tr class="odd">
   <td align="left">

Beckhoff PLC

.. raw:: html

   </td>
   <td align="left">

Establishes the connection to the PLC.

.. raw:: html

   </td>
   </tr>
   <tr class="even">
   <td align="left">

Read Symbol

.. raw:: html

   </td>
   <td align="left">

Reads from a symbol in the PLC.

.. raw:: html

   </td>
   </tr>
   <tr class="odd">
   <td align="left">

Write Symbol

.. raw:: html

   </td>
   <td align="left">

Writes to a symbol in the PLC.

.. raw:: html

   </td>
   </tr>
   </tbody>
   </table>

The **Beckhoff PLC** node establishes a connection to a PLC over the ADS protocol. It needs an AMS NetId and a Port as input, because this is needed for the unique identification of ADS devices. The output of the **Beckhoff PLC** node is the PLC itself, which can be fed into the **Read Symbol** or **Write Symbol** nodes.

The **Read Symbol** node reads a symbol from the PLC. The symbol can be choosen from the list at the Symbol input port. This list contains all available symbols in the connected PLC. The **Read Symbol** node has two outputs. The output port Value gives the value of the accessed symbol. The output port Info gives information about the accessed symbol. These information can be accessed e.g. by the **Get Property** node.

The **Write Symbol** node writes a symbol to the PLC at the input port. The symbol can be choosen from the list at the Symbol input port. This list contains all available symbols in the connected PLC. The new value of the symbol can be set on the input port Value.

The Sync inputs of both the **Read Symbol** and **Write Symbol** nodes can be used to establish a defined order.

The **Read Symbol** node can be polled at regular intervals using the **nVision** timer.

Data Translation OpenLayers
---------------------------

**nVision** supports various digital IO modules from Data Translation (`http:www.datatranslation.com <http://www.datatranslation.com>`__).

.. figure:: images/dt9817.jpg
   :alt: 

The modules are connected via USB or PCI buses to a PC and support a variety of input/output lines. The modules may have additional features, such as counters, etc. but these are not supported.

.. raw:: html

   <table>
   <colgroup>
   <col width="18%" />
   <col width="11%" />
   <col width="56%" />
   </colgroup>
   <thead>
   <tr class="header">
   <th align="left">

module

.. raw:: html

   </th>
   <th align="left">

bus

.. raw:: html

   </th>
   <th align="left">

features

.. raw:: html

   </th>
   </tr>
   </thead>
   <tbody>
   <tr class="odd">
   <td align="left">

DT9817

.. raw:: html

   </td>
   <td align="left">

USB

.. raw:: html

   </td>
   <td align="left">

28 digital inputs/outputs

.. raw:: html

   </td>
   </tr>
   <tr class="even">
   <td align="left">

DT9817-H

.. raw:: html

   </td>
   <td align="left">

USB

.. raw:: html

   </td>
   <td align="left">

28 digital inputs/outputs

.. raw:: html

   </td>
   </tr>
   <tr class="odd">
   <td align="left">

DT9817-R

.. raw:: html

   </td>
   <td align="left">

USB

.. raw:: html

   </td>
   <td align="left">

8 digital inputs, 8 digital outputs

.. raw:: html

   </td>
   </tr>
   <tr class="even">
   <td align="left">

DT9835

.. raw:: html

   </td>
   <td align="left">

USB

.. raw:: html

   </td>
   <td align="left">

64 digital inputs/outputs

.. raw:: html

   </td>
   </tr>
   <tr class="odd">
   <td align="left">

DT335

.. raw:: html

   </td>
   <td align="left">

PCI

.. raw:: html

   </td>
   <td align="left">

32 digital inputs/outputs

.. raw:: html

   </td>
   </tr>
   <tr class="even">
   <td align="left">

DT351

.. raw:: html

   </td>
   <td align="left">

PCI

.. raw:: html

   </td>
   <td align="left">

8 digital inputs, 8 digital outputs

.. raw:: html

   </td>
   </tr>
   <tr class="odd">
   <td align="left">

DT340

.. raw:: html

   </td>
   <td align="left">

PCI

.. raw:: html

   </td>
   <td align="left">

32 digital inputs, 8 digital outputs

.. raw:: html

   </td>
   </tr>
   </tbody>
   </table>

You need to install the Data Translation OpenLayers software in order to use the modules from within **nVision**. We have used OpenLayers 7.5 to implement the support, so OpenLayers 7.5 or higher is needed.

The following nodes are available:

.. raw:: html

   <table>
   <colgroup>
   <col width="16%" />
   <col width="83%" />
   </colgroup>
   <thead>
   <tr class="header">
   <th align="left">

node

.. raw:: html

   </th>
   <th align="left">

purpose

.. raw:: html

   </th>
   </tr>
   </thead>
   <tbody>
   <tr class="odd">
   <td align="left">

OpenLayers System

.. raw:: html

   </td>
   <td align="left">

Provide information about the installed DT OpenLayers software and the connected devices.

.. raw:: html

   </td>
   </tr>
   <tr class="even">
   <td align="left">

OpenLayers Device

.. raw:: html

   </td>
   <td align="left">

A DT OpenLayers module abstraction, which is needed to intialize the module and get information from it.

.. raw:: html

   </td>
   </tr>
   <tr class="odd">
   <td align="left">

Read Inputs

.. raw:: html

   </td>
   <td align="left">

This is needed to read from digital inputs.

.. raw:: html

   </td>
   </tr>
   <tr class="even">
   <td align="left">

Write Outputs

.. raw:: html

   </td>
   <td align="left">

This needed to write to digital outputs.

.. raw:: html

   </td>
   </tr>
   </tbody>
   </table>

The **OpenLayers System** node enumerates the OpenLayers devices connected to the PC. The Modules output delivers a list of conected devices. The Version output displays the version of the installed OpenLayers software.

The **OpenLayers Module** node establishes a connection to an OpenLayers device. The Name input is needed to identify the module. You can find out the name by inspecting the output of the **OpenLayers System** node. The Module output can be fed into the **Read Inputs** or **Write Outputs** nodes.

The **Read Inputs** node reads the values from the electrical inputs. The DigIn output contains the state of the electrical inputs in the form of an integer.

The **Write Outputs** node writes the value from its DigOut input to its electrical outputs.

The Sync inputs of both the **Read Inputs** and **Write Outputs** nodes can be used to establish a defined order.

The **Read Inputs** node can be polled at regular intervals using the **nVision** timer.

Here is an example of how to find out which DT OpenLayers devices are connected.

.. figure:: images/dtol_enumeration.png
   :alt: 

If you know the name of a DT OpenLayers device, you can also type it directly to initialize the module. The value that you want to output on the digital output is typically not typed in, but calculated somewhere else. Also, the value that is input via the digital inputs is typically used somewhere else in the pipeline.

.. figure:: images/dtol_read_write.png
   :alt: 

Imago VisionBox AGE-X
---------------------

**nVision** supports the VisionBox AGE-X industrial PCs from `Imago Technologies <http://imago-technologies.com>`__.

.. figure:: images/VisionBox1.png
   :alt: 

.. figure:: images/VisionBox2.png
   :alt: 

The VisionBox AGE-X is developed for machine vision and has special features for it. But inside it works like a normal PC. The VisionBox AGE-X can boot from a USB 2.0 or eSATA device. The idea is to work with an external hard disk with a normal operation system and an IDE. Boot the system from it and develop directly on the AGE-X. Impuls **nVision** can be installed on the VisionBox AGE-X. Tip: The Microsoft Windows remote desktop (RDP) is very powerful. Using it you can develop from your normal desktop.

Installation
~~~~~~~~~~~~

VisionBox AGE-X device driver
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After booting Windows the first time the OS asks for some driver. The delivery package contains all drivers you need. Other drivers can (must if you need this functionality) be installed manually. Select the desired installer and follow the instructions. One of these drivers is for the AGE-X functionality.

.. figure:: images/VisionBoxDeviceManager.png
   :alt: 

Imago SDK
^^^^^^^^^

To use the VisionBox AGE-X within nVision, the Imago SDK needs to be installed. The installer includes the SDK. Run the installer for your system: ``AGE-X Setup 32bit.exe`` or ``AGE-X Setup 64bit.exe``. Select the desired installer and follow the instructions.

**nVision** pipelines that make use of the Imago nodes can run on a VisionBox AGE-X only. They can be loaded on a different PC (if the Imago SDK is installed on this PC), but they obviously cannot run on non-VisionBox hardware.

Build an Impuls nVision project
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Start nVision, load an image, open a pipeline and you will find the AGE-X nodes in the menu.

.. figure:: images/VisionBoxAgeXNodes.png
   :alt: 

The next pages give an overview of the AGE-X functionality and the best way to use it within **nVision**. This is only an introduction of the general use. The Imago SDK contains a ``VIB_NET.chm`` reference for the whole functionality and function description.

Step 1 - Opening the system
^^^^^^^^^^^^^^^^^^^^^^^^^^^

On the first step we create the main factory. Before we start let’s have a look to the structure of the framework:

.. figure:: images/VisionBoxImagoSdk.png
   :alt: 

This shows a simplified class diagram of the underlying framework. ``VIB_NET`` is a platform independent API to access the hardware functionality. ``VIBSystem`` is a factory which creates all devices. One device class groups functions around a physical interface / unit, for example digital inputs or strobe unit. If the box has two independent channels, you can create two instances of each type. For example the AGE-X has two strobe channels. Finally the ``VIBSystem`` contains / handles a hardware dependent factory to build a concrete device implementation. It allows one program to run on different Imago PC based boxes that will be created in the future without changes or rebuilds.

.. figure:: images/VisionBoxOpenBaseboard.png
   :alt: 

In order to read the version string from the ImagoSystem node, use the GetProperty node (``Ctrl-P``) and connect the ImagoSystem node output to it.

Since opening the system usually is a one-time step, it can be done globally; either in the system globals of **nVision** or in the global settings of the pipeline.

Step 2 - Using simple devices
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An ImagoDevice node, which is not connected to an ImagoSystem node shows all possible devices, but they are all greyed out.

.. figure:: images/VisionBoxImagoDevice.png
   :alt: 

Once an ImagoDevice node is connected to the ImagoSystem node, the available devices are displayed prominently and can be easily selected.

.. figure:: images/VisionBoxAvailableBaseboardDevices.png
   :alt: 

The devices of a Baseboard system are different than those of a Network system.

.. figure:: images/VisionBoxAvailableNetworkDevices.png
   :alt: 

Since opening the devices usually is a one-time step, it can be done globally; either in the system globals of **nVision** or in the global settings of the pipeline.

Here is an example that shows how to set a digital output. The DIGITAL\_OUTPUT Operation node shows a number of methods that can be called on a digital output.

.. figure:: images/VisionBoxDigitalOutput.png
   :alt: 

The example shows how to set a single bit on a digital output.

Step 3 - Using a complex device
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A more complex device is the strobe out. This example cannot explain the strobe unit in detail. Please refer to the function reference in the Imago SDK documentation. To run the code successfully, please connect a zero Ω bridge at the first output.

Note: Every channel of the strobe unit must be calibrated (by default done by Imago), otherwise the initialization will fail.

To use the strobe, several operations must be executed in a row:

-  Initialize the strobe
-  Set the trigger mode
-  Set the desired current

.. figure:: images/VisionBoxStrobe.png
   :alt: 

Setting the trigger mode and current needs to be done after initializing the device. In order to establish this order, the Sync output of ``Init()`` is connected to the Sync input of ``set_TriggerMode()``, and the Sync output of ``set_TriggerMode()`` is connected to the Sync input of ``set_Current()``. Connecting the Sync ports in this way establishes an order of execution in a graph of operations, where otherwise the order of execution would be undefined.

Good to know
~~~~~~~~~~~~

-  Every Imago SDK function is thread safe.
-  The Imago SDK framework is process safe.
-  The device nodes show the available methods of a specific device. They also list property setters in the form of ``set_Xxxx()``. Property setters can be distinguished from methods, which are listed in the form ``XxxxxYyyyy()``.
-  Properties of the Imago nodes can be read with the GetProperty node (Ctrl-P).
-  Every function in the Imago SDK can be called with the graphical nodes.

