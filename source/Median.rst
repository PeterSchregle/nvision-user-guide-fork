Median
------

Perform a median filter.

.. figure:: images/MedianNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

KernelSizeX (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The kernel width.

KernelSizeY (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The kernel height.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies an optional area of interest.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

The median is a noise reduction filter, which often performs better than simple averaging, expecially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

Here are a few results of the median filter with increasing kernel sizes:

Original:

.. figure:: images/GaussianOriginal.png
   :alt: 

KernelSizeX = KernelSizeY = 3:

.. figure:: images/Median3x3.png
   :alt: 

KernelSizeX = KernelSizeY = 5:

.. figure:: images/Median5x5.png
   :alt: 

KernelSizeX = KernelSizeY = 7:

.. figure:: images/Median7x7.png
   :alt: 

KernelSizeX = KernelSizeY = 9:

.. figure:: images/Median9x9.png
   :alt: 

Sample
~~~~~~

Here is an example that shows how to use the median filter.

.. figure:: images/MedianSample.png
   :alt: 


