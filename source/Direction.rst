Direction
---------

Creates a direction.

.. figure:: images/DirectionNode.png
   :alt: 

A direction is a geometric entity, representing a direction in the two-dimensional cartesian plane. A direction is similar to an angle, but is internally implemented as a direction vector (unit vector).

.. figure:: images/direction.png
   :alt: 

Inputs
~~~~~~

Alpha (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

The angle in radians.

Outputs
~~~~~~~

Direction (Type: ``Direction``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The direction.

Sample
~~~~~~

Here is an example:

.. figure:: images/DirectionSample.png
   :alt: 


