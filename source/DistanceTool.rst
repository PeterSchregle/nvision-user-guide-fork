Distance Tool
-------------

.. figure:: images/geometry_distance.png
   :alt: 

The **Distance** tool measures the distance between edges along a line.

The tool displays graphical elements to specify the region of interest as well as to display the results.

It displays a yellow line that specifies the search line. You can move the line around on the image by dragging it. You can also pick either of the line endpoints to prolong the line or move just this endpoint (move the mouse a little bit away from the endpoint, until the cursor changes). If you move the mouse in the vicinity of the line, a little bit outside, you can drag the thickness of the line. The thickness affects the edge detection, because it specifies the area that is averaged.

Once the search line has been positioned over two edges in the image it displays the calculated distance, which is visualized graphically and its numerical value is displayed in green, if it is in between the tool's minimum and maximum expected values, or in red, if the measured distance is not within the expected boundary values.

The tool displays its measurement in pixels, or in world units, such as mm, if the system has been calibrated.

The distance tool has a configuration panel, which can be used to set parameters.

The **Min** and **Max** parameters are used to set the expected minimum and maximum distance.

The parameters in the **Edge Detection Parameters** affect the edge detection.

**Smoothing** is the amount of smoothing used in edge detection (default: 2.7).

**Strength** is the minimum strength of an edge (the gray scale slope of the edge, default = 15). Smoothing and Strength are interrelated: the more you smooth the more you lessen the strength of an edge and vice versa.
