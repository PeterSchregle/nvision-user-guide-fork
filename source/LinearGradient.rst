LinearGradient
--------------

Presets an image with a linear gradient pattern.

.. figure:: images/LinearGradientNode.png
   :alt: 

Inputs
~~~~~~

Start (Type: ``PointDouble``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The start point of the gradient. At this point the dark color is used. Between the start and stop points the colors are interpolated linearily between dark and bright.

Stop (Type: ``PointDouble``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The stop point of the gradient. At this point the bright color is used. Between the start and stop points the colors are interpolated linearily between dark and bright.

Type (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The image type. The following types can be chosen: ImageByte, ImageUInt16, ImageUInt32, ImageDouble, ImageRgbByte, ImageRgbUInt16, ImageRgbUInt32, ImageRgbDouble

Dark (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The value of a dark checkerboard field. This value is converted into the appropriate pixel type, depending on the Type parameter. For monochrome images you should enter one number, for rgb color images you should enter three numbers separated by a space.

Bright (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The value of a bright checkerboard field. This value is converted into the appropriate pixel type, depending on the Type parameter. For monochrome images you should enter one number, for rgb color images you should enter three numbers separated by a space.

Size (Type: ``Extent3d``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The size of the image that is created. The default value is 1024 x 1024 x 1 pixels.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

An optional region that constrains the preset operation to inside the region only. Pixels outside the region are colored with the Background color.

Background (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The preset value outside of the region. This value is converted into the appropriate pixel type, depending on the Type parameter. For monochrome images you should enter one number, for rgb color images you should enter three numbers separated by a space.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The image preset with a linear gradient pattern.

Comments
~~~~~~~~

The **LinearGradient** node presets an image with a linear gradient pattern. The size of the created image, the points and colors for dark and bright, the region of interest and the background color can all be specified.

Sample
~~~~~~

Here is an example that shows how to used the **LinearGradient** node.

.. figure:: images/LinearGradientSample.png
   :alt: 

Here is the image created with the sample:

.. figure:: images/LinearGradientSampleImage.png
   :alt: 


