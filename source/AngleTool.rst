Angle Tool
----------

.. figure:: images/geometry_angle.png
   :alt: 

The **Angle** tool measures the angle between two lines.

The tool displays graphical elements to specify the region of interest as well as to display the results.

It displays two boxes that are used to select regions, the boxes are labeled A and B. You can move the boxes around on the image by dragging their inside. You can also resize the boxes by picking them at their boundary lines (on the boundary line or just a bit outside) or at their corner points (on the corner point of just a bit outside). The arrows at the boxes visualize the search direction of the line detection (box A has horizontal arrows and should be used for vertical lines, box B has vertical arrows and should be used for horizontal lines).

Once the search box has been positioned over an edge in the image it displays the calculated edge points in green, and a resulting line in blue that it calculates by fitting a line through the points.

The tool has outlier detection. Outlier points are marked in red and they are not used for the line fit.

The search boxes move with the pose, that is their position is relative to the part position that has been determined by a location tool upstream.

If the tool finds both lines it calculates the angle between those lines. The angle is visualized graphically and its numerical value is displayed in green, if it is in between the tool's minimum and maximum expected values, or in red, if the measured angle is not within the expected boundary values (betwwen 89,9 ° and 90,1 °):

The angle tool has a configuration panel, which can be used to set parameters.

The **Min** and **Max** parameters are used to set the expected minimum and maximum angles.

The parameters in the **Edge Detection Parameters** affect the edge detection.

**Smoothing** is the amount of smoothing used in edge detection (default: 2.7). **Strength** is the minimum strength of an edge (the gray scale slope of the edge, default = 15). Smoothing and Strength are interrelated: the more you smooth the more you lessen the strength of an edge and vice versa.

**Polarity** can be used to select the type of edge: **Both**, **Rising** and **Falling** can be selected (default = Both). Rising means an edge going from dark to bright along the search direction, falling means an edge from bright to dark, and both means both edge types.

**Spacing** is the distance between the search lines of the edge detection (default = 5). The effect of the spacing can be seen by the density of the visualized edge points.
