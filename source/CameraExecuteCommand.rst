Execute Command
---------------

Executes a GenICam command of a camera.

.. figure:: images/ExecuteCommandNode.png
   :alt: 

Inputs
~~~~~~

Camera (Type: ``CameraInfo``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the camera.

Sync (Type: ``System.Object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This can be used to establish a temporal order, so that you can specify, what has to occur before the parameter will be written.

Name (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The name of the GenICam command. These names are determined by the camera vendor.

Outputs
~~~~~~~

Sync (Type: ``System.Object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This can be used to establish a temporal order, so that you can specify, what has to occur after the parameter has been written.

Comments
~~~~~~~~

The names of the available camera commands are the same as those in the camera parameters window that you can open with the **Show Camera Parameters** on the **Home** ribbon bar.

.. figure:: images/camera_parameters_window.png
   :alt: 


