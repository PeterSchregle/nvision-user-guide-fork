Convert Type
------------

Converts the type of an image.

.. figure:: images/ConvertImageTypeNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Type (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The type of the resulting image.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

This function takes an image and converts it to the specified type.

The following types are available: ``ImageByte``, ``ImageUInt16``, ``ImageUInt32``, ``ImageDouble``, ``ImageRgbByte``, ``ImageRgbUInt32``, ``ImageRgbUInt32`` and ``ImageRgbDouble``.

Usually, **nVision** works with variant image types and treats any specific image type transparently. Sometimes, however, one needs a specific type. This conversion helps to convert to a specific type.

Sample
~~~~~~

Here is an example:

.. figure:: images/ConvertImageSample.png
   :alt: 


