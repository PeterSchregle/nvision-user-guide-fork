Robust Fit Circle
-----------------

Fits a circle to a set of three or more points. The fit is a robust one, i.e. it detects outliers, which are not used in the fit. Besides the fitted circle, the node also returns the set of inliers (i.e. the points that have been used to fit the circle) as well as the outliers (i.e. the points that have been rejected and were not used to fit the circle).

.. figure:: images/RobustFitCircleNode.png
   :alt: 

Inputs
~~~~~~

Points (Type: ``PointList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of points.

Sigma (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

This is the standard deviation of the measurment error (default = 1.0). It affects the behaviour of the RANSAC algorithm.

Alpha (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

This is the probabilty that at least one of the random samples of points is free from outliers (default = 0.95). It affects the behaviour of the RANSAC algorithm.

Outputs
~~~~~~~

Circle (Type: 'Circle')
^^^^^^^^^^^^^^^^^^^^^^^

The fitted circle.

Inliers (Type: 'PointList')
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The points that are considered inliers, and are therefore used to fit the circle.

Outliers (Type: 'PointList')
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The points that are considered outliers, and do not take part in the circle fit.

Comments
~~~~~~~~

The **Robust Fit Circle** node fits a circle to a set of three or more points. It implements the circle fit by Taubin. In addition, it used the RANSAC algorithm to detect outliers. The circle is fitted to the inliers only.

See Also
~~~~~~~~

There is also the **Fit Circle** node, which does not use RANSAC and fits the circle to all points.

Sample
~~~~~~

Here is an **example** that detects several points in an image and fits a circle to the set of points. The **example** allows you to interactively explore the parameters of the **Robust Fit Circle** node.

You can click on the **hyperlink** to load the sample into **nVision**.
