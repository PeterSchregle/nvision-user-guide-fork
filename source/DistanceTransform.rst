Distance Transform
------------------

Calculates the distance transform given a region.

.. figure:: images/DistanceTransformNode.png
   :alt: 

Inputs
~~~~~~

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input region.

Size (Type: ``Extent3d``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The size of the resulting image.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The resulting image.

Comments
~~~~~~~~

Here is an example of a distance transform, given a rotated rectangular region on input:

.. figure:: images/DistanceTransform.png
   :alt: 


