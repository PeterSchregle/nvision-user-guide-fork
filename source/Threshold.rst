Threshold
---------

Binary thresholds an image at a threshold value and creates a region as a result.

.. figure:: images/ThresholdNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Constrains the function to a region of interest.

Comparison (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The thresholding operation: ``<``, ``<=``, ``>``, ``>=``, ``==`` or ``!=``.

Threshold (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The threshold value.

Outputs
~~~~~~~

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

A region that contains all the pixels of the input image that satisfy the thresholding operation.

Comments
~~~~~~~~

The region may or may not be spatially contiguous. If you want to separate the region into spatially distinct regions, use the **Connected Components** command.
