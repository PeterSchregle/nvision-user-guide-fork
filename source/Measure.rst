Measure
-------

The **Measure** tab groups commands for measurements of parts.

The **Edit** group contains commands for pipeline editing.

.. figure:: images/RibbonEdit.png
   :alt: 

The first command deletes a node in the linear pipeline. The second command commits the result of the selected node to the browser, where it can be stored into the filesystem. These two commands are replicated on most tabs of the ribbon.

The **Intensity** group contains commands for intensity based measurments.

.. figure:: images/RibbonIntensity.png
   :alt: 

The two tools calculate brightness and contrast within a region of interest. Brightness and contrast are often sufficent to check for the presence of something.

The **Geometry** group contains commands to make geometric measurements.

.. figure:: images/RibbonGeometry.png
   :alt: 

The commands can calculate distances and angles, can fit circles and measure areas. All measurements can then be classified to return true (ok) when they are within an expected range, or false (nok), when they are outside this range.

The **Count** group contains commands for edge and object counting.

.. figure:: images/RibbonCount.png
   :alt: 

The first tool counts edges crossing a line of interest. The next tool counts the number of contour points within a region of interest (which is high, if the region is structured and low if the region is unstructured). The last command counts the number of distinct objects.

The tools are meant to be chained one after the other, where one of the location tools is often the first tool in a chain of tools.
