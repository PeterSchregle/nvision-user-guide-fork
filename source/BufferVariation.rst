Variation
---------

Calculates the coefficient of variation of all pixels in a buffer.

.. figure:: images/BufferVariationNode.png
   :alt: 

Inputs
~~~~~~

Buffer (Type: ``Buffer``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input buffer.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

An optional region.

Outputs
~~~~~~~

Variation (Type: Object)
^^^^^^^^^^^^^^^^^^^^^^^^

The coefficient of variation.

Comments
~~~~~~~~

The **Variation** node calculates the coefficient of variation of all pixel values of a buffer.

If the *Region* input is connected, the calculation of the coefficient of variation is constrained to the pixels within the region only, otherwise the whole buffer is used.

If a color buffer is used, the coefficient of variation is calculated channel-wise.

The coefficient of variation is the standard deviation divided by the average.

The coefficient of variation is calculated according to this formula:

:math:`c_v =  \frac {\sigma}{\mu}`

Sample
~~~~~~

Here is an example that calculates the coefficient of variation of an image.

.. figure:: images/BufferVariationSample.png
   :alt: 


