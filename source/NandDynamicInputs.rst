Nand
----

Logical Nand of two or more boolean values.

.. figure:: images/NandNode.png
   :alt: 

Inputs
~~~~~~

A (Type: ``Boolean``)
^^^^^^^^^^^^^^^^^^^^^

The first operand.

B (Type: ``Boolean``)
^^^^^^^^^^^^^^^^^^^^^

The second operand.

C...Z (Type: ``Boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Additional operands, added on demand.

Outputs
~~~~~~~

Nand (Type: ``Boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^

The logical Nand of all operands.

Comments
~~~~~~~~

This node calculates the locgical Nand of multiple operands.

The ports for the inputs are added dynamically on demand. The node starts with the two inputs A and B. If both are connected, a third input named C is added. At a maximum, 26 inputs are possible. Inputs can also be deleted by removing the connection to them.
