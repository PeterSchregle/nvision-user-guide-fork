Set Parameter
-------------

Writes a GenICam parameter value to a camera.

.. figure:: images/GetParameterNode.png
   :alt: 

Inputs
~~~~~~

Camera (Type: ``CameraInfo``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the camera.

Sync (Type: ``System.Object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This can be used to establish a temporal order, so that you can specify, what has to occur before the parameter will be written.

Name (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The name of the GenICam parameter. These names are determined by the camera vendor.

Value (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^

The value to write to the camera parameter.

Outputs
~~~~~~~

Sync (Type: ``System.Object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This can be used to establish a temporal order, so that you can specify, what has to occur after the parameter has been written.

Comments
~~~~~~~~

The names of the available camera parameters can be found by opening the camera parameters window with the **Show Camera Parameters** on the **Home** ribbon bar.

.. figure:: images/camera_parameters_window.png
   :alt: 

Example
~~~~~~~

Here is an example that snaps one image, reads the current ExposureTime (20 ms), sets the ExposureTime to 10 ms, takes a second image, sets the ExposureTime back to its original value and takes a third image. The connections of the Sync outputs and inputs establish the temporal order.

.. figure:: images/camera_parameters.png
   :alt: 


