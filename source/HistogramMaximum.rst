Histogram Maximum
-----------------

Calculates the maximum value of a buffer, given a histogram.

.. figure:: images/HistogramMaximumNode.png
   :alt: 

Inputs
~~~~~~

Histogram (Type: ``Histogram``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The input histogram.

Outputs
~~~~~~~

Maximum (Type: ``Object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The maximum value.

Comments
~~~~~~~~

The **Histogram Maximum** node calculates the maximum value of a buffer, given a histogram to it.

If a color buffer is used, the maximum is calculated channel-wise.

Sample
~~~~~~

Here is an example that calculates the maximum of a histogram.

.. figure:: images/BufferMaximumSample.png
   :alt: 


