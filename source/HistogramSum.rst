Histogram Sum
-------------

Calculates the sum of all pixels in a buffer, given a histogram.

.. figure:: images/HistogramSumNode.png
   :alt: 

Inputs
~~~~~~

Histogram (Type: ``Histogram``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The input histogram.

Outputs
~~~~~~~

Sum (Type: Object)
^^^^^^^^^^^^^^^^^^

The sum.

Comments
~~~~~~~~

The **Histogram Sum** node calculates the sum of all pixel values of a buffer, given a histogram to it.

If a color buffer is used, the sum is calculated channel-wise.

Sample
~~~~~~

Here is an example that calculates the sum of a histogram.

.. figure:: images/BufferSumSample.png
   :alt: 


