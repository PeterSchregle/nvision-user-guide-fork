HMI Led
-------

An **Led** can be used to display a state.

.. figure:: images/HMILedNode.png
   :alt: 

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **Led**.

The **WebBrowser** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment* and *Margin* styles.

State (Type: ``boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The **Led** is one if this is ``True``, the **Led** is off otherwise.

Foreground (Type: ``SolidColorBrush``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The color of the **Led**.

Example
~~~~~~~

Here is an example that shows an **Led**. This definition:

.. figure:: images/hmi_led_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_led.png
   :alt: 


