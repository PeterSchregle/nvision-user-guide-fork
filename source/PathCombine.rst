PathCombine
-----------

Combines three strings into a path.

.. figure:: images/PathCombineNode.png
   :alt: 

Inputs
~~~~~~

PathString1 (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The first path to combine.

PathString2 (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The second path to combine.

PathString3 (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The third path to combine.

Outputs
~~~~~~~

CombinedPath (Type: ``Ngi.Path``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The combined path.

Comments
~~~~~~~~

PathString1 should be an absolute path (for example, "d:" or "\\archives"). If PathString2 or PathString3 is also an absolute path, the combine operation discards all previously combined paths and resets to that absolute path.

Empty strings are omitted from the combined path.
