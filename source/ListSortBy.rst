Sort By
-------

Sorts a list of items by some property.

.. figure:: images/ListSortByNode.png
   :alt: 

**Sort By** can be applied on objects which have a property that can be used for sorting. The sort order can be specified.

Inputs
~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The list of items.

Property (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

A property or a property path, such as ``X`` (``Point`` has an property named ``X``) or ``Size.X`` (``Image`` has a property named ``Size``, which in turn has a property named ``X``).

Direction (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The sort direction, use ``Ascending`` or ``Descending`` to sort in the respective direction.

Outputs
~~~~~~~

SortedList (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The sorted list.
