HMI ListBox (Multi Select)
--------------------------

A multi select **ListBox** can be used to select one from a number of entries.

.. figure:: images/HMIMultiSelectListBoxNode.png
   :alt: 

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **ListBox**.

The **ListBox** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding*, *Foreground*, *Background*, *Font* and *Padding* styles.

Items (Type: ``List<string>``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A list of text items that is displayed in the **ListBox**.

Selection (Type: ``List<string>``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The initial selection of the **ListBox**. This value is used only when the *Items* pin is connected.

SelectionMode (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Orientation of the **ListBox**, ``Extended`` or ``Multiple``.

Outputs
~~~~~~~

SelectedItems (Type: ``List<string>``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The selected items.

Example
~~~~~~~

Here is an example that shows a **ListBox**. This definition:

.. figure:: images/hmi_multiselect_listbox_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_multiselect_listbox.png
   :alt: 


