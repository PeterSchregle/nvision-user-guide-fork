Last
----

Takes the last item out of a list.

.. figure:: images/ListLastNode.png
   :alt: 

Inputs
~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The list.

Outputs
~~~~~~~

Item (Type: ``T``)
^^^^^^^^^^^^^^^^^^

The last item of the list.

Sample
~~~~~~

Here is an example:

.. figure:: images/ListItemSample.png
   :alt: 


