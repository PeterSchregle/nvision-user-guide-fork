Contrast Tool
-------------

.. figure:: images/features_contrast.png
   :alt: 

The **Contrast** tool measures the contrast in a region of interest.

The tool displays graphical elements to specify the region of interest as well as to display the results.

It displays a rectangle that is used to select a region. You can move the rectangle around on the image by dragging its inside. You can also resize the rectangle by picking it at its boundary lines (on the boundary line or just a bit outside). You can rotate the rectangle by picking it at its corner points (on the corner point of just a bit outside).

The numerical value is displayed in green, if it is in between the tool's minimum and maximum expected values, or in red if it is outside expected values.

The contrast tool has a configuration panel, which can be used to set parameters.

The **Min Contrast** parameter is used to set the minimal accepatable contrast (default = 30), the **Max Contrast** parameter is used to set the maximal accepatable contrast (default = 255).
