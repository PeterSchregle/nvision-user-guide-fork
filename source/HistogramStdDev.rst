Histogram StdDev
----------------

Calculates the standard deviation of all pixels in a buffer, given a histogram

.. figure:: images/HistogramStdDevNode.png
   :alt: 

Inputs
~~~~~~

Histogram (Type: ``Histogram``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The input histogram.

Outputs
~~~~~~~

StdDev (Type: Object)
^^^^^^^^^^^^^^^^^^^^^

The standard deviation.

Comments
~~~~~~~~

The **Histogram StdDev** node calculates the standard deviation of all pixel values of a buffer, given a historam to it.

If a color buffer is used, the standard deviation is calculated channel-wise.

The standard deviation is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The standard deviation is a measure of how much a statistical distribution is spread out. The standard deviation is the square root of the variance.

The standard deviation is calculated according to this formula:

:math:`\sigma =  \sqrt[]{ \frac {1}{n}  \sum x^2 -  \mu^2 }`

Sample
~~~~~~

Here is an example that calculates the standard deviation of a histogram.

.. figure:: images/BufferStdDevSample.png
   :alt: 


