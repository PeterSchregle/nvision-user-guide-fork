Point Algorithms
----------------

Point operations are the simplest image processing transformations. An output pixel value depends on the input pixel value only. Examples of such transformations are tone adjustments for brightness and contrast, thresholding, color space transformation, image compositing, image arithmetic or logic, etc.

Mathematically, a point operator can be written as

.. math:: h(x,y)=g(f(x,y))

or

.. math:: h = g \circ f

Classification of Point Operations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are various ways to classify point operations. One way to classify them is by the number of arguments. Unary point operations take one argument, binary point operations take two arguments and tertiary point arguments take three arguments. Another way is to separate them into arithmetic, logical, and other operations. The table shows this classification for the operations implemented within **nVision**.

\| \| Arithmetic \| Logic \| Other \| \|----------\|-----------------------------------------------------\|--------------\|---------------------------\| \| Unary \| Absolute, Decrement, Increment, Negate, Square Root \| Not \| Colorspace transformation \| \| Binary \| Add, Difference, Subtract \| And, Or, Xor \| \| \| Tertiary \|

Unary Image Operator
~~~~~~~~~~~~~~~~~~~~

Applies a unary point operation to an image. Unary point operations are applied pixel by pixel, the same way for every pixel.

.. figure:: images/UnaryImageOperatorNode.png
   :alt: 

The following operators are available:

.. figure:: images/UnaryImageOperator.png
   :alt: 

Absolute
^^^^^^^^

Absolute.

.. figure:: images/UnaryAbsolute.png
   :alt: 

abs ( Image ) -> Result

Decrement
^^^^^^^^^

Decrement by 1.

.. figure:: images/UnaryDecrement.png
   :alt: 

Image - 1 -> Result

Increment
^^^^^^^^^

Increment by 1.

.. figure:: images/UnaryIncrement.png
   :alt: 

Image + 1 -> Result

Negate
^^^^^^

Negate (2's complement).

.. figure:: images/UnaryNegate.png
   :alt: 

- Image -> Result

Not
^^^

Not (1's complement).

.. figure:: images/UnaryNot.png
   :alt: 

~ Image -> Result

Square Root
^^^^^^^^^^^

Square root.

.. figure:: images/UnarySquareRoot.png
   :alt: 

sqrt( Image ) -> Result

Inputs
^^^^^^

Image (Type: ``Image``)
'''''''''''''''''''''''

The input image.

Operator (Type: ``string``)
'''''''''''''''''''''''''''

Specifies the operator.

+------------+---------------------------+
| operator   | operation                 |
+============+===========================+
| ``++``     | increment                 |
+------------+---------------------------+
| ``--``     | decrement                 |
+------------+---------------------------+
| ``-``      | negate (2's complement)   |
+------------+---------------------------+
| ``!``      | not (1's complement)      |
+------------+---------------------------+
| ``abs``    | absolute                  |
+------------+---------------------------+
| ``sqrt``   | square root               |
+------------+---------------------------+

Outputs
^^^^^^^

Result (Type: ``Image``)
''''''''''''''''''''''''

The result image.

BinaryImageConstantOperator
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Applies a binary point operation between an image and a constant. Possible operations can be grouped into arithmetic, logic and comparison fields. Binary point operations are applied pixel by pixel, the same way for every pixel.

.. figure:: images/BinaryImageConstantOperatorNode.png
   :alt: 

Arithmetic Operators
^^^^^^^^^^^^^^^^^^^^

The following operators are available:

.. figure:: images/BinaryImageOperatorArithmetic.png
   :alt: 

Add
'''

Add with saturation.

.. figure:: images/BinaryConstantAdd.png
   :alt: 

Image + Constant -> Result

Subtract
''''''''

Subtract with saturation.

.. figure:: images/BinaryConstantSubtract.png
   :alt: 

Image - Constant -> Result

Difference
''''''''''

Difference with saturation.

.. figure:: images/BinaryConstantDifference.png
   :alt: 

abs ( Image - Constant ) -> Result

Multiply
''''''''

Multiply without saturation.

.. figure:: images/BinaryConstantMultiply.png
   :alt: 

Image \* Constant -> Result

Divide
''''''

Divide without saturation.

.. figure:: images/BinaryConstantDivide.png
   :alt: 

Image / Constant -> Result

Multiply (Blend)
''''''''''''''''

Multiply with saturation.

.. figure:: images/BinaryConstantMultiplyBlend.png
   :alt: 

Image \* Constant -> Result

Divide (Blend)
''''''''''''''

Divide with saturation.

.. figure:: images/BinaryConstantDivideBlend.png
   :alt: 

Image / Constant -> Result

Logic Operators
^^^^^^^^^^^^^^^

The following operators are available:

.. figure:: images/BinaryImageOperatorLogic.png
   :alt: 

And
'''

Logical And.

.. figure:: images/BinaryConstantAnd.png
   :alt: 

Image & Constant -> Result

Or
''

Logical Or.

.. figure:: images/BinaryConstantOr.png
   :alt: 

Image \| Constant -> Result

Xor
'''

Logical Xor.

.. figure:: images/BinaryConstantXor.png
   :alt: 

Image ^ Constant -> Result

Comparison Operators
^^^^^^^^^^^^^^^^^^^^

The following operators are available:

.. figure:: images/BinaryImageOperatorComparison.png
   :alt: 

Smaller
'''''''

Compare smaller.

.. figure:: images/BinaryConstantSmaller.png
   :alt: 

Image < Constant -> Result

Smaller or Equal
''''''''''''''''

Compare smaller or equal.

.. figure:: images/BinaryConstantSmallerOrEqual.png
   :alt: 

Image <= Constant -> Result

Equal
'''''

Compare equal.

.. figure:: images/BinaryConstantEqual.png
   :alt: 

Image == Constant -> Result

Bigger or Equal
'''''''''''''''

Compare bigger or equal.

.. figure:: images/BinaryConstantBiggerOrEqual.png
   :alt: 

Image >= Constant -> Result

Bigger
''''''

Compare bigger.

.. figure:: images/BinaryConstantBigger.png
   :alt: 

Image > Constant -> Result

Min/Max Operators
^^^^^^^^^^^^^^^^^

The following operators are available:

.. figure:: images/BinaryImageOperatorMinMax.png
   :alt: 

Min
'''

Minimum.

.. figure:: images/BinaryConstantMin.png
   :alt: 

min( Image, Constant) -> Result

Max
'''

Maximum.

.. figure:: images/BinaryConstantMax.png
   :alt: 

max( Image, Constant) -> Result

Inputs
^^^^^^

Image (Type: ``Image``)
'''''''''''''''''''''''

The input image.

Constant (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The constant.

Operator (Type: ``string``)
'''''''''''''''''''''''''''

Specifies the operator.

+---------------+--------------------+
| operator      | operation          |
+===============+====================+
| ``+``         | add                |
+---------------+--------------------+
| ``-``         | subtract           |
+---------------+--------------------+
| ``diff``      | difference         |
+---------------+--------------------+
| ``*``         | multiply           |
+---------------+--------------------+
| ``/``         | divide             |
+---------------+--------------------+
| ``*_blend``   | multiply (blend)   |
+---------------+--------------------+
| ``/_blend``   | divide (blend)     |
+---------------+--------------------+
| ``&``         | and                |
+---------------+--------------------+
| ``|``         | or                 |
+---------------+--------------------+
| ``^``         | xor                |
+---------------+--------------------+
| ``<``         | smaller            |
+---------------+--------------------+
| ``<=``        | smaller or equal   |
+---------------+--------------------+
| ``==``        | equal              |
+---------------+--------------------+
| ``>=``        | bigger or equal    |
+---------------+--------------------+
| ``>``         | bigger             |
+---------------+--------------------+
| ``min``       | minimum            |
+---------------+--------------------+
| ``max``       | maximum            |
+---------------+--------------------+

Outputs
^^^^^^^

Result (Type: ``Image``)
''''''''''''''''''''''''

The result image.

BinaryImageOperator
~~~~~~~~~~~~~~~~~~~

Applies a binary point operation between two images. Possible operations can be grouped into arithmetic, logic and comparison fields. Binary point operations are applied pixel by pixel, the same way for every pixel, for corresponding pixels of two images.

.. figure:: images/BinaryImageOperatorNode.png
   :alt: 

Arithmetic Operators
''''''''''''''''''''

The following operators are available:

.. figure:: images/BinaryImageOperatorArithmetic.png
   :alt: 

Add
'''

Add with saturation.

.. figure:: images/BinaryAdd.png
   :alt: 

Image + Gradient -> Result

Subtract
''''''''

Subtract with saturation.

.. figure:: images/BinarySubtract.png
   :alt: 

Image - Gradient -> Result

Difference
''''''''''

Difference with saturation.

.. figure:: images/BinaryDifference.png
   :alt: 

abs ( Image - Gradient ) -> Result

Multiply
''''''''

Multiply without saturation.

.. figure:: images/BinaryMultiply.png
   :alt: 

Image \* Gradient -> Result

Divide
''''''

Divide without saturation.

.. figure:: images/BinaryDivide.png
   :alt: 

Image / Gradient -> Result

Multiply (Blend)
''''''''''''''''

Multiply with saturation.

.. figure:: images/BinaryMultiplyBlend.png
   :alt: 

Image \* Gradient -> Result

Divide (Blend)
''''''''''''''

Divide with saturation.

.. figure:: images/BinaryDivideBlend.png
   :alt: 

Image / Gradient -> Result

Logic Operators
^^^^^^^^^^^^^^^

The following operators are available:

.. figure:: images/BinaryImageOperatorLogic.png
   :alt: 

And
'''

Logical And.

.. figure:: images/BinaryAnd.png
   :alt: 

Image & Gradient -> Result

Or
''

Logical Or.

.. figure:: images/BinaryOr.png
   :alt: 

Image \| Gradient -> Result

Xor
'''

Logical Xor.

.. figure:: images/BinaryXor.png
   :alt: 

Image ^ Gradient -> Result

Comparison Operators
^^^^^^^^^^^^^^^^^^^^

The following operators are available:

.. figure:: images/BinaryImageOperatorComparison.png
   :alt: 

Smaller
'''''''

Compare smaller.

.. figure:: images/BinarySmaller.png
   :alt: 

Image < Gradient -> Result

Smaller or Equal
''''''''''''''''

Compare smaller or equal.

.. figure:: images/BinarySmallerOrEqual.png
   :alt: 

Image <= Gradient -> Result

Equal
'''''

Compare equal.

.. figure:: images/BinaryEqual.png
   :alt: 

Image == Gradient -> Result

Bigger or Equal
'''''''''''''''

Compare bigger or equal.

.. figure:: images/BinaryBiggerOrEqual.png
   :alt: 

Image >= Gradient -> Result

Bigger
''''''

Compare bigger.

.. figure:: images/BinaryBigger.png
   :alt: 

Image > Gradient -> Result

Min/Max Operators
^^^^^^^^^^^^^^^^^

The following operators are available:

.. figure:: images/BinaryImageOperatorMinMax.png
   :alt: 

Min
'''

Minimum.

.. figure:: images/BinaryMin.png
   :alt: 

min( Image, Gradient) -> Result

Max
'''

Maximum.

.. figure:: images/BinaryMax.png
   :alt: 

max( Image, Gradient) -> Result

Inputs
^^^^^^

ImageA (Type: ``Image``)
''''''''''''''''''''''''

The first input image.

ImageB (Type: ``Image``)
''''''''''''''''''''''''

The second input image.

Operator (Type: ``string``)
'''''''''''''''''''''''''''

Specifies the operator.

+---------------+--------------------+
| operator      | operation          |
+===============+====================+
| ``+``         | add                |
+---------------+--------------------+
| ``-``         | subtract           |
+---------------+--------------------+
| ``diff``      | difference         |
+---------------+--------------------+
| ``*``         | multiply           |
+---------------+--------------------+
| ``/``         | divide             |
+---------------+--------------------+
| ``*_blend``   | multiply (blend)   |
+---------------+--------------------+
| ``/_blend``   | divide (blend)     |
+---------------+--------------------+
| ``&``         | and                |
+---------------+--------------------+
| ``|``         | or                 |
+---------------+--------------------+
| ``^``         | xor                |
+---------------+--------------------+
| ``<``         | smaller            |
+---------------+--------------------+
| ``<=``        | smaller or equal   |
+---------------+--------------------+
| ``==``        | equal              |
+---------------+--------------------+
| ``>=``        | bigger or equal    |
+---------------+--------------------+
| ``>``         | bigger             |
+---------------+--------------------+
| ``min``       | minimum            |
+---------------+--------------------+
| ``max``       | maximum            |
+---------------+--------------------+

Outputs
^^^^^^^

Result (Type: ``Image``)
''''''''''''''''''''''''

The result image.
