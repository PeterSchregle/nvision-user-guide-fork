Import Image
------------

Imports an image from a file.

.. figure:: images/ImportImageNode.png
   :alt: 

Inputs
~~~~~~

Filename (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

A filename in the filesystem.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The loaded image.

Comments
~~~~~~~~

The **ImportImage** node loads an image from the filesystem. The name of the file can either by typed, or selected with a File Open dialog - by clicking on the |image0| button. The node supports a variety of image file formats, such as TIF, PNG, JPG and BMP to name a few. If a file could not be loaded for some reason, an appropriate error message is shown.

Sample
~~~~~~

Here is an example that shows how to used the **ImportImage** node.

.. figure:: images/ImportExportSample.png
   :alt: 

.. |image0| image:: images/EllipsisButton.png
