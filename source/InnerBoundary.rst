Inner Boundary
--------------

Extracts the inner boundary from a list of regions.

.. figure:: images/InnerBoundaryNode.png
   :alt: 

The inner boundary of a region consists of the pixels of the region that touch the background. The inner boundary lies on the object.

Inputs
~~~~~~

Regions (Type: ``RegionList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of regions.

Outputs
~~~~~~~

Boundary (Type: ``RegionList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The objects reduced to their inner boundary.

Comments
^^^^^^^^

Here is an example of an inner boundary:

.. figure:: images/InnerBoundary.png
   :alt: 


