Horizontal Profile
------------------

Calculates a horizontal profile of an image to show the greyscale/color distribution.

.. figure:: images/HorizontalProfileNode.png
   :alt: 

The Horizontal Profile node gives you information about the greyscale/color distribution along a horizontal line. The line can have a certain thickness.

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Position (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The vertical coordinate of the horizontal line.

Thickness (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The thickness in pixel of the horizontal line.

Outputs
~~~~~~~

Profile (Type: ``Profile``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The profile.

Sample
~~~~~~

Here is an example:

.. figure:: images/ProfileSample.png
   :alt: 


