The **workspace** shows a view of the linear pipeline at the left and the result view at the right.

.. figure:: images/workspace.png
   :alt: 

The linear pipeline shows the linear flow of commands applied to the loaded object.

The result view shows one or more aspects of the result of the pipeline.

The nodes or steps in the linear pipeline are built as commands are entered from the ribbon. A new node is appended or inserted after the selected node.

.. figure:: images/delete.png
   :alt: 

Nodes are deleted with the **Delete Node** command.

.. figure:: images/commit_document.png
   :alt: 

Results of a node can be comitted (copied to the browser) with the **Commit Node Results** command.
