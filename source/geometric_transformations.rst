Geometric Transformations
-------------------------

This chapter explains geometric transformations, such as translation, rotation, etc. The availabe transformations are listed and their properties are explained.

The various translations can be ordered and structured. Starting with the identity transform everything is invariant. The translation transform leaves everything but the position invariant, the rotation transform leaves everything but the rotation invariant. Combining translation and rotation yields a rigid transform, which leaves everything but position and rotation invariant. Combining a rigid transform with an isotropic scaling transform yields a similarity transform, which does no longer preserve area and length. An affine transform no longer leaves angles invariant and a projective transform does not preserve parallelism.

.. figure:: images/geometric_transformations.png
   :alt: The hierarchy of the geometric transformations.

   The hierarchy of the geometric transformations.
The diagram shows the various transforms in a hierarchy and the little drawings hint about the transformations that are carried out. **nVision** implements functions to transform various geometric primitives with these transformations. In addition, transformations can be determined by relating sets of points (often called passpoints) that correspond in both world and image coordinates.

Translation
~~~~~~~~~~~

Translation moves geometry in the plane by a translation vector.

It has two degrees of freedom, namely the horizontal and vertical displacements (i.e. the components of the translation vector).

A point

.. math:: \begin{pmatrix} x \\ y \end{pmatrix}

in the two-dimensional plane is translated or shifted by adding offsets to it. This can be written as:

.. math:: x^{'}=x+t_x

.. math:: y^{'}=y+t_y

or in matrix form:

.. math:: \begin{pmatrix} x^{'} \\ y^{'} \end{pmatrix} = \begin{pmatrix} x \\ y \end{pmatrix} + \begin{pmatrix} t_x \\ t_y \end{pmatrix}

By using homogeneous coordinates, the equation can also be written like this:

.. math:: \begin{pmatrix}x^{'}\\y^{'}\\1\end{pmatrix} = \begin{pmatrix} 1 & 0 & t_x \\ 0 & 1 & t_y \\ 0 & 0 & 1 \end{pmatrix} * \begin{pmatrix} x \\ y \\ 1 \end{pmatrix}

We will use the notation with homogeneous coordinates from now on.

Rotation
~~~~~~~~

Rotation rotates geometry in the plane by some angle.

It has one degree of freedom, namely the rotation angle.

A point

.. math:: \begin{pmatrix} x \\ y \end{pmatrix}

in the two-dimensional plane is rotated around the origin by multiplying with a rotation matrix:

.. math:: \begin{pmatrix}x^{'}\\y^{'}\\1\end{pmatrix} = \begin{pmatrix} cos(\phi) & -sin(\phi) & 0 \\ sin(\phi) & cos(\phi) & 0 \\ 0 & 0 & 1 \end{pmatrix} * \begin{pmatrix} x \\ y \\ 1 \end{pmatrix}

Rigid
~~~~~

A rigid transformation combines translation and rotation.

It has three degrees of freedom, namely the horizontal and vertical displacements (i.e. the components of the translation vector) and the rotation angle.

A point

.. math:: \begin{pmatrix}x\\y\end{pmatrix}

in the two-dimensional plane is transformed by multiplying with a rigid matrix:

.. math:: \begin{pmatrix}x^{'}\\y^{'}\\1\end{pmatrix} = \begin{pmatrix} cos(\phi) & -sin(\phi) & t_x \\ sin(\phi) & cos(\phi) & t_y \\ 0 & 0 & 1 \end{pmatrix} * \begin{pmatrix} x \\ y \\ 1 \end{pmatrix}

Isotropic Scaling
~~~~~~~~~~~~~~~~~

An isotropic scaling scales geometry in the plane by some factor.

It has one degree of freedom, namely the scaling factor.

A point

.. math:: \begin{pmatrix}x\\y\end{pmatrix}

in the two-dimensional plane is transformed by multiplying with an isotropic scaling matrix:

.. math:: \begin{pmatrix}x^{'}\\y^{'}\\1\end{pmatrix} = \begin{pmatrix} s & 0 & 0 \\ 0 & s & 0 \\ 0 & 0 & 1 \end{pmatrix} * \begin{pmatrix} x \\ y \\ 1 \end{pmatrix}

Similarity
~~~~~~~~~~

A similarity transformation combines a rigid transformation with isotropic scaling.

It has four degrees of freedom, namely the horizontal and vertical displacements (i.e. the components of the translation vector), the rotation angle and the scaling factor.

A point

.. math:: \begin{pmatrix}x\\y\end{pmatrix}

in the two-dimensional plane is transformed by multiplying with a similarity matrix:

.. math:: \begin{pmatrix}x^{'}\\y^{'}\\1\end{pmatrix} = \begin{pmatrix} s*cos(\phi) & -s*sin(\phi) & t_x \\ s*sin(\phi) & s*cos(\phi) & t_y \\ 0 & 0 & 1 \end{pmatrix} * \begin{pmatrix} x \\ y \\ 1 \end{pmatrix}

Scaling
~~~~~~~

A scaling scales geometry in the plane by different factors in the horizontal and vertical direction.

It has two degrees of freedom, namely the horizontal and vertical scaling factors (i.e. the components of the scaling vector).

A point

.. math:: \begin{pmatrix}x\\y\end{pmatrix}

in the two-dimensional plane is transformed by multiplying with a scaling matrix:

.. math:: \begin{pmatrix}x^{'}\\y^{'}\\1\end{pmatrix} = \begin{pmatrix} s_x & 0 & 0 \\ 0 & s_y & 0 \\ 0 & 0 & 1 \end{pmatrix} * \begin{pmatrix} x \\ y \\ 1 \end{pmatrix}

Affine
~~~~~~

An affine transformation has six degrees of freedom.

A point

.. math:: \begin{pmatrix}x\\y\end{pmatrix}

in the two-dimensional plane is transformed by multiplying with an affine matrix:

.. math:: \begin{pmatrix}x^{'}\\y^{'}\\1\end{pmatrix} = \begin{pmatrix} m_11 & m_12 & m_13 \\ m_21 & m_22 & m_23 \\ 0 & 0 & 1 \end{pmatrix} * \begin{pmatrix} x \\ y \\ 1 \end{pmatrix}

Projective
~~~~~~~~~~

A projective transformation has eight degrees of freedom.

A point

.. math:: \begin{pmatrix}x\\y\end{pmatrix}

in the two-dimensional plane is transformed by multiplying with a projective matrix:

.. math:: \begin{pmatrix}x^{'}\\y^{'}\\1\end{pmatrix} = \begin{pmatrix} m_11 & m_12 & m_13 \\ m_21 & m_22 & m_23 \\ m_31 & m_32 & 1 \end{pmatrix} * \begin{pmatrix} x \\ y \\ 1 \end{pmatrix}


