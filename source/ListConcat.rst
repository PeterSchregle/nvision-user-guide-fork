Concatenate
-----------

Concatenates two lists of the same type.

.. figure:: images/ConcatNode.png
   :alt: 

Inputs
~~~~~~

ListA (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The first input list.

ListB (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The second input list.

Outputs
~~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The output list, which consists of the items of the first input list followed by the items of the second input list.
