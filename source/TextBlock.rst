HMI TextBlock
-------------

A **TextBlock** can be used to display text.

.. figure:: images/HMITextBlockNode.png
   :alt: 

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **TextBlock**.

The **TextBlock** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding*, *Foreground*, *Background*, *Font* and *Padding* styles.

Text (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^

The text.

Foreground (Type: ``SolidColorBrush``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The foreground.

Background (Type: ``SolidColorBrush``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The background.

Font (Type: ``Font``)
^^^^^^^^^^^^^^^^^^^^^

The font.

Example
~~~~~~~

Here is an example that shows a **TextBlock**. This definition:

.. figure:: images/hmi_textblock_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_textblock_ui.png
   :alt: 


