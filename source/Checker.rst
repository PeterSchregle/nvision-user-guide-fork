Checker
-------

Presets an image with a checkerboard pattern.

.. figure:: images/CheckerNode.png
   :alt: 

Inputs
~~~~~~

CheckerCellSize (Type: ``VectorInt32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The cell size of one checkerboard field. The default is 64 x 64 pixels.

Type (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The image type. The following types can be chosen: ImageByte, ImageUInt16, ImageUInt32, ImageDouble, ImageRgbByte, ImageRgbUInt16, ImageRgbUInt32, ImageRgbDouble

Dark (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The value of a dark checkerboard field. This value is converted into the appropriate pixel type, depending on the Type parameter. For monochrome images you should enter one number, for rgb color images you should enter three numbers separated by a space.

Bright (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The value of a bright checkerboard field. This value is converted into the appropriate pixel type, depending on the Type parameter. For monochrome images you should enter one number, for rgb color images you should enter three numbers separated by a space.

Size (Type: ``Extent3d``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The size of the image that is created. The default value is 1024 x 1024 x 1 pixels.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

An optional region that constrains the preset operation to inside the region only. Pixels outside the region are colored with the Background color.

Background (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The preset value outside of the region. This value is converted into the appropriate pixel type, depending on the Type parameter. For monochrome images you should enter one number, for rgb color images you should enter three numbers separated by a space.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The image preset with a checkerboard pattern.

Comments
~~~~~~~~

The **Checker** node presets an image with a checkerboard pattern. The size of the created image, the colors for dark and bright checkerboard fields, the region of interest and the background color can all be specified.

Sample
~~~~~~

Here is an example that shows how to used the **Checker** node.

.. figure:: images/CheckerSample.png
   :alt: 

Here is the image created with the sample:

.. figure:: images/CheckerSampleImage.png
   :alt: 


