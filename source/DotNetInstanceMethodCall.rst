.NET Instance Method Call
-------------------------

Call an instance method in a .Net assembly.

.. figure:: images/DotNetInstanceMethodCallNode.png
   :alt: 

Inputs
~~~~~~

The number and types of the inputs are determined when the assembly file name, the type name and the method name have been specified. They are determined by reflection and are the parameters of the method..

The first input is the instance of the object, the other inputs are the parameters of the method.

Outputs
~~~~~~~

The output type is determined when the assembly file name, the type name and the method name have been specified. It is determined by reflection and is the return value of the method (if not void).

Comments
~~~~~~~~

The parameters that specify which method to call are only editable when the ports of the node are not connected.

The first parameter is the filename of the assembly that contains the static method. You can use the browse button to browse to the desired assembly. An example of an assembly that you can try is: ``C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETCore\v4.5.1\mscorlib.dll``.

The second parameter is the class that contains the method, prefixed by its namespace, i.e. ``System.String``.

Once all parameters have been specified, and it is clear which method to call, the title of the node will adapt and show a more suitable name.

The third parameter is the instance method to call, i.e. ``Contains(String value)``.

Here is an example that calls the method ``Contains(String value)`` from the ``System.String`` class in the ``mscorlib.dll`` assembly.

.. figure:: images/CallStringContains.png
   :alt: 

The example checks if the string ``World`` is contained in the string ``Hello World!``.
