HistogramVariation
------------------

Calculates the coefficient of variation of all pixels in a buffer, given a histogram.

.. figure:: images/HistogramVariationNode.png
   :alt: 

Inputs
~~~~~~

Histogram (Type: ``Histogram``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The input histogram.

Outputs
~~~~~~~

Variation (Type: Object)
^^^^^^^^^^^^^^^^^^^^^^^^

The coefficient of variation.

Comments
~~~~~~~~

The **HistogramVariation** node calculates the coefficient of variation of all pixel values of a buffer, given a histogram

If a color buffer is used, the coefficient of variation is calculated channel-wise.

The coefficient of variation is the standard deviation divided by the average.

The coefficient of variation is calculated according to this formula:

:math:`c_v =  \frac {\sigma}{\mu}`

Sample
~~~~~~

Here is an example that calculates the coefficient of variation of a histogram:

.. figure:: images/BufferVariationSample.png
   :alt: 


