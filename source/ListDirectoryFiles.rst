List Files
----------

Lists the available files in a directory.

.. figure:: images/ListDirectoryFilesNode.png
   :alt: 

Inputs
~~~~~~

Directory (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The directory (provided as a string, or selected via the ... button. This can be a relative or absolute path to the directory to search. This string is not case-sensitive.

Filter (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The filter string to match against the names of the files in the directory. This parameter can contain a combination of valid literal path and wildcard characters (\* to match zero or more characters and ? to match zero or one characters).

Subdirectories (Type: ``Boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If this parameter is set to false, the files in the specified directory only are returned. If this parameter is set to true, the files in the specified directory as well as in all its subdirectories are returned.

Outputs
~~~~~~~

Filenames (Type: ``List<String>``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of files.
