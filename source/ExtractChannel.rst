Extract Channel
---------------

Extracts a specific color channel from a multi-channel image.

.. figure:: images/ExtractChannelNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Channel (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The channel that should be extracted.

Outputs
~~~~~~~

Channel (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

This function extracts a specific channel from a multi-channel color image.

The channel can be specified by meaning or by position.

Positional arguments are: ``First``, ``Second`` and ``Third``. The other arguments are ``Red``, ``Green``, ``Blue``, ``Hue``, ``Lightness``, ``Saturation``, ``Intensity``, ``L``, ``A`` and ``B``. If a channel is not valid (such as when trying to extract the red channel from an HSI image), an error is shown.

Here is an example of the original color image:

.. figure:: images/rgbimg.png
   :alt: 

And here are the three color channels red, green and blue from left to right:

.. figure:: images/rgb_channels.png
   :alt: 

The same color channels again with proper coloring:

.. figure:: images/rgb_channels_color.png
   :alt: 

Sample
~~~~~~

Here is an example that extracts the three color channels from an RGB image.

.. figure:: images/ExtractChannelSample.png
   :alt: 


