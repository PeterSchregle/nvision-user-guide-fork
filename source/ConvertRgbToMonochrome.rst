RGB -> Monochrome
-----------------

Converts an image in the RGB color model to monochrome.

.. figure:: images/ConvertRgbToMonochromeNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Outputs
~~~~~~~

Monochrome (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

This function converts an image in the RGB color model to a monochrome image.

Here is an example of the original color image:

.. figure:: images/rgbimg.png
   :alt: 

And here is the same image converted to monochrome:

.. figure:: images/rgb_mono.png
   :alt: 

Sample
~~~~~~

Here is an example that converts a color image to monochrome.

.. figure:: images/ConvertRgbToMonochromeSample.png
   :alt: 


