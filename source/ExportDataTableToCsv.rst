ExportDataTableToCsv
--------------------

Exports the results of a blob analysis to a file with comma-separated values.

.. figure:: images/ExportDataTableToCsvNode.png
   :alt: 

Inputs
~~~~~~

Data (Type: 'System.Data.DataTable')
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The blob analysis data.

Directory (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A directory in the filesystem. This is the directory, where the saved files will be placed. If the directory does not exist, an error message will be shown.

Filename (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

A filename. You can either enter a filename (with extension) or leave this empty. If you leave it empty, the system will create a name in the form of "data\_00000.csv", where the number will be incremented automatically. If you enter a filename, the file will be overwritten in every execution of the pipeline node.

ValueSeparator (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This separates single values in one line of the output file. This must be a single character.

QuoteCharacter (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This character is used to quote values. This must be a single character.

Culture (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

Cultural information used to format values.

Comments
~~~~~~~~

The **ExportDataTableToCsv** node saves an csv file to the filesystem. The directory where the file will be saved can either by typed, or selected with a Sewarch Folder dialog - by clicking on the |image0| button. If a file could not be saved for some reason, an appropriate error message is shown.

The **ExportDataTableToCsv** node saves data only in two cases:

1. | The export mode is on (the **Export on/off** button is pressed).
   | |image1|
   |  If the **Export on/off** button is pressed, any time the pipeline runs the file will be saved.

2. | The **Export once** button will be clicked.
   | |image2|
   | If the **Export once** button is clicked, the file will be saved.

.. |image0| image:: images/EllipsisButton.png
.. |image1| image:: images/ExportOnOffButton.png
.. |image2| image:: images/ExportOnceButton.png
