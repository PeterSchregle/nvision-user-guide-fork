Rescale2D
---------

Rescales an image.

.. figure:: images/Rescale2dNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

XFactor (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The horizontal scaling factor.

YFactor (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The vertical scaling factor.

Filter (Type: ``String``)
'''''''''''''''''''''''''

The geometric interpolation. Available values are ``NearestNeighbor``, ``Box``, ``Triangle``, ``Cubic``, ``Bspline``, ``Sinc`` , ``Lanczos`` and ``Kaiser``. The accuracy of the interpolation increases from ``NearestNeighbor`` to ``Kaiser``, but the performance decreases. The default is set to ``Box``.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image. The size of the output image is determined by the width and height of the input image multiplied by the horizontal and vertical scaling factors.
