Camera Infos
------------

Lists information about the available cameras.

.. figure:: images/CameraInfosNode.png
   :alt: 

The preview shows the number of available cameras.

Outputs
~~~~~~~

CameraInfos (Type: ``List<CameraInfo>``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A list of **CameraInfo** objects. You can use **GetItem** to take a specific item from the list and **GetProperty** to read a specific piece of information, such as the *Model* or the *Vendor*.

Example
~~~~~~~

Here is an example that lists some information about cameras:

.. figure:: images/list_cameras.png
   :alt: 


