Group
-----

You can use the **Group** command from the pipeline menu to move the selected nodes and their inside connections to a newly created subpipeline. The outside connections are automatically turned into input and output ports of the subpipeline.

You can select nodes by clicking them with the left mouse button. Hold down the ``Ctrl`` key if you want to add to (or subtract from) the selection.

You can also select a set of nodes inside a rectangle that you drag while holding down the ``Ctrl`` key.

.. figure:: images/RubberBandSelect.png
   :alt: 

If the selected nodes have vertical widget or HMI connections to the outside, the command has no effect.
