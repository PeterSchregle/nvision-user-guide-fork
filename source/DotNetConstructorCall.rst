.NET Constructor Call
---------------------

Call a constructor in a .Net assembly.

.. figure:: images/DotNetConstructorCallNode.png
   :alt: 

Inputs
~~~~~~

The number and types of the inputs are determined when the assembly file name, the type name and the constructor name have been specified. They are determined by reflection and are the parameters of the constructor.

Outputs
~~~~~~~

The output type is determined when the assembly file name, the type name and the constructor name have been specified. It is determined by reflection and is of the selected type.

Comments
~~~~~~~~

The parameters that specify which constructor to call are only editable when the ports of the node are not connected.

The first parameter is the filename of the assembly that contains the static method. You can use the browse button to browse to the desired assembly. An example of an assembly that you can try is: ``C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETCore\v4.5.1\mscorlib.dll``.

The second parameter is the class that contains the constructor, prefixed by its namespace, i.e. ``System.Version``.

The third parameter is the constructor to call, i.e. ``GetEnvironmentVariable``.

Once all parameters have been specified, and it is clear which constructor to call, the title of the node will adapt and show a more suitable name.

Here is an example that calls the constructor ``Version(Int32 major, Int32 minor)`` from the ``System.Version`` class in the ``mscorlib.dll`` assembly.

.. figure:: images/CallConstructVersion.png
   :alt: 

The example reads the value of the ``windir`` environment value and returns the string ``C:\Windows``.
