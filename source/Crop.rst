Crop
----

Crops a an arbitrary region out of an image.

.. figure:: images/CropNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The region.

Outputs
~~~~~~~

Cropped (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The portion of the original image that is specified by the crop region.

Comments
~~~~~~~~

This function cuts out an arbitraty portion of an image. Often this is used to constrain a subsequent processing operation to a region of interest.

Sample
~~~~~~

Here is an example, where the region is specified interactively with a circle widget:

.. figure:: images/CropSample.png
   :alt: 


