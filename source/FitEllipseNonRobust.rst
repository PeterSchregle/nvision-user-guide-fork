Fit Ellipse
-----------

Fits an ellipse to a set of five or more points.

.. figure:: images/FitEllipseNode.png
   :alt: 

Inputs
~~~~~~

Points (Type: ``PointList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of points.

Outputs
~~~~~~~

Ellipse (Type: 'Ellipse')
^^^^^^^^^^^^^^^^^^^^^^^^^

The fitted ellipse.

Comments
~~~~~~~~

The **Robust Fit Ellipse** node fits an ellipse to a set of three or more points. It implements the ellipse fit by Fitzgibbon. In addition, it used the RANSAC algorithm to detect outliers. The ellipse is fitted to the inliers only.

See Also
~~~~~~~~

There is also the **Robust Fit Ellipse** node, which uses RANSAC and rejects outliers.

Sample
~~~~~~

Here is an **example** that detects several points in an image and fits a ellipse to the set of points. The **example** allows you to interactively explore the parameters of the **Fit Ellipse** node.

You can click on the **hyperlink** to load the sample into **nVision**.
