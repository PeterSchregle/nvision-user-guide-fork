Histogram Quantile
------------------

Calculates the quantile of a buffer, given a histogram.

.. figure:: images/HistogramQuantileNode.png
   :alt: 

Inputs
~~~~~~

Histogram (Type: ``Histogram``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The input histogram.

Percentage (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the quantile.

Outputs
~~~~~~~

Quantile (Type: ``Object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The quantile value.

Comments
~~~~~~~~

The **Histogram Quantile** node calculates a quantile of a buffer, given a histogram to it.

If a color buffer is used, the quantile is calculated channel-wise.

Sample
~~~~~~

Here is an example that calculates the 50% quantile of a histogram.

.. figure:: images/BufferQuantileSample.png
   :alt: 


