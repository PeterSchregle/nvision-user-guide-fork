Hipass
------

Filters an image using a highpass kernel of size 3x3 or 5x5.

.. figure:: images/HipassNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

KernelSize (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The kernel size (3 -> 3x3, 5 -> 5x5).

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies an optional area of interest.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values::

::

    -1 -1 -1
    -1  8 -1
    -1 -1 -1

or a 5x5 matrix with the following values:

::

    -1 -1 -1 -1 -1
    -1 -1 -1 -1 -1
    -1 -1 24 -1 -1
    -1 -1 -1 -1 -1
    -1 -1 -1 -1 -1

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

Here are a few results of the hipass filter with increasing kernel sizes:

Original:

.. figure:: images/GaussianOriginal.png
   :alt: 

KernelSize = 3:

.. figure:: images/Hipass3x3.png
   :alt: 

KernelSize = 5:

.. figure:: images/Hipass5x5.png
   :alt: 

Sample
~~~~~~

Here is an example that shows how to use the hipass filter.

.. figure:: images/HipassSample.png
   :alt: 


