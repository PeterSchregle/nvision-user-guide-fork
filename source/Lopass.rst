Lowpass
-------

Perform a lowpass filter.

.. figure:: images/LopassNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

KernelSizeX (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The kernel width.

KernelSizeY (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The kernel height.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies an optional area of interest.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

Here are a few results of the lowpass filter with increasing kernel sizes:

Original:

.. figure:: images/GaussianOriginal.png
   :alt: 

KernelSizeX = KernelSizeY = 3:

.. figure:: images/Lowpass3x3.png
   :alt: 

KernelSizeX = KernelSizeY = 5:

.. figure:: images/Lowpass5x5.png
   :alt: 

KernelSizeX = KernelSizeY = 7:

.. figure:: images/Lowpass7x7.png
   :alt: 

KernelSizeX = KernelSizeY = 9:

.. figure:: images/Lowpass9x9.png
   :alt: 

Sample
~~~~~~

Here is an example that shows how to use the median filter.

.. figure:: images/LowpassSample.png
   :alt: 


