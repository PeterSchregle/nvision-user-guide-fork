Write Symbol
------------

Writes symbol data to a Beckhoff PLC.

.. figure:: images/BeckhoffWriteSymbolNode.png
   :alt: 

This node writes digital signals to an OpenLayers module.

Inputs
~~~~~~

PLC (Type: ``TwinCAT.Ads.AdsClient``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A Beckhoff PLC (ADS) instance.

Symbol (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^

A PLC symbol name.

Value (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^

The value that is written to the Beckhoff PLC symbol.

Sync (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^

This input can be connected to any other object. It is used to establish an order of execution.

Sometimes, if this is necessary for technical reasons, you can add a **Delay** node in order to introduce additional delay between synchronized nodes.

Outputs
~~~~~~~

Sync (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^

Synchronizing object. It is used to establish an order of execution.
