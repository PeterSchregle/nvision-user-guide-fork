Distance Transform Constrained
------------------------------

Calculates the constrained distance transform given a region.

.. figure:: images/DistanceTransformConstrainedNode.png
   :alt: 

Inputs
~~~~~~

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input region.

Size (Type: ``Extent3d``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The size of the resulting image.

MaxDistance (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The maximum distance.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The resulting image.

Comments
~~~~~~~~

Here is an example of a constrained distance transform, given a rotated rectangular region on input:

.. figure:: images/DistanceTransformConstrained.png
   :alt: 

The resulting values of the transform are constrained to the maximum distance input.
