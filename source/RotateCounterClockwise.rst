Rotate Counter Clockwise
------------------------

Rotates an image counter-clockwise by 90 degrees.

.. figure:: images/RotateCounterClockwise.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

This function rotates an image by 90 degrees in counter-clockwise direction.

.. figure:: images/RotateCounterClockwiseImage.png
   :alt: 

Sample
~~~~~~

Here is an example that rotates an image.

.. figure:: images/RotateCounterClockwiseSample.png
   :alt: 


