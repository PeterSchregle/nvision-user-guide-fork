Start Acquisition
-----------------

Starts the acquisition of a camera.

.. figure:: images/StartAcquisitionNode.png
   :alt: 

Inputs
~~~~~~

Camera (Type: ``CameraInfo``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the camera.

Sync (Type: ``System.Object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This can be used to establish a temporal order, so that you can specify, what has to occur before the parameter will be written.

Outputs
~~~~~~~

Sync (Type: ``System.Object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This can be used to establish a temporal order, so that you can specify, what has to occur after the parameter has been written.

Comments
~~~~~~~~

The **Start Acquisition** node starts the acquisition on the specified camera. Once the acquition is running, an **Image Source** node will deliver images from the camera. The **Stop Acquisition** node stops the acquisition on the specified camera.
