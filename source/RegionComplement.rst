RegionComplement
----------------

Calculates the complement of a region.

.. figure:: images/RegionComplementNode.png
   :alt: 

Calculates the complement (yellow) of a region (dark).

.. figure:: images/region_complement.png
   :alt: 

A -> Complement

Inputs
~~~~~~

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input region.

Outputs
~~~~~~~

Result (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The result region.
