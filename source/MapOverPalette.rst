MapOverPalette
--------------

Maps an image over a palette.

.. figure:: images/MapOverPaletteNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Palette (Type: ``Palette``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The mapping palette.

Outputs
~~~~~~~

Mapped (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

For every pixel in the input image, the corresponding color entry in the palette is read and written to the output image.
