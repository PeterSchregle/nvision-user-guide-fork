Count Controur Points Tool
--------------------------

.. figure:: images/features_count_contour_points.png
   :alt: 

The **Count Contour Points** tool counts the number of high-contrasting pixels within a region of interest.

The tool displays graphical elements to specify the region of interest as well as to display the results.

It displays a rectangle that is used to select a region. You can move the rectangle around on the image by dragging its inside. You can also resize the rectangle by picking it at its boundary lines (on the boundary line or just a bit outside). You can rotate the rectangle by picking it at its corner points (on the corner point of just a bit outside).

One use of the tool is to check for riffle or embossing on metal parts.

The numerical value is displayed in green, if it is in between the tool's minimum and maximum expected values, or red , if the number of contour points is not within the expected boundary values.

The count contour points tool has a configuration panel, which can be used to set parameters.

The **Min** parameter is used to set the minimal accepatable area, the **Max** parameter is used to set the maximal accepatable area.

The **Sensitivity** can be set to a value between 0 (highly sensitive) and 255 (not sensitive).
