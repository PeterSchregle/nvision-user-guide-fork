|image0| MySql Connection
-------------------------

Establishes a connection to a MySql database.

.. figure:: images/MySqlConnectionNode.png
   :alt: 

Inputs
~~~~~~

Server (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The name or network address of the instance of MySql to which to connect. Examples are ``localhost``, ``127.0.0.1``, etc.

Database (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The case-sensitive name of the database to use.

User (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The MySql login account being used.

Password (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The password for the MySql account being used.

Outputs
~~~~~~~

Connection (Type: ``MySql.Data.MySqlClient.MySqlConnection``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The database connection.

Comments
~~~~~~~~

Establishing a database connection is the first step in communicating with a database. All other queries need the connection. Therefore, establishing the database connection has a somewhat global nature, and in many cases it can be put into the system globals pipeline or in the globals pipeline. If the connection is created in such a way it will be available from everywhere in your pipeline.

See the `MySql documentation <https://dev.mysql.com/doc/>`__ for more information about how to establish connections to MySql databases.

.. |image0| image:: images/db_mysql.png
