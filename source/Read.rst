Read
----

Read data from an interface.

.. figure:: images/ReadNode.png
   :alt: 

This node reads data from an interface (such as a serial port).

Inputs
~~~~~~

Io (Type: ``IoResource``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The IO interface.

BytesToRead (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The number of bytes to read from the interface.

Sync (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^

This input can be connected to any other object. It is used to establish an order of execution.

Outputs
~~~~~~~

Data (Type: ``DataList``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The data that has been read in the form of a DataList (a list of bytes).

Comments
~~~~~~~~

The **Sync** input and the **Data** output can be used to establish an order of execution.

Sometimes, if this is necessary for technical reasons, you can add a **Delay** node in order to introduce additional delay between synchronized nodes.
