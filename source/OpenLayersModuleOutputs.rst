OpenLayers DigOut
-----------------

Writes digital electrical outputs to an OpenLayers module from Data Translation.

.. figure:: images/OpenLayersDigOutNode.png
   :alt: 

This node writes digital signals to an OpenLayers module.

Inputs
~~~~~~

Module (Type: ``OpenLayersModule``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An OpenLayers module instance.

DigOut (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^

The numeric value that is written to the electrical outputs.

Sync (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^

This input can be connected to any other object. It is used to establish an order of execution.

.. figure:: images/OpenLayersDigOutSync.png
   :alt: 

Here, the digital output is written **after** the digital input has been read.

Sometimes, if this is necessary for technical reasons, you can add a **Delay** node in order to introduce additional delay between synchronized nodes.

Outputs
~~~~~~~

Sync (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^

Synchronizing object. It is used to establish an order of execution.
