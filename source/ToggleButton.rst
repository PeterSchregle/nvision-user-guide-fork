HMI ToggleButton
----------------

Buttons are used to switch or visualize state, as well as to trigger actions. The **ToggleButton** has two states, up or down.

.. figure:: images/HMIToggleButtonNode.png
   :alt: 

A **ToggleButton** toggles between the up and down states when it is clicked.

At the bottom of the **ToggleButton** node is a pin that allows it to connect one child.

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **ToggleButton**.

The **ToggleButton** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding*, *Foreground*, *Background*, *Font* and *Padding* styles.

Name (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^

The text that is displayed within the button. This text is only displayed, when no child is connected. If a child is connected, the visual definition of the child is used.

Outputs
~~~~~~~

IsChecked (Type: ``boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``True`` if the button is down, ``False`` otherwise.

Example
~~~~~~~

Here is an example that shows a simple toggle button. This definition:

.. figure:: images/toggle_button_def.png
   :alt: 

creates the following user interface:

.. figure:: images/toggle_button.png
   :alt: 


