Max Position
------------

Calculates the position of the maximum value of a buffer.

.. figure:: images/BufferMaxPositionNode.png
   :alt: 

Inputs
~~~~~~

Buffer (Type: ``Buffer``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input buffer.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

An optional region.

Outputs
~~~~~~~

MaxPosition (Type: ``Index3d``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The position of the maximum value.

Comments
~~~~~~~~

The **Max Position** node calculates the (first occurrence of the) position of the maximum value of a buffer.

If the *Region* input is connected, the calculation of the maximum is constrained to the pixels within the region only, otherwise the whole buffer is used.

If a color buffer is used, the maximum length color vector is calculated.
