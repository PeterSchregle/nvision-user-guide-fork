Fill Holes
----------

Fills holes in a list of objects.

.. figure:: images/FillHolesNode.png
   :alt: 

Inputs
~~~~~~

Regions (Type: ``RegionList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of regions.

Outputs
~~~~~~~

Regions (Type: ``RegionList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The resulting list of regions, which have their holes filled.

Comments
~~~~~~~~

Here is an example of a list of regions:

.. figure:: images/FillHoles0.png
   :alt: 

and here is the same list of regions with their holes filled:

.. figure:: images/FillHoles1.png
   :alt: 


