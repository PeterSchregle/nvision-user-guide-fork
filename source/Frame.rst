Frame
-----

Puts a frame around an image.

.. figure:: images/Frame.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Size (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^

The frame size.

Framing (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The framing method (Nearest, Reflective (Middle), Reflective (Border), Periodic).

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

The **Frame** node puts a border around the input image.

The border can be put around in various ways. *Nearest* takes the nearest pixel value to the image border and repeats it.

.. figure:: images/FrameNearest.png
   :alt: 

*Reflective (Middle)* reflects the image at the middle of the border pixel.

.. figure:: images/FrameReflectiveMiddle.png
   :alt: 

*Reflective (Border)* reflects the image at the border of the border pixel.

.. figure:: images/FrameReflectiveBorder.png
   :alt: 

*Periodic* periodically repeats the image.

.. figure:: images/FramePeriodic.png
   :alt: 

Sample
~~~~~~

Here is an example that puts a symmetric frame around an image.

.. figure:: images/FrameSample.png
   :alt: 


