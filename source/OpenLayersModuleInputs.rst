OpenLayers DigIn
----------------

Reads digital electrical inputs from an OpenLayers Module from Data Translation.

.. figure:: images/OpenLayersDigInNode.png
   :alt: 

This node reads digital signals from an OpenLayers module.

If the clock button inside the node is pressed |image0|, the node polls the module regularly. The frequency of the polling is controlled with the timer interval setting on the Pipeline tab on the ribbon.

.. figure:: images/timer_tab.png
   :alt: 

If the clock button inside the node is released |image1|, the module is not polled, and the node executes during regular pipeline updates only.

Inputs
~~~~~~

Module (Type: ``OpenLayersModule``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An OpenLayers module instance.

Sync (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^

This input can be connected to any other object. It is used to establish an order of execution.

.. figure:: images/OpenLayersDigInSync.png
   :alt: 

Here, the digital input is read **after** the digital output has been written.

Sometimes, if this is necessary for technical reasons, you can add a **Delay** node in order to introduce additional delay between synchronized nodes.

Outputs
~~~~~~~

DigIn (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The digital input in the form of a 32 bit integer number.

.. |image0| image:: images/clock_pressed.png
.. |image1| image:: images/clock_released.png
