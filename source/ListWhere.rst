Filter (Where)
--------------

**Filter (Where)** takes those items from a list where a a predicate is true. The predicate is specified itself as a graphical pipeline. The predicate is evaluated for each item of the list, beginning with the first item. Items where the predicate evaluates to true are added to the output list.

.. figure:: images/FilterWhereNode.png
   :alt: 

Outside View
~~~~~~~~~~~~

At the outside, the **Filter (Where)** node appears like a single node, with inputs and outputs.

Inputs
^^^^^^

List (Type: ``List<T>``)
''''''''''''''''''''''''

The input list.

Outputs
^^^^^^^

FilteredList (Type: ``List<T>``)
''''''''''''''''''''''''''''''''

The output list, consisting of a part of the input list, as determined by consecutive execution of the predicate.

Inside View
~~~~~~~~~~~

Inside, the **Filter (Where)** node looks like a normal branched pipeline. This pipeline is used to specify the predicate, which is evaluated for each item.

.. figure:: images/FilterWhereNodeInside.png
   :alt: 

Inputs
^^^^^^

Item (Type: ``T``)
''''''''''''''''''

One item of the input list.

ItemIndex (Type: ``Int32``)
'''''''''''''''''''''''''''

The index of the item of the input list.

Outputs
^^^^^^^

Result (Type: ``Boolean``)
''''''''''''''''''''''''''

The boolean predicate.

Comments
~~~~~~~~

**Filter (Where)** nodes can be nested in similar ways as subpipelines.

The nesting is shown with a breadcrumbs control at the top.

.. figure:: images/breadcrumbs.png
   :alt: 

Double-click a **Filter (Where)** node to enter it, and use the breadcrumbs control to exit it and go up one or more levels.

By default, a **Filter (Where)** node has one input and one output.

You can add additional inputs with the **Add Input (Ctrl-I)** command. You can edit the port names by clicking on them and typing a new name. The type of the ports is determined by the first connection made to them, either from the outside or from the inside.

Sample
~~~~~~

Here is an example:

.. figure:: images/FilterWhereSample.png
   :alt: 

.. figure:: images/FilterWhereSample2.png
   :alt: 

It takes items from the first list (0, 1, ..., 9) where the predicate (is even) is true and returns the resulting list (0, 2, ..., 8).
