Result Tool
-----------

.. figure:: images/tool_result.png
   :alt: 

The **Result** tool is used to communicate the result of an inspection task. It works with an Adam module from Advantec.

The result tool writes the result in the form of an electrical signal to the Adam module.

The result tool has a configuration panel, which can be used to set parameters.

The parameters in **Configure IO** specify the electrical lines that are switched on.

**OK** specifies the number of the output of the Adam module which is used to signal the **OK** state.

**Not OK** specifies the number of the output of the Adam moduel which is used to signal the **not OK** state.
