Locate Contour Tool
-------------------

.. figure:: images/locate_shape.png
   :alt: 

The locate contour tool locates a part using geometric contour matching.

The tool displays graphical elements to specify the region of interest as well as to display the results.

The tool displays a yellow region of interest that you can drag around to specify the pattern. You can move the box around on the image by dragging its inside. You can also resize the box by picking it at its boundary lines (on the boundary line or just a bit outside) or at its corner points (on the corner point of just a bit outside).

Make sure that you select a characteristic portion when you define the pattern. Once the box has been positioned, make sure that the filename in the **Template File** edit control is correct and press **Teach**. This saves the pattern.

If the tool finds the pattern, it displays a green box where it found the pattern and also moves the coordinate axes to the middle of the box to visualize the new origin and rotation (the pose).

Subsequent tools use the pose to position their ROIs, such that the ROIs are always in the correct position, even if the part moves considerably.

The locate shape tool has a configuration panel, which can be used to set parameters.

The **Template File** parameter specifies the match template, which is loaded from disk. If the template file exists, it is displayed here.

The **Min Score** parameter is used to set the minimum score for an acceptable match (default = 0.6). Matches below this score are not considered.

The **Teach** button saves the pattern to the disk.
