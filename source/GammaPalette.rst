GammaPalette
------------

Creates a palette with a gamma transfer function.

.. figure:: images/GammaPaletteNode.png
   :alt: 

Inputs
~~~~~~

Type (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The type of the palette. Choices are ``PaletteByte``, ``PaletteUInt16``, ``PaletteUInt32``, ``PaletteDouble``, ``PaletteRgbByte``, ``PaletteRgbUInt16``, ``PaletteRgbUInt32`` and ``PaletteRgbDouble``.

Width (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The width of the palette.

Gamma (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

The gamma parameter.

Outputs
~~~~~~~

Palette (Type: ``Palette``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The output palette.

Comments
~~~~~~~~

This function creates a palette with a gamma function.

A gamma value below 1.0 compresses darks and expands brights. Here is an example of a gamma palette with a gamma of 0.5:

.. figure:: images/gamma_05_palette.png
   :alt: 

A gamma value of exactly 1.0 creates a linear transfer function:

.. figure:: images/gamma_1_palette.png
   :alt: 

A gamma value above 1.0 expands darks and compresses brights. Here is an example of a gamma palette with a gamma of 2.0:

.. figure:: images/gamma_2_palette.png
   :alt: 


