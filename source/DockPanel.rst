HMI DockPanel
-------------

The **DockPanel** docks its child controls to one of the four sides *Left*, *Top*, *Right* or *Bottom*, where the respective child takes at much space as it needs.

.. figure:: images/HMIDockPanelNode.png
   :alt: 

At the bottom of the **DockPanel** node is a pin that allows it to connect several children.

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **DockPanel**.

The **DockPanel** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin* and *Background* styles.

LastChildFill (Type: ``boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If true, the last child added to the **DockPanel** takes all the available space. If false, the last child added takes only the space it needs.

Background (Type: ``SolidColorBrushByte``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The background of the **DockPanel**.

Comments
~~~~~~~~

Children of the **DockPanel** have the attached property **Dock**. This property can take the values *Left*, *Top*, *Right* and *Bottom* and specifies to which side the child will be docked.

Example
~~~~~~~

Here is an example that docks a text label to the top and a webbrowser to the left, where the webbrowser fills the available space fully. This definition

.. figure:: images/hmi_dock_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_dock.png
   :alt: 


