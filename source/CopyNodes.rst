Copy
----

You can use ``Ctrl-C`` or the **Copy (Ctrl-C)** command from the pipeline menu to copy the selected nodes and their inside connections to the clipboard.

You can select nodes by clicking them with the left mouse button. Hold down the ``Ctrl`` key if you want to add to (or subtract from) the selection.

You can also select a set of nodes inside a rectangle that you drag while holding down the ``Ctrl`` key.

.. figure:: images/RubberBandSelect.png
   :alt: 

Once you have copied a set of nodes to the clipboard, you can paste them somewhere else.
