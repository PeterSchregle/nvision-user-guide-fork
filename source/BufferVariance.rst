Variance
--------

Calculates the variance of all pixels in a buffer.

.. figure:: images/BufferVarianceNode.png
   :alt: 

Inputs
~~~~~~

Buffer (Type: ``Buffer``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input buffer.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

An optional region.

Outputs
~~~~~~~

Variance (Type: Object)
^^^^^^^^^^^^^^^^^^^^^^^

The variance.

Comments
~~~~~~~~

The **Variance** node calculates the variance of all pixel values of a buffer.

If the *Region* input is connected, the calculation of the variance is constrained to the pixels within the region only, otherwise the whole buffer is used.

If a color buffer is used, the variance is calculated channel-wise.

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The variance is calculated according to this formula:

:math:`\sigma^2 = \frac {1}{n}  \sum x^2 -  \mu^2`

Sample
~~~~~~

Here is an example that calculates the variance of an image.

.. figure:: images/BufferVarianceSample.png
   :alt: 


