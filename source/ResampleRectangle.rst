ResampleRectangle
-----------------

Resamples a rectangular portion of an image.

.. figure:: images/ResampleRectangleNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Rectangle (Type: ``Ngi.Rectangle``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The rectangle. A rectangle can be oriented at any angle.

Filter (Type: ``String``)
'''''''''''''''''''''''''

The geometric interpolation. Available values are ``NearestNeighbor``, ``Box``, ``Triangle``, ``Cubic``, ``Bspline``, ``Sinc`` , ``Lanczos`` and ``Kaiser``. The accuracy of the interpolation increases from ``NearestNeighbor`` to ``Kaiser``, but the performance decreases. The default is set to ``Box``.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image (has the same size as the rectangle, but rounded to an integer).
