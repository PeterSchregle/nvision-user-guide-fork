Beckhoff PLC
------------

Establish a connection to a Beckhoff TwinCAT PLC, which allows you to read and write to PLC symbols.

.. figure:: images/BeckhoffPlcNode.png
   :alt: 

This node initializes the PLC. If you connect its output to a **GetProperty** node, you can gather information about the PLC. Usually you would connect the output to a **Read Symbol** node to read symbol data, or a **Write Symbol** node to write symbol data.

Inputs
~~~~~~

AMS NetID (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The identifier of the Beckhoff PLC.

Port (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^

The port of the Beckhoff PLC.

Outputs
~~~~~~~

Module (Type: ``TwinCAT.Ads.AdsClient``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Beckhoff PLC (ADS) instance.

Since establishing a connection to a PLC is often an operation with a somewhat global character, the **Beckhoff PLC** node is sometimes put into the pipeline globals or the system globals.
