Vertical Shift Tool
-------------------

.. figure:: images/locate_vertical_shift.png
   :alt: 

The **Vertical Shift** tool uses horizontal edge detection to find a horizontal location.

The tool displays a region of interest that is used to detect the shift. The region of interest can be moved by the user.

The **Vertical Shift** tool has a configuration panel, which can be used to set parameters.

The parameters in the **Edge Detection Parameters** affect the edge detection.

**Orientation** can be used to set the direction of the edge detection: **Top to Bottom** or **Bottom to Top** are available.

**Polarity** can be used to select the type of edge: **Both**, **Rising** and **Falling** can be selected (default = Both). Rising means an edge going from dark to bright along the search direction, falling means an edge from bright to dark, and both means both edge types.

**Smoothing** is the amount of smoothing used in edge detection (default: 2.7).

**Strength** is the minimum strength of an edge (the gray scale slope of the edge, default = 15). Smoothing and Strength are interrelated: the more you smooth the more you lessen the strength of an edge and vice versa.

The graphic below the controls shows the image that corresponds to the region of interest. The filled blue curve is the averaged brightness and the yellow curve is the gradient, both after the smoothing is applied. You can observe how the shape of the curves is affected by changing the **Smoothing**.

The two vertical lines visualize the effect of the edge strength. An edge is only found if the yellow curve crosses the lines. You can observe how the edge detection is affected by changing the **Strength**.

The horizontal line shows where in the curve and image the edge is detected.
