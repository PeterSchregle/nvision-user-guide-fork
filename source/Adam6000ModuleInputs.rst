Adam 6000 DigIn
---------------

Reads digital electrical inputs from an Adam-60xx Ethernet Module from Adavantech.

.. figure:: images/Adam6000DigInNode.png
   :alt: 

This node reads digital signals from an Adam-60xx module.

If the clock button inside the node is pressed |image0|, the node polls the module regularly. The frequency of the polling is controlled with the timer interval setting on the Pipeline tab on the ribbon.

.. figure:: images/timer_tab.png
   :alt: 

If the clock button inside the node is released |image1|, the module is not polled, and the node executes during regular pipeline updates only.

Inputs
~~~~~~

Module (Type: ``Adam6000Module``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An Adam 60xx module instance.

Sync (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^

This input can be connected to any other object. It is used to establish an order of execution.

.. figure:: images/AdamDigInSync.png
   :alt: 

Here, the digital input is read **after** the image has been imported.

Sometimes, if this is necessary for technical reasons, you can add a **Delay** node in order to introduce additional delay between synchronized nodes.

Outputs
~~~~~~~

DigIn (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The digital input in the form of a 32 bit integer number.

Sample
~~~~~~

Here is an example:

.. figure:: images/Adam6000ModuleSample.png
   :alt: 

.. |image0| image:: images/clock_pressed.png
.. |image1| image:: images/clock_released.png
