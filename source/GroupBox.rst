HMI GroupBox
------------

The **GroupBox** puts a group box around its child.

.. figure:: images/HMIGroupBoxNode.png
   :alt: 

At the bottom of the **GroupBox** node is a pin that allows it to connect one child.

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **GroupBox**.

The **GroupBox** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding* and *Background*, *Foreground* and *Font* styles.

Header (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The header text is displayed at the top left corner or the **GroupBox**.

Example
~~~~~~~

Here is an example that puts a groupbox around three radio buttons. This definition:

.. figure:: images/hmi_groupbox_def.png
   :alt: 

creates the following user interface:

.. figure:: images/hmi_groupbox.png
   :alt: 


