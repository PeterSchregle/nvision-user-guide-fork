Calibrate Tool
--------------

.. figure:: images/calibrate.png
   :alt: 

The **Calibrate** tool is used to establish a camera calibration. The calibration can correct a perspective distortion and still measure proper distances in the calibrated plane.

The tool detects the positions of dots on a calibration target and uses those to detect the perspective distortion. In addition to the detected dot positions the tool needs to know the distance between two dots in world units as well as the world unit.

.. figure:: images/calibration_target_sample.png
   :alt: 

Only the dots within the yellow region of interest turn blue and are used for calibration.

The **Calibrate** tool has a configuration panel where you can enter the distance between two dots as well as the world units.

Once you have a proper image of the calibration target, eventually aligned the region of interest properly and entered the distance between the dots as well as the unit, press the **Teach** button.
