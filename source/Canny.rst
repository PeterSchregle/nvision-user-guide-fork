Canny
-----

Applies Canny edge detection to an image.

.. figure:: images/CannyNode.png
   :alt: 

According to Canny's edge detection algorithm, the filter performs Gaussian smoothing, edge detection with a Sobel filter and then uses hysteresis thresholding and non maximum suppression to end up with a binary imnage of the edges.

You can follow this node with a Threshold node to convert the binary image to a region.

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Sigma (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

Determines the size of the Gaussian filter kernel.

SobelSize (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Determines the size of the Sobel filter kernel.

Low (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^

The low threshold of the hysteresis thresholding expressed as a percentage.

High (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^

The hiegh threshold of the hysteresis thresholding expressed as a percentage.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image of the edges.

Sample
~~~~~~

Here is an example:

.. figure:: images/CannySample.png
   :alt: 


