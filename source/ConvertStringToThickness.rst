Text -> Thickness
-----------------

Converts text to a Thickness.

Inputs
~~~~~~

Text (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^

The text that is converted to a Thickness.

Outputs
~~~~~~~

Thickness (Type: ``Thickness``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The output Thickness.

Comments
~~~~~~~~

A Thickness encodes values of four sides, for *Left*, *Top*, *Right* and *Bottom*.

You can specify one number, which is then used for all four sides, i.e. ``50``.

Alternatively, you can specify two numbers separated with a semicolon, where the first is used for *Left* and *Right*, and the second for *Top* and *Bottom*, which creates a symmetric thickness, i.e. ``5; 10``.

Alternatively, you can specify four numbers separated with semicolons, that are used for *Left*, *Top*, *Right* and *Bottom*, i.e. ``5, 10, 20, 30``.
