GetEnvironmentVariable
----------------------

Retrieves the value of an environment variable from the current process.

.. figure:: images/GetEnvironmentVariableNode.png
   :alt: 

Inputs
~~~~~~

Variable (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The name of the environment variable.

Outputs
~~~~~~~

Value (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^

The value of the environment variable specified by variable, or null if the environment variable is not found.
