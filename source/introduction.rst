Introduction
============

**nVision** is a computer vision development system. The main use is to create automated machine vision applications that are used in the industry. It uses a graphical approach to programming, which leads to much shorter development time, compared to traditional programming. In addition, the learning curve is shallow, which allows you to be immediately productive.

Since **nVision** has valuable and powerful tools for image inspection, it has proven to be usable in the scientific laboratory as well. It is the tool of choice in the lab, when a Photoshop-based approach is too laborious and when you need fast results. The graphical programming helps in the lab as well to quickly create the needed results in an automated way.

**nVision** helps you with a variety of vision based tasks needed in the industry:

-  Check the position of a part to guide a handling systems or to put a vision tool in the right position.
-  Identify a part based on marks, shape or other visual aspects.
-  Verify a part to check if it has been built or assembled correctly.
-  Measure a parts dimensions.
-  Inspect a part for defects.

.. figure:: images/nVision.png
   :alt: The nVision window.

   The nVision window.
 

Make sure to read the **nVision Quick Start Guide**. It contains the most important information on a single page.

\ |image0|\ 

Program Editions
----------------

**nVision** has two main components: the **nVision Designer** is the rapid application development system and the **nVision Runtime** is the runtime part. The **nVision Designer** provides a graphical user interface and a graphical programming system. The **nVision Runtime** provides the computer vision functionality in a modular way and a facility to execute the graphical programs. It also has the means to display a HMI (human-machine interface).

**nVision Designer** is what you use to create applications. When these applications are to be deployed to the machine on the factory floor, usually only the more cost-effective and modular **nVision Runtime** is used.

nVision Designer
~~~~~~~~~~~~~~~~

**nVision Designer** comes with complete feature sets targeted to the intended audience.

+---------------------------+--------------------------------------------+
| Product                   | Description                                |
+===========================+============================================+
| **nVision Designer MV**   | Targeted for machine vision development.   |
+---------------------------+--------------------------------------------+
| **nVision Designer L**    | Targeted for laboratory use.               |
+---------------------------+--------------------------------------------+
| **nVision Designer U**    | The ultimate version has everything.       |
+---------------------------+--------------------------------------------+

nVision Runtime
~~~~~~~~~~~~~~~

The following runtime modules are available.

+-----------------------------+-------------------------------------------------------------------------------------------------------+
| Module                      | Description                                                                                           |
+=============================+=======================================================================================================+
| **Image Processing**        | Contains basic image processing functionality, both monochrome and color, including camera support.   |
+-----------------------------+-------------------------------------------------------------------------------------------------------+
| **Analysis, Measurement**   | Contains image analysis functionality, blob analysis, camera calibration, gauging measurement.        |
+-----------------------------+-------------------------------------------------------------------------------------------------------+
| **Template Matching**       | Contains the matching and searching functionality, template matching, geometric pattern matching.     |
+-----------------------------+-------------------------------------------------------------------------------------------------------+
| **Barcode, Matrixcode**     | Contains the barcode reading functionality, both linear and two dimensional symbologies.              |
+-----------------------------+-------------------------------------------------------------------------------------------------------+
| **OCR, OCV**                | Contains the Optical Character Recognition and Optical Character Verification functionality.          |
+-----------------------------+-------------------------------------------------------------------------------------------------------+

Module Matrix
~~~~~~~~~~~~~

The table shows which modules are included in which designer edition.

+-----------------------------+------+------+------+
| Module                      | MV   | L    | U    |
+=============================+======+======+======+
| **Image Processing**        | \*   | \*   | \*   |
+-----------------------------+------+------+------+
| **Analysis, Measurement**   | \*   | \*   | \*   |
+-----------------------------+------+------+------+
| **Template Matching**       | \*   |      | \*   |
+-----------------------------+------+------+------+
| **Barcode, Matrixcode**     | \*   |      | \*   |
+-----------------------------+------+------+------+
| **OCR, OCV**                | \*   |      | \*   |
+-----------------------------+------+------+------+

Requirements
------------

**nVision** (Designer and Runtime) runs on Windows 7 or higher, 64 bit (recommended) or 32 bit (memory limitations apply). It supports many cameras of different vendors, either via the GigE Vision, USB3 Vision, and GenICam standards or directly via the manufacturers SDK.

Licensing
---------

**nVision** (Designer and Runtime) are licensed to the specific hardware. In order to run **nVision**, you need a license key.

When **nVision** is started for the first time, it needs to be activated. In addition to the license key, activation needs a hardware key, which is determined automatically. The activation process uses the he hardware key, together with the license key and creates an activation key. It is the activation key - in combination with the two other keys - that actually unlocks the **nVision** software.

Usually, activation works through an internet connection and is simple and transparent. If you copy your license key to the clipboard before you start **nVision** for the very first time, you will hardly notice the process. Otherwise, a dialog such as the following will be displayed:

.. figure:: images/nVisionLicensing1.png
   :alt: The **nVision** licensing dialog.

   The **nVision** licensing dialog.
 

In order to continue, you should enter your license key and press the Return key. **nVision** will contact the licensing server and use the returned activation key to unlock. If the license server cannot be connected for some reason, you can phone (+49 8245 7749600) or write an email to info@impuls-imaging.com to tell us both your license key and the hardware key, and we will provide you with an activation key that you can enter manually.

Activation is perpetual for a specific hardware. If the hardware changes, this is considered a new hardware, which means that you have to activate **nVision** again. While **nVision** is licensed to one machine only, one license key permits three activations to cover the case of defective hardware.

Camera and Device Interfaces
----------------------------

The primary interfaces to cameras used in **nVision** are the **GigEVision** and the **USB3 Vision** interfaces. These interfaces are standardized. Image data can be accessed via **GenTL** and camera properties can be read and written via **GenICam**.

For cameras interfaced via **GenTL** and **GenICam**, the full set of properties is accessible from within **nVision**.

The camera properties are accessible in the form of a properties window.

.. figure:: images/camera_properties.png
   :alt: The camera properties window.

   The camera properties window.
 

In addition, the camera properties can be read and written from within a graphical pipeline, i.e. the properties can be changed from within a program.

.. figure:: images/camera_properties_pipeline.png
   :alt: Writing camera properties from within a pipeline.

   Writing camera properties from within a pipeline.
 

Secondary interfaces to cameras and devices exist via manufacturer SDKs for historical reasons. USB2 cameras are interfaced this way, since they were available before standardization, as well as interfaces to frame grabbers.

The following table lists tested devices:

+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Manufacturer         |             | URL                                                               | Device                  | Interface                           |
+======================+=============+===================================================================+=========================+=====================================+
| Allied Vision        | |image20|   | `www.alliedvision.com <http://www.alliedvision.com/>`__           | GigE Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Basler               | |image21|   | `www.baslerweb.com <http://www.baslerweb.com/>`__                 | GigE Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Baumer               | |image22|   | `www.baumer.com <http://www.baumer.com/>`__                       | GigE / USB3 Cameras     | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Flir                 | |image23|   | `www.flir.com <http://www.flir.com/>`__                           | GigE Cameras (A315)     | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| IDS                  | |image24|   | `www.ids-imaging.com <http://www.ids-imaging.com/>`__             | USB Cameras             | Direct (Vendor SDK)                 |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| IDS                  | |image25|   | `www.ids-imaging.com <http://www.ids-imaging.com/>`__             | GigE Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| JAI                  | |image26|   | `www.jai.com <http://www.jai.com/>`__                             | GigE / USB3 Cameras     | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Leutron              | |image27|   | `www.leutron.com <http://www.leutron.com/>`__                     | USB Cameras             | Direct (Vendor SDK)                 |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Gardasoft            | |image28|   | `www.gardasoft.com <http://www.gardasoft.com/>`__                 | GigE Flash Controller   | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Matrix Vision        | |image29|   | `www.matrix-vision.com <http://www.matrix-vision.com/>`__         | Framegrabber            | Direct (Vendor SDK)                 |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Matrix Vision        | |image30|   | `www.matrix-vision.com <http://www.matrix-vision.com/>`__         | USB Cameras             | Direct (Vendor SDK)                 |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Matrix Vision        | |image31|   | `www.matrix-vision.com <http://www.matrix-vision.com/>`__         | GigE /USB3 Cameras      | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Net GmbH             | |image32|   | `www.net-gmbh.com <http://www.net-gmbh.com/>`__                   | USB Cameras             | Direct (Vendor SDK)                 |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Point Grey           | |image33|   | `www.ptgrey.com <http://www.ptgrey.com/>`__                       | GigE / USB3 Cameras     | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Sentech              |             | `www.sentecheurope.com <http://www.sentecheurope.com/>`__         | GigE / USB3 Cameras     | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Smartek              | |image34|   | `www.smartek.vision <http://www.smartek.vision/>`__               | GigE Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| SVS Vistek           | |image35|   | `www.svs-vistek.com <http://www.svs-vistek.com/>`__               | GigE Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| The Imaging Source   | |image36|   | `www.theimagingsource.com <http://www.theimagingsource.com/>`__   | GigE Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Various              | |image37|   |                                                                   | Webcams                 | Direct (Windows Media Foundation)   |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Ximea                | |image38|   | `www.ximea.com <http://www.ximea.com/>`__                         | USB3 Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+

All other **GigE Vision** or **USB3 Vision** capable cameras should work with **nVision** right our of the box, but have not been tested in-house so far.

.. |image0| image:: images/nVisionQuickStartGuide.png
.. |image1| image:: images/allied_vision_logo.png
.. |image2| image:: images/basler_logo.png
.. |image3| image:: images/baumer_logo.png
.. |image4| image:: images/flir_logo.png
.. |image5| image:: images/ids_logo.png
.. |image6| image:: images/ids_logo.png
.. |image7| image:: images/jai_logo.png
.. |image8| image:: images/leutron_logo.png
.. |image9| image:: images/gardasoft_logo.png
.. |image10| image:: images/mv_logo.png
.. |image11| image:: images/mv_logo.png
.. |image12| image:: images/mv_logo.png
.. |image13| image:: images/net_logo.png
.. |image14| image:: images/point_grey_logo.png
.. |image15| image:: images/smartek_logo.png
.. |image16| image:: images/svs_vistek_logo.png
.. |image17| image:: images/the_imaging_source_logo.png
.. |image18| image:: images/windows_media_foundation_logo.png
.. |image19| image:: images/ximea_logo.png
.. |image20| image:: images/allied_vision_logo.png
.. |image21| image:: images/basler_logo.png
.. |image22| image:: images/baumer_logo.png
.. |image23| image:: images/flir_logo.png
.. |image24| image:: images/ids_logo.png
.. |image25| image:: images/ids_logo.png
.. |image26| image:: images/jai_logo.png
.. |image27| image:: images/leutron_logo.png
.. |image28| image:: images/gardasoft_logo.png
.. |image29| image:: images/mv_logo.png
.. |image30| image:: images/mv_logo.png
.. |image31| image:: images/mv_logo.png
.. |image32| image:: images/net_logo.png
.. |image33| image:: images/point_grey_logo.png
.. |image34| image:: images/smartek_logo.png
.. |image35| image:: images/svs_vistek_logo.png
.. |image36| image:: images/the_imaging_source_logo.png
.. |image37| image:: images/windows_media_foundation_logo.png
.. |image38| image:: images/ximea_logo.png
