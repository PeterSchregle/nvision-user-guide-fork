Path
----

Make a path given a string.

.. figure:: images/PathNode.png
   :alt: 

Inputs
~~~~~~

String (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^

A string that should be converted to a path.

Outputs
~~~~~~~

Path (Type: ``Ngi.Path``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The path.
