Binning
-------

Reduces an image in size by combining several neighboring pixels.

.. figure:: images/BinningNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

XFactor (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The horizontal binning factor.

YFactor (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The vertical binning factor.

Function (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The binning function.

Outputs
~~~~~~~

Binned (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

This function reduces an image in size by combining several neighboring pixels. The binning factor in horizontal and vertical direction can be different. Different mathematical methods for combination can be selected with the binning function.

**ArithmeticMean**: the aithmetic mean of the neighboring pixels is used.

.. figure:: images/BinningArithmeticMean.png
   :alt: 

**GeometricMean**: the geometric mean of the neighboring pixels is used.

.. figure:: images/BinningGeometricMean.png
   :alt: 

**HarmonicMean**: the harmonic mean of the neighboring pixels is used.

.. figure:: images/BinningHarmonicMean.png
   :alt: 

**QuadraticMean**: the quadratic mean of the neighboring pixels is used.

.. figure:: images/BinningQuadraticMean.png
   :alt: 

**Minimum**: the minimum of the neighboring pixels is used.

.. figure:: images/BinningMinimum.png
   :alt: 

**Maximum**: the maximum of the neighboring pixels is used.

.. figure:: images/BinningMaximum.png
   :alt: 

**Midrange**: the midrange of the neighboring pixels is used.

.. figure:: images/BinningMidrange.png
   :alt: 

**Median**: the median of the neighboring pixels is used.

.. figure:: images/BinningMedian.png
   :alt: 

Sample
~~~~~~

Here is an example that bins an image by a factor of 4 in both horizontal and vertical directions.

.. figure:: images/BinningSample.png
   :alt: 


