Compare Text
------------

Compares two texts, ignoring or honoring their case, and returns an integer that indicates their relative position in the sort order.

.. figure:: images/StringCompareNode.png
   :alt: 

Inputs
~~~~~~

A (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^

The first text to compare.

B (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^

The second text to compare.

IgnoreCase (Type: ``Boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

True to ignore case during the comparison, false otherwise.

Outputs
~~~~~~~

Relation (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

An integer that indicates the relationship between the two texts:

+---------+-----------------------------------------------------------------+
| value   | condition                                                       |
+=========+=================================================================+
| < 0     | **A** precedes **B** in the sort order.                         |
+---------+-----------------------------------------------------------------+
| 0       | **A** occurs in the same position as **B** in the sort order.   |
+---------+-----------------------------------------------------------------+
| > 0     | **A** follows **B** in the sort order.                          |
+---------+-----------------------------------------------------------------+

