HMI Button
----------

Buttons are used to switch or visualize state, as well as to trigger actions. The **Button** is a push-button, which is released when you no longer push it.

.. figure:: images/HMIButtonNode.png
   :alt: 

A **Button** is down as long as it is held down with the mouse. When it is no longer held down, it releases itself to the up state.

At the bottom of the **Button** node is a pin that allows it to connect one child.

Inputs
~~~~~~

Style (Type: ``Style``)
^^^^^^^^^^^^^^^^^^^^^^^

Optional styling of the **Button**.

The **Border** respects the *Width*, *Height*, *HorizontalAlignment*, *VerticalAlignment*, *Margin*, *Padding*, *Foreground*, *Background*, *Font* and *Padding* styles.

Name (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^

The text that is displayed within the button. This text is only displayed, when no child is connected. If a child is connected, the visual definition of the child is used.

Outputs
~~~~~~~

IsDown (Type: ``boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

``True`` if the button is down, ``False`` otherwise.

Example
~~~~~~~

Here is an example that shows a simple text button. This definition:

.. figure:: images/text_button_def.png
   :alt: 

creates the following user interface:

.. figure:: images/text_button.png
   :alt: 

And this more complicated example shows a button with an image and a text. This definition:

.. figure:: images/button_with_image_def.png
   :alt: 

creates the following user interface:

.. figure:: images/button_with_image.png
   :alt: 


