Crop Box
--------

Crops a rectangular portion out of an image.

.. figure:: images/CropBoxNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

CropBox (Type: ``Box``)
^^^^^^^^^^^^^^^^^^^^^^^

The box geometry.

Outputs
~~~~~~~

Cropped (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The rectangular portion of the original image that is specified by the crop box.

Comments
~~~~~~~~

This function cuts out a rectangular portion of an image. Often this is used to constrain a subsequent processing operation to an area of interest.

Sample
~~~~~~

Here is an example, where the box is specified interactively with a box widget:

.. figure:: images/CropBoxSample.png
   :alt: 


