Text -> CornerRadius
--------------------

Converts text to a CornerRadius.

Inputs
~~~~~~

Text (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^

The text that is converted to a CornerRadius.

Outputs
~~~~~~~

CornerRadius (Type: ``CornerRadius``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The output CornerRadius.

Comments
~~~~~~~~

A CornerRadius encodes radius values of four corners, for *TopLeft*, *TopRight*, *BottomRight* and *BottomLeft*.

You can specify one number, which is then used for all four corners, i.e. ``50``.

Alternatively, you can specify four numbers separated with semicolons, that are used for *TopLeft*, *TopRight*, *BottomRight* and *BottomLeft*, i.e. ``5, 10, 20, 30``.
