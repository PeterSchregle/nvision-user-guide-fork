Change Extension
----------------

Changes the extension of a path string.

.. figure:: images/ChangeExtensionNode.png
   :alt: 

Inputs
~~~~~~

Path (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The path information to modify.

Extension (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The new extension (with or without a leading period). Specify an empty string to remove an existing extension from path.

Outputs
~~~~~~~

NewPath (Type: ``Ngi.Path``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The modified path information

Comments
~~~~~~~~

If path is an empty string, the path information is returned unmodified. If extension is an empty string, the returned string contains the specified path with its extension removed. If path has no extension, and extension is not the empty string, the returned path string contains extension appended to the end of path.
