SerialPort
----------

Establish a connection to a serial port, which allows you to communicate over a serial interface.

.. figure:: images/SerialPortNode.png
   :alt: 

This node initializes the serial port interface. You can connect the output to a **Read** node to read data from the serial port, or a **Write** node to write data to the serial port.

Inputs
~~~~~~

Port (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^

The identifier of the serial port (``COM1``, ``COM2``, etc.).

Baudrate (Type: ``UInt32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The baudrate of the serial port (``9600``, ``14400``, ``19200``, etc.).

ByteSize (Type: ``UInt32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The number of bits in the bytes transmitted and received (4 - 8).

Parity (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The parity (``No``, ``Odd``, ``Even``, ``Mark`` or ``Space``).

StopBits (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The number of stop bits (``One``, ``OnePointFive`` or ``Two``).

ReadTimeout (Type: ``UInt32``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The read timeout in milliseconds (0: polling, > 0: use the timeout for read operations).

Outputs
~~~~~~~

SerialPort (Type: ``SerialPort``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An instance of the serial port.

Comments
~~~~~~~~

The output of the **SerialPort** node is usually connected to **Read** or **Write** nodes, in order to read from or write to the serial port, respectively.

Since establishing a connection to a serial port is often an operation with a somewhat global character, the **SerialPort** node is sometimes put into the pipeline globals or the system globals.
