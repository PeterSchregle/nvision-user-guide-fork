Maximum
-------

Calculates the maximum value of a buffer.

.. figure:: images/BufferMaximumNode.png
   :alt: 

Inputs
~~~~~~

Buffer (Type: ``Buffer``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input buffer.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

An optional region.

Outputs
~~~~~~~

Maximum (Type: ``Object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The maximum value.

Comments
~~~~~~~~

The **Maximum** node calculates the maximum value of a buffer.

If the *Region* input is connected, the calculation of the maximum is constrained to the pixels within the region only, otherwise the whole buffer is used.

If a color buffer is used, the maximum length color vector is calculated.

Sample
~~~~~~

Here is an example that calculates the maximum of an image.

.. figure:: images/BufferMaximumSample.png
   :alt: 


