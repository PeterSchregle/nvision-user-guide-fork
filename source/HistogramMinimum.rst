Histogram Minimum
-----------------

Calculates the minimum value of a buffer, given a histogram.

.. figure:: images/HistogramMinimumNode.png
   :alt: 

Inputs
~~~~~~

Histogram (Type: ``Histogram``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The input histogram.

Outputs
~~~~~~~

Minimum (Type: ``Object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The minimum value.

Comments
~~~~~~~~

The **Histogram Minimum** node calculates the minimum value of a buffer, given a histogram to it.

If a color buffer is used, the minimum is calculated channel-wise.

Sample
~~~~~~

Here is an example that calculates the minimum of a histogram.

.. figure:: images/BufferMinimumSample.png
   :alt: 


