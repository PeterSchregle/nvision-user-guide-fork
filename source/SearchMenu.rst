Search Menu
-----------

.. figure:: images/search_menu.png
   :alt: 

You can search the context menu for a specific command with this edit control. Start typing letters to narrow down the menu, so that you are able to quickly find the command you are looking for.

I.e., if you type ``histo`` the menu will show only commands that have the letters ``histo`` in their name or in the name of their group. You will be able to find all the commands that are related to ``histogram`` very quickly.

.. figure:: images/search_menu_2.png
   :alt: 

This is a very useful way to quickly find the command you are looking for, if you know the name.
