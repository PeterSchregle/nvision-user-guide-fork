Save File
---------

Saves text to a file.

.. figure:: images/SaveFileNode.png
   :alt: 

Inputs
~~~~~~

Text (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^

The text that is written to the file.

Directory (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A directory. This is preset with the user's documents folder.

Filename (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

A filename. This is preset with ``"data.txt"``.

Comments
~~~~~~~~

The **Save File** node writes text to a file.

The directory and filename of the file can be specified. If no directory is used, the node uses the user's documents folder. If no filename is used, the node uses the name ``"data.txt"``. If the directory does not exist, it will be created.

The node only executes, if the **Export On/Off** mode has been turned on,

.. figure:: images/ExportOnOffButton.png
   :alt: 

or if the **Export Once** button is clicked.

.. figure:: images/ExportOnceButton.png
   :alt: 

Otherwise, the node does not execute and has no effect.
