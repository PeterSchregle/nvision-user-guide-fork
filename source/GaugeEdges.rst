Edges
-----

Finds vertical or horizontal edges and their positions in an image.

.. figure:: images/GaugeEdgesNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The source image. This should be a monochrome image. If the image is a color image, it is internally converted to monochrome, before the edges are searched.

Orientation (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The orientation, the default is ``LeftToRight``. ``LeftToRight`` or ``RightToLeft`` look for horizontal edges, ``TopToBottom`` or ``BottomToTop`` look for vertical edges. When horizontal edges are to be found, all horizontal rows in the image are averaged together, whereas when vertical edges are to be found, all vertical columns in the image are averaged together. The edges are then searched in the averaged row or column.

Polarity (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The type of the grayscale transition to look for: ``Rising``, ``Falling`` or ``Both``, the default is ``Rising``. ``Rising`` only finds edges going from dark to bright, ``Falling`` only finds edges going from bright to dark, when searching in the direction specified by **Orientation**.

Selector (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The selection of the edges: ``First`` finds the first edge and ``Last`` finds the last edge when searching in the direction specified by **Orientation**. ``Best`` (the default) finds the best edge, where the best edge is the edge with the steepest gradient. ``All`` finds all edges.

Sigma (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the **Sigma** parameter of a gaussian smoothing filter, which is applied to the averaged row or column. The default is 2, which specifies considerable smoothing. Thus, the overall smoothing of the greyvalue profile comes both from the smoothing effect of averaging multiple rows or columns, combined with additional smoothing using a gaussian filter.

The effect of the smoothing on the greyvalue profile can be illustrated with the following pictures. First, no smoothing, i.e. **Sigma == 0**:

.. figure:: images/gauge_edges_info_profile_sigma0.png
   :alt: 

Then, considerable smoothing, i.e. **Sigma == 5**:

.. figure:: images/gauge_edges_info_profile_sigma5.png
   :alt: 

You can see that stronger smoothing in the profile flattens out weak edges and makes stronger edges less steep.

Threshold (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To find edges, the grayvalue profile is differentiated twice in order to find inflection points. The inflection points correspond to the subpixel position of the edge (zero crossings in the second derivative), and the value of the first derivative corresponds to the steepness of the edge. The threshold parameter specifies the minimum steepness and thereby controls what is considered to be an edge. The default is 15, which specifies a considerable steep edge.

The effect of the smoothing on the gradient profile can be illustrated with the following pictures. First, no smoothing, i.e. **Sigma == 0**:

.. figure:: images/gauge_edges_info_gradient_sigma0.png
   :alt: 

Then, considerable smoothing, i.e. **Sigma == 5**:

.. figure:: images/gauge_edges_info_gradient_sigma5.png
   :alt: 

You can see how stronger smoothing in the profile weakens the edge gradients.

Outputs
~~~~~~~

Edges (Type: 'Edge1dList')
^^^^^^^^^^^^^^^^^^^^^^^^^^

A list of ``Edge1d`` edges. An ``Edge1d`` contains a subpixel **Position**, the interpolated **Grey** value at the position and the **Strength**, which is the interpolated gradient value or steepness at the position. The **Position**\ s of the edges are with respect to the left or top of the image. They are not affected by the **Orientation** of the search, i.e. ``LeftToRight`` or ``RightToLeft``, or also ``TopToBottom`` or ``BottomToTop`` return the same position values.

Comments
~~~~~~~~

The **Edges** node finds vertical or horizontal edge positions in an image. When horizontal edges are to be found, all horizontal rows in the image are averaged together, whereas when vertical edges are to be found, all vertical columns in the image are averaged together. The edges are then searched in the averaged row or column.

See Also
~~~~~~~~

There is also the **Edges Info** node, which, in addition to outputting the same edge information, also outputs the internal profile and gradient data.

Sample
~~~~~~

Here is an **➠ example** that gauges several edges in an image. The **➠ example** allows you to interactively explore the parameters of the **Edges** node.

.. figure:: images/GaugeEdgesSample.png
   :alt: 


