Boolean
-------

Create and set a boolean value.

.. figure:: images/ConstantBoolNode.png
   :alt: 

Outputs
~~~~~~~

Value (Type: ``Boolean``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The boolean value.

Comments
~~~~~~~~

This node provides the means to create and set a boolean value.

The value can be changed by clicking on the button inside the node.

The title of the node can be edited.
