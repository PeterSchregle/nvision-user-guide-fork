nVision - Videos
================

**nVision** tutorial videos are available online. Watching these videos gives you a quick start with **nVision**.

Introduction
~~~~~~~~~~~~

\ |image0|\ 

\ |image1|\ 

nVision Tools
~~~~~~~~~~~~~

\ |image2|\ 

\ |image3|\ 

\ |image4|\ 

\ |image5|\ 

.. |image0| image:: images/introduction_to_nvision.png
.. |image1| image:: images/introduction_to_pipeline_programming_with_nvision.png
.. |image2| image:: images/locating_parts.png
.. |image3| image:: images/identifying_parts.png
.. |image4| image:: images/checking_parts.png
.. |image5| image:: images/measuring_parts.png
