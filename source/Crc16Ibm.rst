Crc16Ibm
--------

The CRC16 IBM cyclic redundancy check.

.. figure:: images/Crc16IbmNode.png
   :alt: 

Calculates the CRC using the CRC16 IBM algorithm.

Inputs
~~~~~~

Data (Type: ``DataList``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The data to write in the form of a DataList (a list of bytes).

Outputs
~~~~~~~

Crc (Type: ``Byte``)
^^^^^^^^^^^^^^^^^^^^

The result of the cyclic redundancy check.

Comments
~~~~~~~~

The CRC16 IBM algorithm is one implementation of a cyclic redundancy check, used for error detection.

For more information, have a look at https://en.wikipedia.org/wiki/Cyclic_redundancy_check.
