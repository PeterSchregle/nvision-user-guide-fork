Point
-----

Creates a point.

.. figure:: images/PointDoubleNode.png
   :alt: 

A point is a geometric entity, representing a location in the two-dimensional cartesian plane.

.. figure:: images/point.png
   :alt: 

Inputs
~~~~~~

X (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^

The horizontal coordinate.

Y (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^

The vertical coordinate.

Outputs
~~~~~~~

PointDouble (Type: ``PointDouble``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The point.
