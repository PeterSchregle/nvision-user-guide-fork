Imago System
------------

Factory to acquire/release every device implementation.

.. figure:: images/ImagoSystemNode.png
   :alt: 

Inputs
~~~~~~

Index (Type: ``Int32``)
^^^^^^^^^^^^^^^^^^^^^^^

The index of the desired system. The system itself is selected with the combobox inside the node.

Outputs
~~~~~~~

System (Type: ``VIB_NET.VIBSystem``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The factory instance.

Comments
~~~~~~~~

The first step in using any Imago device functionality is to create the system. With a system, you can then create an Imago device, and with a device you can call any method or property on it to carry out the respective operation.

For a more detailed description see the Imago .NET documentation (VIBSystem::VIBSystem method).
