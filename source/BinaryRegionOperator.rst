BinaryRegionOperator
--------------------

Applies a binary operation between two regions.

.. figure:: images/BinaryRegionOperatorNode.png
   :alt: 

The following operators are available:

.. figure:: images/BinaryRegionOperators.png
   :alt: 

Union (``|``)
             

Calculates the union (yellow) of two regions A (dark), B (bright).

.. figure:: images/region_union.png
   :alt: 

A \| B -> Union

Intersection (``&``)
                    

Calculates the intersection (yellow) of two regions A (dark), B (bright).

.. figure:: images/region_intersection.png
   :alt: 

A & B -> Intersection

Difference (``\``)
                  

Calculates the difference (yellow) of two regions A (dark), B (bright).

.. figure:: images/region_difference_a-b.png
   :alt: 

A  B -> Difference

.. figure:: images/region_difference_b-a.png
   :alt: 

B  A -> Difference

Inputs
~~~~~~

RegionA (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The first input region.

RegionB (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^^

The second input region.

Operator (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the operator.

+------------+----------------+
| operator   | operation      |
+============+================+
| ``|``      | union          |
+------------+----------------+
| ``&``      | intersection   |
+------------+----------------+
| ``\``      | difference     |
+------------+----------------+

Outputs
~~~~~~~

Result (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The result region.
