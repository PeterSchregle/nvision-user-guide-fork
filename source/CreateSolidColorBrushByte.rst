Solid Color Brush
-----------------

Creates a solid color brush. A brush is used to specify fills.

.. figure:: images/SolidColorBrushNode.png
   :alt: 

Inputs
~~~~~~

R (Type: ``Byte``)
^^^^^^^^^^^^^^^^^^

The red primary.

G (Type: ``Byte``)
^^^^^^^^^^^^^^^^^^

The green primary.

B (Type: ``Byte``)
^^^^^^^^^^^^^^^^^^

The blue primary.

Alpha (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

The opacity.

Outputs
~~~~~~~

SolidColorBrush (Type: ``SolidColorBrushByte``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The brush.

Sample
~~~~~~

Here is an example that creates a yellowish half transparent brush and a wide pen using this brush.

.. figure:: images/BrushPenSample.png
   :alt: 


