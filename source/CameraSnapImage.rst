Snap Image
----------

Snaps an image from a camera.

.. figure:: images/SnapImageNode.png
   :alt: 

The preview shows the number of available cameras.

Inputs
~~~~~~

Camera (Type: ``CameraInfo``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the camera.

Sync (Type: ``System.Object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This can be used to establish a temporal order, so that you can specify, when the image shapshot occurs.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The acquired image.

Example
~~~~~~~

Here is an example that snaps two images in fast succession:

.. figure:: images/snap_image.png
   :alt: 


