Quantile
--------

Calculates the quantile of a buffer.

.. figure:: images/BufferQuantileNode.png
   :alt: 

Inputs
~~~~~~

Buffer (Type: ``Buffer``)
^^^^^^^^^^^^^^^^^^^^^^^^^

The input buffer.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

An optional region.

Percentage (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the quantile.

Outputs
~~~~~~~

Quantile (Type: ``Object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The quantile value.

Comments
~~~~~~~~

The **Quantile** node calculates a quantile of a buffer.

If the *Region* input is connected, the calculation of the quantile is constrained to the pixels within the region only, otherwise the whole buffer is used.

If a color buffer is used, the quantile is calculated channel-wise.

Sample
~~~~~~

Here is an example that calculates the 50% quantile of an image.

.. figure:: images/BufferQuantileSample.png
   :alt: 


