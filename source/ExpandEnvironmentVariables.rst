ExpandEnvironmentVariables
--------------------------

Replaces the name of each environment variable embedded in the specified string with the string equivalent of the value of the variable, then returns the resulting string.

.. figure:: images/ExpandEnvironmentVariablesNode.png
   :alt: 

Inputs
~~~~~~

In (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^

A string containing the names of zero or more environment variables. Each environment variable is quoted with the percent sign character (%).

Outputs
~~~~~~~

Out (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^

A string with each environment variable replaced by its value.
