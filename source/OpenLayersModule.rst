OpenLayers Module
-----------------

Establish a connection to an OpenLayers module from Data Translation, which allows you to read and write digital signals.

.. figure:: images/OpenLayersModuleNode.png
   :alt: 

This node initializes the module. If you connect its output to a **GetProperty** node, you can gather information about the device. Usually you would connect the output to an **OpenLayers DigIn** node to read electrical input, or an **OpenLayers DigOut** node to write electrical output.

.. figure:: images/dt9817.jpg
   :alt: 

The OpenLayers modules are connected via USB or PCI to a PC and support a variety of input/output lines. The modules may have additional features, such as counters, etc., but these are not yet supported.

+------------+-------+----------------------------------------+
| module     | bus   | features                               |
+============+=======+========================================+
| DT9817     | USB   | 28 digital inputs/outputs              |
+------------+-------+----------------------------------------+
| DT9817-H   | USB   | 28 digital inputs/outputs              |
+------------+-------+----------------------------------------+
| DT9817-R   | USB   | 8 digital inputs, 8 digital outputs    |
+------------+-------+----------------------------------------+
| DT9835     | USB   | 64 digital inputs/outputs              |
+------------+-------+----------------------------------------+
| DT335      | PCI   | 32 digital inputs/outputs              |
+------------+-------+----------------------------------------+
| DT251      | PCI   | 8 digital inputs, 8 digital outputs    |
+------------+-------+----------------------------------------+
| DT340      | PCI   | 32 digital inputs, 8 digital outputs   |
+------------+-------+----------------------------------------+

Inputs
~~~~~~

Name (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^

The name of the OpenLayers module.

Outputs
~~~~~~~

Module (Type: ``OpenLayersModule``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The OpenLayers module instance.
