Convert to XYZ
--------------

Converts an image to the XYZ color model.

.. figure:: images/ConvertToLabNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image. This image can be of any color model.

Outputs
~~~~~~~

Xyz (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^

The output image in the XYZ color model.

Comments
~~~~~~~~

This function converts an image to the XYZ color model, regardless of the color space of the input image.

Here is an example of the original color image:

.. figure:: images/rgbimg.png
   :alt: 

And here is how the x, y and c channels look side by side.

.. figure:: images/lab_sbs.png
   :alt: 

Sample
~~~~~~

Here is an example:

.. figure:: images/ConvertToLabSample.png
   :alt: 


