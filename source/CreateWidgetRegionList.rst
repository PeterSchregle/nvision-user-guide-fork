Regions
-------

The **Regions** node is used to display a list of regions.

.. figure:: images/WidgetRegionsNode.png
   :alt: 

At the top of the **Regions** node is a pin that allows it to connect to a parent widget. The region will be displayed only, if a parent connection is made.

At the bottom of the **Regions** node is a pin that allows it to connect children to the regions widget. The parent, grandparent, children and grandchildren define the layering of the widgets and the graphical outcome.

Children are layered on top of the parent.

Inputs
~~~~~~

RegionList (Type: ``RegionList``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of regions to display inside the widget. The different regions are colored with a total of six colors: red, green, blue, yellow, cyan, magenta. These colors are used in turn, and recycled as needed.

Alpha (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^^^^

The opacity for the regions display: 0 is fully transparent, 1 is fully opaque.

Example
~~~~~~~

Here is an example that displays a list of regions on top of an image. This pipeline

.. figure:: images/RegionsDisplayExample.png
   :alt: 

shows the following result:

.. figure:: images/RegionsDisplayExample2.png
   :alt: 


