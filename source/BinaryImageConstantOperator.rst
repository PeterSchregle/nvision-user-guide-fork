BinaryImageConstantOperator
---------------------------

Applies a binary point operation between an image and a constant. Possible operations can be grouped into arithmetic, logic and comparison fields. Binary point operations are applied pixel by pixel, the same way for every pixel.

.. figure:: images/BinaryImageConstantOperatorNode.png
   :alt: 

Arithmetic Operators
''''''''''''''''''''

The following operators are available:

.. figure:: images/BinaryImageOperatorArithmetic.png
   :alt: 

Add
   

Add with saturation.

.. figure:: images/BinaryConstantAdd.png
   :alt: 

Image + Constant -> Result

Subtract
        

Subtract with saturation.

.. figure:: images/BinaryConstantSubtract.png
   :alt: 

Image - Constant -> Result

Difference
          

Difference with saturation.

.. figure:: images/BinaryConstantDifference.png
   :alt: 

abs ( Image - Constant ) -> Result

Multiply
        

Multiply without saturation.

.. figure:: images/BinaryConstantMultiply.png
   :alt: 

Image \* Constant -> Result

Divide
      

Divide without saturation.

.. figure:: images/BinaryConstantDivide.png
   :alt: 

Image / Constant -> Result

Multiply (Blend)
                

Multiply with saturation.

.. figure:: images/BinaryConstantMultiplyBlend.png
   :alt: 

Image \* Constant -> Result

Divide (Blend)
              

Divide with saturation.

.. figure:: images/BinaryConstantDivideBlend.png
   :alt: 

Image / Constant -> Result

Logic Operators
'''''''''''''''

The following operators are available:

.. figure:: images/BinaryImageOperatorLogic.png
   :alt: 

And
   

Logical And.

.. figure:: images/BinaryConstantAnd.png
   :alt: 

Image & Constant -> Result

Or
  

Logical Or.

.. figure:: images/BinaryConstantOr.png
   :alt: 

Image \| Constant -> Result

Xor
   

Logical Xor.

.. figure:: images/BinaryConstantXor.png
   :alt: 

Image ^ Constant -> Result

Comparison Operators
''''''''''''''''''''

The following operators are available:

.. figure:: images/BinaryImageOperatorComparison.png
   :alt: 

Smaller
       

Compare smaller.

.. figure:: images/BinaryConstantSmaller.png
   :alt: 

Image < Constant -> Result

Smaller or Equal
                

Compare smaller or equal.

.. figure:: images/BinaryConstantSmallerOrEqual.png
   :alt: 

Image <= Constant -> Result

Equal
     

Compare equal.

.. figure:: images/BinaryConstantEqual.png
   :alt: 

Image == Constant -> Result

Bigger or Equal
               

Compare bigger or equal.

.. figure:: images/BinaryConstantBiggerOrEqual.png
   :alt: 

Image >= Constant -> Result

Bigger
      

Compare bigger.

.. figure:: images/BinaryConstantBigger.png
   :alt: 

Image > Constant -> Result

Min/Max Operators
'''''''''''''''''

The following operators are available:

.. figure:: images/BinaryImageOperatorMinMax.png
   :alt: 

Min
   

Minimum.

.. figure:: images/BinaryConstantMin.png
   :alt: 

min( Image, Constant) -> Result

Max
   

Maximum.

.. figure:: images/BinaryConstantMax.png
   :alt: 

max( Image, Constant) -> Result

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Constant (Type: ``object``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The constant.

Operator (Type: ``string``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies the operator.

+---------------+--------------------+
| operator      | operation          |
+===============+====================+
| ``+``         | add                |
+---------------+--------------------+
| ``-``         | subtract           |
+---------------+--------------------+
| ``diff``      | difference         |
+---------------+--------------------+
| ``*``         | multiply           |
+---------------+--------------------+
| ``/``         | divide             |
+---------------+--------------------+
| ``*_blend``   | multiply (blend)   |
+---------------+--------------------+
| ``/_blend``   | divide (blend)     |
+---------------+--------------------+
| ``&``         | and                |
+---------------+--------------------+
| ``|``         | or                 |
+---------------+--------------------+
| ``^``         | xor                |
+---------------+--------------------+
| ``<``         | smaller            |
+---------------+--------------------+
| ``<=``        | smaller or equal   |
+---------------+--------------------+
| ``==``        | equal              |
+---------------+--------------------+
| ``>=``        | bigger or equal    |
+---------------+--------------------+
| ``>``         | bigger             |
+---------------+--------------------+
| ``min``       | minimum            |
+---------------+--------------------+
| ``max``       | maximum            |
+---------------+--------------------+

Outputs
~~~~~~~

Result (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^

The result image.
