Subpipelines
============

**Subpipelines** can be used to group a set of connected **Nodes** into one **Node**.

As any other **Node**, a **Subpipeline** has **Input Pins** and/or **Output Pins**.

By default, a **Subpipeline** **Node** has one **Input Pin** and one **Output Pin**.

.. figure:: ./images/subpipeline.png
   :alt: 

The name of the **Subpipeline** can be edited by clicking it:

.. figure:: ./images/subpipeline_title_edit.png
   :alt: 

Inside the **Subpipeline** (double-click the **Node** to get inside) it looks like this:

.. figure:: ./images/subpipeline_inside.png
   :alt: 

Additional **Pins** of a **Subpipeline** **Node** can be defined with the *Add Input Pin* and *Add Output Pin* commands on the context menu. The name of the **Pin** can be edited by clicking on it. A **Pin** can also be removed with the *Delete Pin* command (right click somewhere on the **Pin** definition - but not on the text).

The **Type** of a **Subpipeline** **Pin** is undefined as long as it is not connected. As soon as the **Pin** is connected (either from the inside, or from the outside) its **Type** is set (determined by the **Type** of the **Pin** on the other end of the connection).
