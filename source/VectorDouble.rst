Vector
------

Creates a vector.

.. figure:: images/VectorDoubleNode.png
   :alt: 

A vector is a geometric entity, representing a directed length in the two-dimensional cartesian plane.

.. figure:: images/vector.png
   :alt: 

Inputs
~~~~~~

Dx (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^

The horizontal coordinate.

Dy (Type: ``Double``)
^^^^^^^^^^^^^^^^^^^^^

The vertical coordinate.

Outputs
~~~~~~~

VectorDouble (Type: ``VectorDouble``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The vector.
