Combine Channels
----------------

Combines three channels to create a color image.

.. figure:: images/CombineChannelsNode.png
   :alt: 

Inputs
~~~~~~

First (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The first channel image.

Second (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^^

The second channel image.

Third (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The third channel image.

Model (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^

The color model of the resulting image.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

This function combines three channels to a color image.

The color model specifies the color space of the resulting image and also the meaning of the channels. The following color models are available: **RGB**, **HLS**, **HSI** and **LAB**.

Given the following channels (red, green and blue from left to right):

.. figure:: images/combine_channels.png
   :alt: 

Here are the channels again visualized in their proper colors:

.. figure:: images/combine_channels_color.png
   :alt: 

The result of the combination as an RGB image is:

.. figure:: images/combine_channels_rgb.png
   :alt: 

Sample
~~~~~~

Here is an example that combines three channels to form a color image.

.. figure:: images/CombineChannelsSample.png
   :alt: 


