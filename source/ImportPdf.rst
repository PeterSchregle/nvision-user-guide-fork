Import PDF
----------

Imports a file in PDF format.

.. figure:: images/ImportPdfNode.png
   :alt: 

Inputs
~~~~~~

Filename (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

A filename in the filesystem.

Password (Type: ``String``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

An optional password to enable loading of password protected PDF files.

Outputs
~~~~~~~

Document (Type: ``PdfLoadedDocument``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The loaded PDF document.

Comments
~~~~~~~~

The **Import PDF** node loads a PDF file from the filesystem. The name of the file can either by typed, or selected with a File Open dialog - by clicking on the |image0| button. The node supports a variety of image file formats, such as TIF, PNG, JPG and BMP to name a few. If a file could not be loaded for some reason, an appropriate error message is shown.

Once a PDF file has been loaded, any page in it can be rendered into an image with the **Render PDF** node.

.. |image0| image:: images/EllipsisButton.png
