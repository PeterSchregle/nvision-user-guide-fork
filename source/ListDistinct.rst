Distinct
--------

Removes duplicates from a list, so that only distinct items remain in the list.

.. figure:: images/ListDistinctNode.png
   :alt: 

Inputs
~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The input list.

Outputs
~~~~~~~

List (Type: ``List<T>``)
^^^^^^^^^^^^^^^^^^^^^^^^

The output list, which consists of the distinct items of the input list only.

Comments
^^^^^^^^

The output list is a set in the mathematical sense, which can have distinct objects only.
