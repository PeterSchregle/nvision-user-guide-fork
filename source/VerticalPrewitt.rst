Vertical Prewitt
----------------

Filters an image using a vertical prewitt kernel.

.. figure:: images/VerticalPrewittNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image.

Region (Type: ``Region``)
^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies an optional area of interest.

Outputs
~~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The output image.

Comments
~~~~~~~~

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

::

    1 0 -1
    1 0 -1
    1 0 -1

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

Here are a few results of the vertical prewitt filter with increasing kernel sizes:

Original:

.. figure:: images/GaussianOriginal.png
   :alt: 

Result:

.. figure:: images/VerticalPrewitt.png
   :alt: 

Sample
~~~~~~

Here is an example that shows how to use the vertical prewitt filter.

.. figure:: images/VerticalPrewittSample.png
   :alt: 

Often, you want to combine the horizontal and the vertical prewitt filter to create an edge map. A simple way to do this is shown in the following sample:

.. figure:: images/PrewittEdgeMapSample.png
   :alt: 

Here is an example image of such an edge map:

.. figure:: images/PrewittEdgeMap.png
   :alt: 


