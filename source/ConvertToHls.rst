Convert to HLS
--------------

Converts an image to the HLS color model.

.. figure:: images/ConvertToHlsNode.png
   :alt: 

Inputs
~~~~~~

Image (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^^^

The input image. This image can be of any color model.

Outputs
~~~~~~~

Hls (Type: ``Image``)
^^^^^^^^^^^^^^^^^^^^^

The output image in the HLS color model.

Comments
~~~~~~~~

This function converts an image to the HLS color model, regardless of the color space of the input image.

Here is an example of the original color image:

.. figure:: images/rgbimg.png
   :alt: 

And here is how the hue, lightness and saturation channels look side by side.

.. figure:: images/hls_sbs.png
   :alt: 

Sample
~~~~~~

Here is an example:

.. figure:: images/ConvertToHlsSample.png
   :alt: 


